# 마이크로 앱 개발 가이드
## 컴포넌트의 종류
마이크로앱에서 정의하는 컴포넌트는 다음과 같이 분류할 수 있습니다.
1. 앱 컴포넌트 : 모든 마이크로 페이지 컴포넌트를 감싸는 최상위 컴포넌트입니다.
2. 마이크로 페이지 : 클라이언트에게 노출되는 페이지이며, 디렉토리 기반의 url마다 한 컴포넌트 씩 매핑됩니다.
3. 메인 페이지 : 마이크로 페이지의 일부이며, 앱의 메인 페이지입니다. 반드시 public 페이지로 구현합니다.
4. 레이아웃 : 컬렉티 별로 구현되며, 메뉴 구성과 AppDock 컴포넌트를 통해 구현됩니다. 마이크로 페이지를 감싸는 레이아웃입니다.
5. AppDock : 테넌트 이동을 위한 Transit 컴포넌트와 메뉴를 통해 구현됩니다.
6. Transit : 테넌트 이동을 위한 컴포넌트이며, 접근 가능한 공간(조직)에 대한 정보를 제공합니다.

## 프로젝트 개요
클래스형과 함수형 컴포넌트의 사용 가이드를 위해 classpattern, functionpattern이라는 두개의 컬렉티로 이루어진 마이크로 앱입니다.

## Dock 서비스
Dock 서비스는 마이크로 앱 개발의 편이성을 위해 구현되었으며, Nara서비스(Metro, Stage, Checkpoint ...)의 Weaving 서비스입니다.     
즉, 개발자는 Nara 서비스를 알 필요가 없으며, Dock 서비스를 통해 런타임에서의 세션 정보와, role 정보를 자동 주입받아 사용할 수 있습니다.    
이 정보와 Dock서비스의 기능을 통해 다음과 같은 인가 처리를 진행합니다.

![auth_summary](./docs/auth_summary.png)

## Dock 서비스 기반의 컴포넌트 구현
앞서 알아본 컴포넌트의 종류에 따라 dock 서비스를 기반으로 구현하는 방법에 대해 설명합니다.

#### 1. 앱 컴포넌트
앱 컴포넌트는 모든 마이크로 페이지 컴포넌트를 감싸는 부모 컴포넌트 입니다. 기능은 다음과 같습니다.
1. Tenant 기반의 인가 처리 : Cineroom, Cast
2. Role 기반 인가 처리 및 정보 제공 : Roles(StageRole, TaskRole, DramaRole), allowed, menus(visible)       
3. 세션 정보 제공 : Current Context(Cineroom, Audience, Cast, Actor), Accessible Tenant(Cinerooms, Casts)
4. 로그인, 로그아웃 함수 제공 : LoginAction, LogoutAction
5. Role 정보 제공 : Roles(StageRole, TaskRole, DramaRole), hasSomeRole 함수
6. 페이지 Props에 따른 분기 처리 : 레이아웃 사용여부, public 페이지, 컬렉티 정보 등..

  ```typescript jsx
        <Provider                                         // page에서 사용되는 store를 주입합니다.
          {...store}
        >
          <ReactApp>                                      // 테마 적용을 위한 컨텍스트가 적용됩니다.
            <Bay                                          // Dock 서비스를 위한 컨텍스트가 제공됩니다.
              routeToAppMainAction={this.routeToMain}     // 인가 실패 대응의 한 방법인 메인페이지로의 라우팅 액션을 주입해줍니다.
              // below optional
              isPublic={pageProps.withoutAuth}            // 인가처리를 하지 않도록 하는 옵션입니다.
              devAccount={this.getOptionalDevAccount()}   // 개발 모드일 경우 자동 인가 처리를 위한 계정입니다..
              onExpiredSession={this.onExpiredSession}    // 토큰의 세션이 만료되었을 때 이루어져야할 행위를 주입합니다.
            >
              <Helmet>
                <title>template</title>
                <link rel="icon" href="/kollect-template-front/images/favicon/favicon.ico" />
              </Helmet>
              {this.renderByLayout()}                     // pageProps를 통해 제공되는 값을 통해 레이아웃 적용 여부를 결정합니다.
            </Bay>
          </ReactApp>
        </Provider>
  ```
[가이드]
1. 위치 : ~/src/pages/_app.page.tsx
2. 개발 영역 :
   - 컬렉티 타입에 따른 레이아웃 처리
   - RouteAction에 대한 정의(main page)


#### 2. 마이크로 페이지
마이크로 페이지는 마이크로 서비스의 컴포넌트가 조합되어 클라이언트에게 직접 노출되는 페이지 컴포넌트 입니다. 
마이크로 페이지에서는 다음의 요소를 고려해야합니다.
1. pageProps 정의 
   1) withoutAuth : 인가 처리 여부 ( true : public 페이지 ) (Optional)
   2) withoutAppLayout : 해당 페이지의 컬렉티 정보           (Optional)
   3) kollectieType : 해당 페이지의 컬렉티 정보
    ```typescript
    export async function getStaticProps() {
      //
      return {
        props: {
          withoutAppLayout: true,
          withoutAuth: true,
          kollectieType: KollectieType.ClassPattern
        },
      };
    }
    ```
2. pageRole 정의 : 해당 페이지를 볼수 있는 Role을 정의합니다  (Optional)
    ```typescript
    const pageRoles = [StageRoleType.Admin, StageRoleType.Manager];
    ```
3. dock 서비스 적용
    1) 클래스 패턴

    ```typescript jsx
    import {ReactComponent} from '@nara.drama/prologue';
    import {WithDock} from '@nara.platform/dock';
    import { NextRouter, withRouter } from 'next/router';
    
    interface Props {
      router: NextRouter;
    }
    
    interface InjectedProps extends WithDock {              // WithDock을 extends한 인터페이스 정의
    }
    
    @dock(pageRoles)                                        // dock 서비스 활용을 위한 데코레이터이며, pageRoles 정보 주입(optional)
    class SomePage extends ReactComponent<Props, {}, InjectedProps> {
        render() {
          const { dock, roles, allowed } = this.injected;   // injected로부터 dock 서비스 제공 정보 
          return undefined;
        }
    }
    export default WithRouter(SomePage)
    ```

    2) 함수 패턴

    ```typescript jsx
    import {ReactComponent} from '@nara.drama/prologue';
    import {withDock, WithDock} from '@nara.platform/dock';
    import {NextRouter, withRouter} from 'next/router';
    import {observer} from 'mobx-react';
    
    interface Props extends WithDock {                       // WithDock을 extends한 인터페이스 정의
      router: NextRouter;
    }
    
    const SomePage: React.FC<Props> = observer(
      ({router, ...rest}) => {
        const {dock, roles, allowed} = rest;                // props로부터 dock 서비스 제공 정보
        return undefined;
      }
    )
    export default WithRouter(withDock(SomePage, pageRoles))// dock 서비스 활용을 위한 HoC, pageRoles 정보 주입(optional)
    ```
[가이드]
1. 위치 : ~/src/pages/kollectie/**/index.page.tsx
2. 개발 영역 :
   - pageProps 및 pageRoles 정의
   - dock 서비스를 위한 코드 작성 (~ extends WithDock, 데코레이터 또는 HoC)
3. 마이크로 페이지는 마이크로 서비스의 컴포넌트의 조합이며, 별도의 코드가 없어도 마이크로 서비스에 자동으로 해당 서비스에 맞는 DramaRole이 주입됩니다.       
  단, 마이크로 서비스의 컴포넌트 또한 withDramaRoles 패턴의 HoC로 작성되어야 합니다.


#### 3. 메인 페이지
메인 페이지는 마이크로 페이지의 일부로서 앞서 언급한 인가 처리 방식에 기반하여 반드시 public 페이지로 작성되어야 합니다.
[가이드]
1. 위치 : ~/src/pages/index.page.tsx
2. 개발 영역 :
   - pageProps 에서 반드시 withOutAuth: true 속성을 주어야 합니다.


#### 4. 레이 아웃
레이아웃 페이지는 컬렉티 별로 구현되며 다음의 요소를 고려해야합니다.
1. 메뉴 정보 구성

    ```typescript
    import { MenuNode } from '@nara.platform/dock';
    import { StageRoleType } from '~/nara';
    import fooRoutes from '~/pages/classpattern/foo/routes';
    import barRoutes from '~/pages/classpattern/bar/routes';
    
    export const menuNodes = [
      MenuNode.new({
        key: 'foo',                     // 대표 url과 같은 값이어야하며, 현재 선택된 메뉴 식별을 위해 사용됩니다.
        label: 'FOO',                   // 화면에 표시될 메뉴의 이름입니다.
        route: fooRoutes.index(),       // 해당 메뉴를 클릭했을 때 이동할 라우팅 정보입니다. (optional)
        children: [                     // 해당 메뉴의 하위 메뉴 정보입니다. (optional)
          MenuNode.new({                // 이 메뉴는 라우팅 정보가 서술되지 않은 메뉴로서 타이틀 메뉴를 의미합니다. (node.isTitle = true)
            key: 'foo',
            label: 'Foo',
          }),
        MenuNode.new({                  // 이 메뉴는 라우팅 정보가 서술된 메뉴로서 액션 메뉴를 의미합니다. (node.isTitle = false)
            key: 'content',
            label: 'Content',
            route: fooRoutes.content(),
          }),
          MenuNode.new({                
            key: 'management',
            label: 'Management',
            route: fooRoutes.management(),
            roles: [StageRoleType.Admin, StageRoleType.Manager],    // 해당 메뉴를 볼수 있는 role 정보입니다. 아무 정보도 없을 때는 public으로 간주합니다.
          }),
        ],
      }),
      MenuNode.new({
        key: 'bar',
        label: 'BAR',
        route: barRoutes.index(),
        roles: [StageRoleType.Admin, StageRoleType.Manager],
        children: [
          MenuNode.new({
            key: 'bar',
            label: 'Bar',
            roles: [StageRoleType.Admin, StageRoleType.Manager],
          }),
          MenuNode.new({  
            key: 'content',
            label: 'Content',
            route: barRoutes.content(),
          }),
        ],
      }),
    ];
    
    ```

2. dock 서비스에 메뉴 정보 전달
앞서 정의한 메뉴 정보를 dock 서비스에 전달하여 런타임에서 해당 유저의 Role에 따라 볼수 있는 정보만 리턴받아야 합니다.
메뉴를 전달하는 방식은 pageRole을 전달하던 방식과 매우 유사합니다.     
   1) 클래스형
    ```typescript
    import {dock} from '@nara.platform/dock';
    import {menuNodes} from './menus';
    
    @dock([], menuNodes)
    class LayoutComponent ...
    ``` 
   2) 함수형
    ```typescript
    import {dock} from '@nara.platform/dock';
    import {menuNodes} from './menus';
    ...
    export default withDock(LayoutComponent, [], menuNodes)
    ```
   

2. 트리 구조의 메뉴 노드 핸들링
메뉴 노드는 dock-front에 정의된 모델로서 다음과 같은 특징을 가집니다.
   1. 트리 구조 형태의 모델로써 어떤 형태의 디자인이든 적용할 수 있습니다. (top-side menu || flat menu etc)
   2. route 속성의 유무에 따라 isTitle getter 값이 결정됩니다.
   3. role 정보가 있을 시에 visible 여부를 결정하게 됩니다. 없을 시에는 public으로 간주합니다.
   4. role 기반 visibility는 cascade로 처리되어, 상위 메뉴에서 볼수 없으면 그 하위는 모두 볼 수 없습니다
    ```typescript
    import { autobind, makeExtendedObservable, Route } from '@nara.drama/prologue';
    
    
    type MenuNodeType = {
      key: string;
      label: string;
      roles?: string[];
      children?: MenuNode[];
      route?: Route;
      [key: string]: any;
    };
    
    @autobind
    class MenuNode {
      //
      key: string;
      label: string;
      roles: string[];
      children: MenuNode[];
      route?: Route;
      private readonly unAuthorized: boolean;
    
      constructor(menuNodeType: MenuNodeType, unAuthorized: boolean = false ) {
        //
        Object.assign(this, menuNodeType);
        this.key = menuNodeType.key;
        this.label = menuNodeType.label;
        this.roles = menuNodeType.roles ? menuNodeType.roles : [];
        this.children = menuNodeType.children ? menuNodeType.children : [];
        this.route = menuNodeType.route ? menuNodeType.route : undefined;
    
        this.unAuthorized = unAuthorized;
        makeExtendedObservable(this);
      }
    
      static new(menuNodeType: MenuNodeType) {
        //
        return new MenuNode(menuNodeType);
      }
    
      static newUnauthorized() {
        //
        const dummyNode = new MenuNode({
          key: '',
          label: '',
          roles: [],
          children: [],
        }, true);
    
        return dummyNode;
      }
    
      isVisible(roles: string[]) {
        if (this.roles.length === 0) {
          return true;
        }
        else {
          return roles.find(role => this.roles.includes(role));
        }
      }
    
      get isTitle() {
        if (this.route) {
          return false;
        }
        else {
          return true;
        }
      }
    
      get isUnAuthorized() {
        return this.unAuthorized;
      }
    }
    
    export default MenuNode;
    
    export const filterMenu = (menu: MenuNode, roles: string[]) => {
      if (menu.isVisible(roles)) {
        menu.children = menu.children
          .map(child => filterMenu(child, roles))
          .filter(child => !child.isUnAuthorized);
        return menu;
      }
      else {
        return MenuNode.newUnauthorized();
      }
    };
    ``` 

3. AppDock을 포함한 뷰 제공
    ```typescript jsx
          <KollectieLayoutView
            appDock={                                       // view는 반드시 AppDock 컴포넌트를 주입받아 화면에 표시해야합니다.
              <AppDock
                kollectieType={KollectieType.ClassPattern}
                menuNodes={menu}
                activeMenuKey={activeMenuKey}
              />
            }
            subMenus={subMenus}
          >
            {this.props.children}                           // 마이크로 페이지 컴포넌트가 주입될 공간입니다.
          </KollectieLayoutView>
    ```



[가이드]
1. 위치
    - 레이아웃 : ~/src/pages/kollectie/layout/KollectieLayout/KollectieLayout.tsx
    - 메뉴 : ~/src/pages/kollectie/layout/KollectieLayout/menus.tsx
2. 개발 영역 :
    - 메뉴 정의
    - 레이아웃 view 




#### 5. AppDock
AppDock은 마이크로 앱의 상단에 고정적인 공간이며 반드시 Transit 컴포넌트를 포함합니다.
AppDock에서는 다음과 같은 요소가 고려되어야합니다.
1. 최상위 메뉴 정보 구성
2. Transit 컴포넌트 포함
```typescript jsx
      <AppDockView
        kollectieType={kollectieType}
        menus={menuNodes}
        activeMenuKey={activeMenuKey}
      >
        <Transit />
      </AppDockView>
```


[가이드]
1. 위치 : ~/src/comp/essential/dockingStation/AppDock
2. 개발 영역 :
    - AppDock view 



#### 6. Transit
Transit은 환승이라는 의미로 Tenant 공간 간 이동을 담당하는 컴포넌트로서 매우 중요합니다.
이때 Tenant 공간은 접근 가능한 모든 Cineroom과 현재 Cineroom에서 접근 가능한 모든 Cast 입니다.
Transit 다음과 같은 요소가 고려되어야합니다.
1. 접근 가능한 공간 정보 수집
메뉴 정보가 MenuNode 타입으로 제공된 것과 같이 공간 정보는 DockNode 타입으로 제공됩니다.
```typescript
class DockNode {
  //
  ancestorId: string;    // 해당 노드가 Cineroom이라면 PavilionId, Cast라면 TroupeId가 저장됩니다.
  ancestorName: string;  // ancestorId와 동일합니다.
  id: string;
  name: string;
  // 해당 노드를 클릭했을 때 발생하는 액션은 미리 정의됩니다. (Nara 서비스 호출)
  // 마이크로 앱에서는 해당 액션을 실행만 합니다.
  onClickAction: (id: string) => Promise<void>; 
}
```
위의 타입을 기반으로 dock으로부터 접근 가능한 공간 정보를 가져올 수 있습니다.
```typescript jsx
const { dock } = this.injected;
const { accessibleCinerooms, accessibleCasts } = dock;
```
2. 뷰를 위한 공간정보 그룹핑
접근 가능한 공간 정보를 알더라도 필요에 따라 화면에서 해당 공간들을 그룹핑하여 보여주어야합니다.
즉, 현재 접속한 Cineroom에 접근 가능한 Cast 가 10개가 있을 때 Cast는 각각 서로 다른 Troupe에 속할 수 있으므로 Troupe을 기준으로 그룹핑한 Cast 정보가 필요합니다.
DockNodeGroup 모델이 이러한 기능을 지원합니다.
```typescript
class DockNodeGroup {
  //
  ancestorId: string;  
  ancestorName: string; 
  dockNodes: DockNode[];
}
```
byScattered(nodes: DockNode[]) 에 의해 새성이 가능합니다.
```typescript jsx
const cineroomGroupByPavilion = DockNodeGroup.byScattered(accessibleCinerooms);
const castGroupByTroupe = DockNodeGroup.byScattered(accessibleCasts);
```

[가이드]
1. 위치 : ~/src/comp/essential/dockingStation/Transit
2. 개발 영역 :
    - Transit view 







