import { keeper as dockStateKeeper } from '@nara.platform/dock';

import { store as stageStore } from '@nara.drama/stage';
import { keeper as dashboardStore } from '@nara.drama/dashboard';
import { keeper as gitmateStore } from '@nara.drama/gitmate';
import { keeper as taskStore } from '@nara.drama/task';
import { keeper as learningLogStore } from '@nara.drama/learninglog';
import { keeper as qnaStore } from '@nara.drama/qna';


export default {
  ...dockStateKeeper,
  ...stageStore,
  dashboard: {
    ...dashboardStore,
  },
  ...taskStore,
  gitmate: {
    ...gitmateStore,
  },
  learninglog: {
    ...learningLogStore,
  },
  qna: {
    ...qnaStore,
  },
};

