import React from 'react';
import { ReactComponent, autobind } from '@nara.drama/prologue';
import { List, Popover } from '@nara.drama/material-ui';
import { WithStyles, withStyles } from './style';


export enum ProfileMenuType {
  Profile= 'Profile',
}


interface Props extends WithStyles {
  targetElement: HTMLElement | null;
  onClose: () => void;
  onClickProfileMenuItem: (event: React.MouseEvent<HTMLLIElement>, value: ProfileMenuType) => void;
}

interface State {
  activeMenu: string;
}


@autobind
class ProfileMenuView extends ReactComponent<Props, State> {
  //
  state: State = {
    activeMenu: '',
  };

  onClickListItem(event: React.MouseEvent<HTMLLIElement>, value: ProfileMenuType) {
    //
    const { onClickProfileMenuItem } = this.props;

    this.setState({ activeMenu: value }, () => onClickProfileMenuItem(event, value));
  }

  render() {
    //
    const { activeMenu } = this.state;
    const { classes, targetElement, onClose } = this.props;
    const open = Boolean(targetElement);

    return (
      <Popover
        open={open}
        anchorEl={targetElement}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        PaperProps={{
          className: classes.root,
        }}
        onClose={onClose}
      >
        <List dense>
          <List.Item
            button
            selected={activeMenu === ProfileMenuType.Profile}
            onClick={(e: any) => this.onClickListItem(e, ProfileMenuType.Profile)}
          >
            <List.ItemText primary="My Profile" />
          </List.Item>
        </List>
      </Popover>
    );
  }
}

export default withStyles(ProfileMenuView);
