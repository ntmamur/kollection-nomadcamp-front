
import React from 'react';
import { ReactComponent, autobind } from '@nara.drama/prologue';
import { List, Popover } from '@nara.drama/material-ui';

import { WithStyles, withStyles } from './style';


export enum ShouterMenuType {
  Register = 'Register',
  List = 'List',
}


interface Props extends WithStyles {
  targetElement: HTMLElement | null;
  onClose: () => void;
  onClickShouterMenuItem: (event: React.MouseEvent<HTMLLIElement>, value: ShouterMenuType) => void;
}

interface State {
  activeMenu: string;
}


@autobind
class ShouterMenuView extends ReactComponent<Props, State> {
  //
  state: State = {
    activeMenu: '',
  };

  onClickListItem(event: React.MouseEvent<HTMLLIElement>, value: ShouterMenuType) {
    //
    const { onClickShouterMenuItem } = this.props;

    this.setState({ activeMenu: value }, () => onClickShouterMenuItem(event, value));
  }

  render() {
    //
    const { activeMenu } = this.state;
    const { classes, targetElement, onClose } = this.props;
    const open = Boolean(targetElement);

    return (
      <Popover
        open={open}
        anchorEl={targetElement}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        PaperProps={{
          className: classes.root,
        }}
        onClose={onClose}
      >
        <List dense>
          <List.Item
            button
            selected={activeMenu === ShouterMenuType.Register}
            onClick={(e: any) => this.onClickListItem(e, ShouterMenuType.Register)}
          >
            <List.ItemText primary="Register Report" />
          </List.Item>
          <List.Item
            button
            selected={activeMenu === ShouterMenuType.List}
            onClick={(e: any) => this.onClickListItem(e, ShouterMenuType.List)}
          >
            <List.ItemText primary="Report List" />
          </List.Item>
        </List>
      </Popover>
    );
  }
}

export default withStyles(ShouterMenuView);
