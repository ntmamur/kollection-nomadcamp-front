import React from 'react';
import { ReactComponent, autobind } from '@nara.drama/prologue';

import { AssignmentInd, SettingsRounded } from '@material-ui/icons';
import { List, Popover, Collapse } from '@nara.drama/material-ui';
import { WithStyles, withStyles } from './style';
import ThemeMenu from '../ThemeMenu';


interface Props extends WithStyles {
  targetElement: HTMLElement | null;
  onClose: () => void;
  onClickMyActivity: () => void;
}

interface State {
  themeSettingsOpen: boolean;
}


@autobind
class MoreMenuView extends ReactComponent<Props, State> {
  //
  state: State = {
    themeSettingsOpen: false,
  };

  onClickThemeSettings() {
    //
    this.setState((prevState) => ({
      themeSettingsOpen: !prevState.themeSettingsOpen,
    }));
  }

  render() {
    //
    const { classes, targetElement, onClose, onClickMyActivity } = this.props;
    const { themeSettingsOpen } = this.state;
    const open = Boolean(targetElement);

    return (
      <Popover
        open={open}
        anchorEl={targetElement}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        PaperProps={{
          className: classes.root,
        }}
        onClose={onClose}
      >
        <List dense>
          <List.Item
            button
            onClick={onClickMyActivity}
          >
            <List.ItemIcon>
              <AssignmentInd fontSize="small" />
            </List.ItemIcon>
            <List.ItemText>
              My Activity
            </List.ItemText>

          </List.Item>
          <List.Item
            button
            onClick={this.onClickThemeSettings}
          >
            <List.ItemIcon>
              <SettingsRounded />
            </List.ItemIcon>
            <List.ItemText>
              Theme
            </List.ItemText>
            <Collapse in={themeSettingsOpen} timeout="auto">
              <ThemeMenu />
            </Collapse>
          </List.Item>
        </List>
      </Popover>
    );
  }
}

export default withStyles(MoreMenuView);
