import {
  createStyles,
  Theme,
  WithStyles as MuiWithStyles,
  withStyles as muiWithStyles,
} from '@material-ui/core/styles';


const style = (theme: Theme) => createStyles({
  root: {
    width: '250px',
  },
});

export default style;
export type WithStyles = MuiWithStyles<typeof style>;
export const withStyles = muiWithStyles(style);
