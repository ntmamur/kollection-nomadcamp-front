import React from 'react';
import { ReactComponent, autobind } from '@nara.drama/prologue';

import { AppContext } from '@nara.drama/prologue-util';
import { Button, Divider, List } from '@nara.drama/material-ui';
import { ThemeType } from '~/comp/view';
import { WithStyles, withStyles } from './style';


interface Props extends WithStyles {
}

interface State {
  theme: string;
}


@autobind
class ThemeMenuView extends ReactComponent<Props, State> {
  //
  static contextType = AppContext.Context;

  context!: React.ContextType<typeof AppContext.Context>;

  state: State = {
    theme: '',
  };


  componentDidMount() {
    //
    const { value } = this.context.settings;

    this.setState({
      theme: value.theme,
    });
  }

  onClickListItem(event: React.MouseEvent<HTMLLIElement>, value: string) {
    //
    event.stopPropagation();
    this.setState({
      theme: value,
    });
  }

  onClickApply() {
    //
    const { settings } = this.context;
    const { theme } = this.state;

    settings.setValue({
      ...settings.value,
      theme,
    });
  }

  render() {
    //
    const { theme } = this.state;

    return (
      <>
        <List dense>
          <List.Item
            button
            selected={theme === ThemeType.Light}
            onClick={(e: any) => this.onClickListItem(e, ThemeType.Light)}
          >
            <List.ItemText primary="Light" />
          </List.Item>
          <List.Item
            button
            selected={theme === ThemeType.OneDark}
            onClick={(e: any) => this.onClickListItem(e, ThemeType.OneDark)}
          >
            <List.ItemText primary="Dark" />
          </List.Item>
          <List.Item
            button
            selected={theme === ThemeType.Unicorn}
            onClick={(e: any) => this.onClickListItem(e, ThemeType.Unicorn)}
          >
            <List.ItemText primary="Unicorn" />
          </List.Item>
        </List>
        <Divider />

        <List dense>
          <List.Item>
            <Button fullWidth color="primary" onClick={this.onClickApply}>
              Apply
            </Button>
          </List.Item>
        </List>
      </>
    );
  }
}

export default withStyles(ThemeMenuView);
