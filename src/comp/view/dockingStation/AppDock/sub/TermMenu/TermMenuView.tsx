import React from 'react';
import { autobind, fetchFromTermsStorage, ReactComponent, saveToTermsStorage, TermType } from '@nara.drama/prologue';

import { Button, List, ListItem, ListItemText, Popover } from '@material-ui/core';
import { WithStyles, withStyles } from './style';


interface Props extends WithStyles {
  targetElement: HTMLElement | null;
  onClose: () => void;
}

interface State {
  termType: TermType;
}


@autobind
class TermMenuView extends ReactComponent<Props, State> {
  //
  state: State = {
    termType: fetchFromTermsStorage(),
  };


  componentDidMount() {
    //
    this.setState({
      termType: fetchFromTermsStorage(),
    });
  }

  onClickListItem(event: React.MouseEvent<HTMLLIElement>, value: TermType) {
    //
    this.setState({
      termType: value,
    });
  }

  onClickApply() {
    //
    const { onClose } = this.props;
    const { termType } = this.state;

    onClose();
    saveToTermsStorage(termType);
    window.location.reload();
  }

  render() {
    //
    const { classes, targetElement, onClose } = this.props;
    const { termType } = this.state;
    const open = Boolean(targetElement);

    return (
      <Popover
        open={open}
        anchorEl={targetElement}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        PaperProps={{
          className: classes.root,
        }}
        onClose={onClose}
      >
        <List dense>
          <ListItem
            button
            selected={termType === TermType.Korean}
            onClick={(e: any) => this.onClickListItem(e, TermType.Korean)}
          >
            <ListItemText primary="헌국어" />
          </ListItem>
          <ListItem
            button
            selected={termType === TermType.English}
            onClick={(e: any) => this.onClickListItem(e, TermType.English)}
          >
            <ListItemText primary="English" />
          </ListItem>

          <ListItem
            button
            selected={termType === TermType.Russian}
            onClick={(e: any) => this.onClickListItem(e, TermType.Russian)}
          >
            <ListItemText primary="Русский" />
          </ListItem>
        </List>

        <List dense>
          <ListItem>
            <Button fullWidth color="primary" onClick={this.onClickApply}>
              Apply
            </Button>
          </ListItem>
        </List>
      </Popover>
    );
  }
}

export default withStyles(TermMenuView);
