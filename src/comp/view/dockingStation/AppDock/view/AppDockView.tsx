import React from 'react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { MenuNode } from '@nara.platform/dock';
import { WithStyles, withStyles } from './style';
import { JsonViewer, KollectieType } from '~/nara';


interface Props {
  kollectieType: KollectieType;
  activeMenuKey: string;
  menus?: MenuNode[];
}

interface InjectedProps extends WithStyles {

}

@autobind
class AppDockView extends ReactComponent<Props, {}, InjectedProps> {
  //
  static defaultProps = {
    menus: [],
  };

  render() {
    //
    const { kollectieType, activeMenuKey, menus } = this.propsWithDefault;
    const { classes } = this.injected;

    return (
      <div style={{ paddingLeft: '100px', paddingTop: '100px' }}>
        <h1>
          [AppDockView] Customize design by below information...
        </h1>
        <JsonViewer title={'KollectieType'} data={kollectieType} />
        <JsonViewer title={'ActiveMenuKey'} data={activeMenuKey} />
        <JsonViewer title={'Visible Menu'} data={menus} />
        {this.props.children}
      </div>
    );
  }
}

export default withStyles(AppDockView);
