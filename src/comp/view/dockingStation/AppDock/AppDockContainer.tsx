import React from 'react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { MenuNode } from '@nara.platform/dock';
import { KollectieType } from '~/nara';
import AppDockView from './view/AppDockView';
import { Transit } from '~/comp/view/dockingStation/Transit';


interface Props {
  kollectieType: KollectieType;
  menuNodes: MenuNode[];
  activeMenuKey: string;
}


@autobind
class AppDockContainer extends ReactComponent<Props> {
  //
  render() {
    //
    const { kollectieType, menuNodes, activeMenuKey } = this.props;

    return (
      <AppDockView
        kollectieType={kollectieType}
        menus={menuNodes}
        activeMenuKey={activeMenuKey}
      >
        <Transit />
      </AppDockView>
    );
  }
}

export default AppDockContainer;
