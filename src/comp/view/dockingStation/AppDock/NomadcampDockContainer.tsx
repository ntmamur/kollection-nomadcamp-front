import React from 'react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { Box, Typography } from '@nara.drama/material-ui';
import LanguageIcon from '@material-ui/icons/Language';
import { MenuNode } from '@nara.platform/dock';
import { LayoutHeader } from '~/comp/view/layout';
import Transit from '../Transit/TransitContainer';
import TermMenu from './sub/TermMenu/TermMenuView';
import { WithStyles, withStyles } from './style';
import { KollectieType } from '~/nara';


interface Props extends WithStyles {
  kollectieType: KollectieType;
  menuNodes: MenuNode[];
  activeMenuKey: string;
  onClickLogo?: () => void;
  onClickMenu?: (menu: MenuNode) => void;
  onClickMyActivity?: () => void;
}

interface State {
  langButton: HTMLElement | null;
}

@autobind
class NomadcampDockContainer extends ReactComponent<Props, State> {
  //
  static defaultProps = {
    onClickLogo: () => {},
    onClickMenu: () => {},
    onClickMyActivity: () => {},
  };

  state: State = {
    langButton: null,
  };


  onOpenLangMenu(event: React.MouseEvent<HTMLElement>) {
    //
    this.setState({
      langButton: event.currentTarget,
    });
  }


  _mapStyleClassName(kollectieType: KollectieType) {
    //
    switch (kollectieType) {
      case KollectieType.Management:
        return 'management';
      case KollectieType.Classroom:
        return 'classroom';
      default:
        return 'classroom';
    }
  }

  onClickMenuItem(menu: MenuNode) {
    //
    const { onClickMenu } = this.propsWithDefault;

    onClickMenu(menu);
  }


  onCloseLangMenu() {
    //
    this.setState({
      langButton: null,
    });
  }

  render() {
    //
    const {
      classes,
      kollectieType,
      menuNodes,
      activeMenuKey,
    } = this.props;
    const { onClickLogo, onClickMyActivity } = this.propsWithDefault;
    const { langButton } = this.state;

    const className = this._mapStyleClassName(kollectieType);

    return (
      <LayoutHeader className={classes[className]}>
        <LayoutHeader.Logo>
          <Box
            onClick={onClickLogo}
            component="a"
          >
            <img src={`${process.env.BASE_PATH}/images/logo/namoosori_logo_x.svg`} className={classes.logo} alt="" />
          </Box>
          {
            kollectieType === KollectieType.Management && (
              <Box pl={2} className={classes.clt_tit}>
                <Typography component="h2">Management</Typography>
              </Box>
            )
          }
          {
            kollectieType === KollectieType.Classroom && (
              <Box pl={2} className={classes.clt_tit}>
                <Typography component="h2">Classroom</Typography>
              </Box>
            )
          }
        </LayoutHeader.Logo>

        <LayoutHeader.Menu className={classes.menu}>
          {
            menuNodes.map((menu) => (
              <Box
                key={`menu-item-${menu.label}`}
                component="a"
                className={classes.item}
                onClick={() => this.onClickMenuItem(menu)}
              >
                <Typography className={classes.nmenu}>
                  {menu.key === activeMenuKey ?
                    <b>{menu.label}</b>
                    :
                    menu.label
                  }
                </Typography>
              </Box>
            ))
          }
        </LayoutHeader.Menu>

        <LayoutHeader.RightMenu>
          <LayoutHeader.IconButton
            onClick={this.onOpenLangMenu}
          >
            <LanguageIcon />
          </LayoutHeader.IconButton>
          <TermMenu
            targetElement={langButton}
            onClose={this.onCloseLangMenu}
          />
          <Transit />
        </LayoutHeader.RightMenu>
      </LayoutHeader>
    );
  }
}

export default withStyles(NomadcampDockContainer);
