import React from 'react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { dock, WithDock } from '@nara.platform/dock';
import { SupervisorAccountRounded } from '@material-ui/icons';
import TransitView from './view/TransitView';
import { LayoutHeader } from '../../layout';


interface Props {
}

interface InjectedProps extends WithDock {
}

@dock([], [])
@autobind
class TransitContainer extends ReactComponent<Props, {}, InjectedProps> {
  //
  render() {
    //
    const { dock } = this.injected;
    const { accessibleCinerooms, accessibleTroupes, accessibleCasts } = dock;

    return (
      <TransitView
        accessibleCinerooms={accessibleCinerooms}
        accessibleTroupes={accessibleTroupes}
        accessibleCasts={accessibleCasts}
        icon={(
          <LayoutHeader.IconButton>
            <SupervisorAccountRounded />
          </LayoutHeader.IconButton>
        )}
      />
    );
  }
}

export default TransitContainer;
