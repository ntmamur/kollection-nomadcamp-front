
import React from 'react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { observer } from 'mobx-react';

import { Box, Typography, List, Divider, ListItemText, ListItem } from '@material-ui/core';
import { NameTextModel } from '@nara.drama/prologue-util';
import { DockNode, DockNodeGroup } from '@nara.platform/dock';


interface Props {
  items: DockNode[];
  title?: string;
  defaultActive?: string;
  onClickItem?: (item: NameTextModel) => void;
}

interface State {
  activeItem: string;
}

@autobind
@observer
class ContextMenuListContainer extends ReactComponent<Props, State> {
  //
  static defaultProps = {
    title: '',
    defaultActive: '',
  };

  state: State = {
    activeItem: '',
  };


  constructor(props: Props) {
    //
    super(props);

    this.state.activeItem = props.defaultActive || '';
  }

  onClickItem(menuItem: NameTextModel) {
    //
    const { onClickItem } = this.props;

    if (typeof onClickItem === 'function') {
      this.setState({
        activeItem: menuItem.name,
      });

      onClickItem(menuItem);
    }
  }

  isActive(menuItem: NameTextModel): boolean {
    //
    const { defaultActive } = this.props;
    const { activeItem } = this.state;

    if (defaultActive) {
      return activeItem === menuItem.name;
    }
    else {
      return !!menuItem.active;
    }
  }

  render() {
    //
    const { title, items, onClickItem } = this.props;

    return (
      <>
        <List dense>
          <Box ml={1}>
            <Typography variant="caption" color="textSecondary">{title}</Typography>
          </Box>
          {
            items.map((item, index) => (
              <ListItem
                key={`list-item-${index}`}
                button={typeof onClickItem === 'function' || true}
                // selected={this.isActive(item)}
                onClick={() => item.onClickAction(item.id)}
              >
                <ListItemText>{item.name}</ListItemText>
              </ListItem>
            ))
          }
        </List>

        <Divider />
      </>
    );
  }
}

export default ContextMenuListContainer;
