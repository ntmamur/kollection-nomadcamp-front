

interface MenuParams {
  onClose: () => void;
}

export default MenuParams;
