
import * as ContextMenuTypes from './type';
import ContextMenuContainer from './ContextMenuContainer';
import ContextMenuList from './sub-comp/ContextMenuList';


type ContextMenuComponent = typeof ContextMenuContainer & {
  List: typeof ContextMenuList;
};


const ContextMenu = ContextMenuContainer as ContextMenuComponent;

ContextMenu.List = ContextMenuList;

export default ContextMenu;
export { ContextMenuTypes };
