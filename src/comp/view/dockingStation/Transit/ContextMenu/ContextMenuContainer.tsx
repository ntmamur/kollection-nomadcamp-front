
import React from 'react';
import { autobind, ReactComponent, InvalidPropsException } from '@nara.drama/prologue';
import { observer } from 'mobx-react';

import { Button, List, Box, ListItem } from '@material-ui/core';
import { Popover } from '../../../layout';
import { MenuParams } from './model';


interface Props {
  trigger: React.ReactElement;
  onLogout?: (params: MenuParams) => void;
}

interface State {
  open?: boolean;
}

@autobind
@observer
class ContextMenuContainer extends ReactComponent<Props, State> {
  //
  static defaultProps = {
  };

  state: State = {
    open: undefined,
  };


  onClose() {
    //
    this.setState(
      { open: false },
      () => this.setState({ open: undefined }),
    );
  }

  onClickLogout() {
    //
    const { onLogout } = this.props;

    if (typeof onLogout !== 'function') {
      throw new InvalidPropsException('CineroomMenu', 'onLogout');
    }

    onLogout({
      onClose: this.onClose,
    });
  }

  renderContent() {
    //
    const { children, onLogout } = this.props;

    return (
      <Box minWidth={200}>
        {children}

        { typeof onLogout === 'function' ?
          <List dense>
            <ListItem>
              <Button fullWidth color="primary" onClick={this.onClickLogout}>
                Logout
              </Button>
            </ListItem>
          </List>
          :
          null
        }
      </Box>
    );
  }

  render() {
    //
    const { trigger } = this.props;
    const { open } = this.state;

    return (
      <Popover
        trigger={trigger}
        open={open}
        anchorOrigin={{
          horizontal: 'right',
          vertical: 'bottom',
        }}
        onClose={this.onClose}
      >
        {this.renderContent()}
      </Popover>
    );
  }
}

export default ContextMenuContainer;
