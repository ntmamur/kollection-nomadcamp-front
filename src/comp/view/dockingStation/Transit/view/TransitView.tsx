import React from 'react';
import { SupervisorAccountRounded } from '@material-ui/icons';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { DockNode } from '@nara.platform/dock';
import ContextMenu from '../ContextMenu';


interface Props {
  accessibleCinerooms: DockNode[];
  accessibleTroupes: DockNode[];
  accessibleCasts: DockNode[];
  icon?: React.ReactElement<{ onClick: any }>;
}

@autobind
class TransitView extends ReactComponent<Props> {
  //
  renderIcon() {
    //
    const { icon: iconProps } = this.props;
    const icon = iconProps || <SupervisorAccountRounded />;

    return React.cloneElement(icon, { onClick: this.onClickIcon });
  }

  async onClickIcon() {
    //
  }

  onLogout() {
    //

  }

  onClickCineroom() {

  }

  onClickKollection() {

  }

  render() {
    //
    const { accessibleCinerooms, accessibleTroupes, accessibleCasts } = this.props;
    console.log('accessibleCinerooms', accessibleCinerooms);
    console.log('accessibleTroupes', accessibleTroupes);
    console.log('accessibleCasts', accessibleCasts);
    return (
      <ContextMenu
        trigger={this.renderIcon()}
        onLogout={this.onLogout}
      >
        { accessibleCinerooms.length > 0 && (
          <ContextMenu.List
            title="Cineroom"
            items={accessibleCinerooms}
            onClickItem={this.onClickCineroom}
          />
        )}
        { accessibleTroupes.length > 0 && (
          <ContextMenu.List
            title="Troupe"
            items={accessibleTroupes}
            onClickItem={this.onClickKollection}
          />
        )}
        { accessibleCasts.length > 0 && (
          <ContextMenu.List
            title="Cast"
            items={accessibleCasts}
            onClickItem={this.onClickKollection}
          />
        )}
      </ContextMenu>
    );
  }
}

export default TransitView;
