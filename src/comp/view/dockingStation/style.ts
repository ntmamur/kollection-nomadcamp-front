
import { createStyles, Theme, WithStyles as MuiWithStyles, withStyles as muiWithStyles } from '@material-ui/core/styles';


const style = (theme: Theme) => createStyles({
  classroom: {
    backgroundColor: '#fff',
  },
  management: {
    backgroundColor: '#fff',
  },
  // member: {
  //   backgroundColor: '#fff',
  // },
  image: {
    height: '24px',
    marginRight: theme.spacing(1),
  },
  menu: {
    justifyContent: 'center',
    '& b': {
      color: '#ED5F65',
    },
  },
  nmenu: {
    color: '#000',
    fontSize: '1.25rem',
  },
  item: {
    fontSize: '14px',
    position: 'relative',
    marginRight: theme.spacing(6),
  },

  logo: {
    width: 'auto',
    marginTop: '4px',
    marginLeft: '30px',
    // '&:hover': {
    //   color: 'white',
    // },
  },
  clt_tit: {
    marginLeft: '25px',
    paddingLeft: '25px',
    borderLeft: '1px solid #dcdcdc',
    '& h2': {
      color: '#222',
      fontSize: '1.15rem',
      fontWeight: 'bold',
    },
  },


});

export default style;
export type WithStyles = MuiWithStyles<typeof style>;
export const withStyles = muiWithStyles(style);
