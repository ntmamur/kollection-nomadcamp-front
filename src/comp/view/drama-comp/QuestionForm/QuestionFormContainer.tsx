import React from 'react';
import { observer } from 'mobx-react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { Question, QuestionForm, Writer } from '@nara.drama/qna';
import { Button } from '@nara.drama/material-ui';
import { dock, WithDock } from '@nara.platform/dock';
import {QnaUtil} from "~/comp/view/shared/util";



interface Props {
  date: string;
  taskGroupName: string;
  onClick: (event: React.MouseEvent, question: Question) => void;
}


interface InjectedProps extends WithDock {
}


@dock()
@observer
@autobind
class QuestionFormContainer extends ReactComponent<Props, {}, InjectedProps> {
  //
  render() {
    //
    const {
      date,
      taskGroupName,
      onClick,
    } = this.props;
    const {
      currentAudienceId,
      currentActorId,
      currentActorName,
      // currentKollectionEditionId, // FIXME
    } = this.injected.dock;

    const currentKollectionEditionId = 'namoosori-nextree';
    const writer = new Writer(currentActorName, false);
    const groupTags = [ date, taskGroupName ];

    return (
      <QuestionForm
        clientAccountId={QnaUtil.genClientAccountId(currentActorId, currentKollectionEditionId)}
        groupTags={groupTags}
        writerId={currentActorId}
        writer={writer}
        maxLength={2000}
        audienceKey={currentAudienceId}
        secret={false}
        options={{
          onSubmit: onClick,
          customButtonContent: (onSubmit) => (
            <Button
              color="primary"
              variant="outlined"
              onClick={onSubmit}
            >
              완료
            </Button>
          ),
        }}
        hideTags
      />
    );
  }
}

export default QuestionFormContainer;
