import React from 'react';
import { IdName, autobind, ReactComponent, ServiceInjector } from '@nara.drama/prologue';
import { CommentConfig, CommentFeedback, CommentFeedbackCdo, CommentFeedbackStateKeeper } from '@nara.drama/feedback';
import { Box, Card, Container, Grid } from '@nara.drama/material-ui';
import dynamic from 'next/dynamic';
import { observer } from 'mobx-react';
import { PageLayout } from '~/comp/view';


const PostForm = dynamic<any>(
  () => import('@nara.drama/board').then(module => module.PostForm),
  { ssr: false }
);

const PostFormTitle = dynamic<any>(
  () => import('@nara.drama/board').then(module => module.PostForm)
    .then(postForm => postForm.Title),
  { ssr: false }
);

const PostFormContent = dynamic<any>(
  () => import('@nara.drama/board').then(module => module.PostForm)
    .then(postForm => postForm.Content),
  { ssr: false }
);

const PostFormToggleButtons = dynamic<any>(
  () => import('@nara.drama/board').then(module => module.PostForm)
    .then(postForm => postForm.ToggleButton),
  { ssr: false }
);

const PostFormButtons = dynamic<any>(
  () => import('@nara.drama/board').then(module => module.PostForm)
    .then(postForm => postForm.Buttons),
  { ssr: false }
);

interface Props {
  //
  boardId: string;
  currentCitizenKey: string;
  currentActorName: string;
  currentAudienceKey: string;
  onClickCancel: () => void;
  onSuccess: () => void;
}

interface InjectedProps {
  //
  commentFeedbackStateKeeper: CommentFeedbackStateKeeper;
}

@autobind
@observer
class PostRegistrationContainer extends ReactComponent<Props, {}, InjectedProps> {

  createCommentFeedback(postTitle: string, postId: string) {

    const { commentFeedbackStateKeeper } = this.injected;
    const newCommentFeedback = new CommentFeedback(postTitle, 0, postId);

    newCommentFeedback.config = CommentConfig.getDefaultConfig();
    newCommentFeedback.config.embeddedSubComment = true;
    newCommentFeedback.config.deletable = true;
    const newCommentFeedbackCdo = CommentFeedbackCdo.fromModel(newCommentFeedback);

    commentFeedbackStateKeeper.register(newCommentFeedbackCdo);
  }


  render() {
    //
    const { currentCitizenKey, currentActorName, currentAudienceKey, onClickCancel, onSuccess, boardId } = this.props;

    return (
      <Container>
        <PageLayout>
          <PageLayout.Body>
            <Box>
              <PostForm boardId={boardId} idName={new IdName(currentCitizenKey, currentActorName, '')}>
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <PostFormTitle />
                  </Grid>
                  <Grid item xs={12}>
                    <Card>
                      <Box p={2}>
                        <PostFormContent />
                      </Box>
                    </Card>
                  </Grid>
                  <Grid item xs={12}>
                    <Card>
                      <Box p={2}>
                        <PostFormToggleButtons />
                      </Box>
                    </Card>
                  </Grid>
                </Grid>
                <PostFormButtons
                  audienceKey={currentAudienceKey}
                  onSuccess={onSuccess}
                  onClickCancel={onClickCancel}
                  createCommentFeedback={this.createCommentFeedback}
                />
              </PostForm>
            </Box>
          </PageLayout.Body>
        </PageLayout>
      </Container>
    );
  }
}

export { PostRegistrationContainer };
export default ServiceInjector.withContext(
  CommentFeedbackStateKeeper,
)(PostRegistrationContainer);
