import React, { ReactComponentElement } from 'react';
import { observer } from 'mobx-react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { NextRouter } from 'next/router';
import { Box, Button, Container, Paper, Toolbar } from '@nara.drama/material-ui';
import { ExamType } from '@nara.drama/exam';
import MenuIcon from '@material-ui/icons/Menu';

import { sideBarRoute } from '~/pages/management/exam/routes';
import { ExamCategoryTab, ExamMenuItem, ExamSideBar } from './sub-comp';


interface Props {
  menuItem: ExamMenuItem;
  router: NextRouter;
  defaultActive: ExamType;

  questionComp?: ReactComponentElement<any>;
  quizComp?: ReactComponentElement<any>;
  codingComp?: ReactComponentElement<any>;
  assignmentComp?: ReactComponentElement<any>;
  interviewComp?: ReactComponentElement<any>;
}

interface State {
  sidebar: boolean;

}

@autobind
@observer
class ExamCoachLayoutContainer extends ReactComponent<Props, State> {
  //
  state: State ={
    sidebar: false,
  };

  sideBarRouting(item: ExamMenuItem) {
    const route = sideBarRoute(item);

    this.props.router.push(route.href, route.as);
  }

  onCloseSidebar() {
    this.setState({ sidebar: false });
  }

  tabbable() {
    return this.props.menuItem !== ExamMenuItem.None;
  }

  render() {
    //
    const {
      questionComp,
      quizComp,
      codingComp,
      assignmentComp,
      interviewComp,
      menuItem,
      defaultActive,
      router,
    } = this.props;

    return (
      <>
        <Toolbar />
        <Button.Icon style={{ position: 'absolute', zIndex: 1 }}>
          <MenuIcon onClick={() => this.setState({ sidebar: true })} />
        </Button.Icon>
        <ExamSideBar
          open={this.state.sidebar}
          onClose={this.onCloseSidebar}
          onRoute={this.sideBarRouting}
          menuItem={menuItem}
        />
        {
          this.tabbable()
          && (
            <Paper>
              <Container>
                <ExamCategoryTab
                  defaultActive={defaultActive}
                  router={router}
                  questionComp={questionComp}
                  quizComp={quizComp}
                  codingComp={codingComp}
                  assignmentComp={assignmentComp}
                  interviewComp={interviewComp}
                  menuItem={menuItem}
                />
              </Container>
            </Paper>
          )
          || <Box minHeight={'10px'} />
        }
        {this.props.children}
      </>
    );
  }
}

export default ExamCoachLayoutContainer;
