import React, { ReactComponentElement } from 'react';
import { observer } from 'mobx-react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { SmartTabsTypes } from '@nara.drama/prologue-util';
import { NextRouter } from 'next/router';

import { ExamType } from '@nara.drama/exam';
import { ExamMenuItem } from '~/comp/view/drama-comp/ExamCoachLayout';
import routes from '~/pages/management/exam/routes';
import { PageTab } from '~/comp/view';


interface Props {
  defaultActive: ExamType;
  menuItem: ExamMenuItem;
  router: NextRouter;

  questionComp?: ReactComponentElement<any>;
  quizComp?: ReactComponentElement<any>;
  codingComp?: ReactComponentElement<any>;
  assignmentComp?: ReactComponentElement<any>;
  interviewComp?: ReactComponentElement<any>;
}

interface State {
  active: ExamType;
}

@autobind
@observer
class ExamCategoryTabContainer extends ReactComponent<Props, State> {
  //
  static defaultProps = {
    routeActions: (examType: ExamType) => {},
  };

  state: State = {
    active: this.props.defaultActive,
  };

  onChangeTab(e: React.ChangeEvent<{}>, params: SmartTabsTypes.SmartTabParams) {
    const examType = params.activeValue;

    switch (examType) {
      case 'Question':
        this.setState({ active: ExamType.Question });
        this.registerRoute(ExamType.Question);
        break;
      case 'Quiz':
        this.setState({ active: ExamType.Quiz });
        this.registerRoute(ExamType.Quiz);
        break;
      case 'Coding':
        this.setState({ active: ExamType.Coding });
        this.registerRoute(ExamType.Coding);
        break;
      case 'Assignment':
        this.setState({ active: ExamType.Assignment });
        this.registerRoute(ExamType.Assignment);
        break;
      case 'Interview':
        this.setState({ active: ExamType.Interview });
        this.registerRoute(ExamType.Interview);
        break;
    }

  }

  registerRoute(examType: ExamType) {
    const { menuItem } = this.props;

    switch (menuItem) {
      case ExamMenuItem.BankRegister:
        this.bankRegisterRoute(examType);
        break;
      case ExamMenuItem.SheetRegister:
        this.sheetRegisterRoute(examType);
        break;
      case ExamMenuItem.TestRegister:
        this.testRegisterRoute(examType);
        break;
      default:
    }
  }

  bankRegisterRoute(examType: ExamType) {
    const { router } = this.props;
    let route;

    switch (examType) {
      case ExamType.Question:
      case ExamType.Quiz:
        route = routes.questionBankRegister();
        break;
      case ExamType.Coding:
        route = routes.codingBankRegister();
        break;
      case ExamType.Assignment:
        break;
      case ExamType.Interview:
        break;
    }

    if (route) {
      this.props.router.push(route.href, route.as);
    }
  }

  sheetRegisterRoute(examType: ExamType) {
    const { router } = this.props;
    let route;

    switch (examType) {
      case ExamType.Question:
        break;
      case ExamType.Quiz:
        route = routes.quizSheetRegister();
        break;
      case ExamType.Coding:
        route = routes.codingSheetRegister();
        break;
      case ExamType.Assignment:
        break;
      case ExamType.Interview:
        break;
    }

    if (route) {
      this.props.router.push(route.href, route.as);
    }
  }

  testRegisterRoute(examType: ExamType) {
    const { router } = this.props;
    let route;

    switch (examType) {
      case ExamType.Question:
        break;
      case ExamType.Quiz:
        break;
      case ExamType.Coding:
        route = routes.codingTestRegister();
        break;
      case ExamType.Assignment:
        break;
      case ExamType.Interview:
        break;
    }

    if (route) {
      this.props.router.push(route.href, route.as);
    }
  }

  componentDidUpdate(prevProps: Readonly<Props>, prevState: Readonly<State>, snapshot?: any) {
    if (prevProps && prevProps.defaultActive !== this.props.defaultActive) {
      // FIMXE
      // this.setState({ active: this.props.defaultActive });
    }
  }

  render() {
    //
    const { menuItem, questionComp, quizComp, codingComp, assignmentComp, interviewComp } = this.props;
    const { active } = this.state;
    return (
      <PageTab
        onChange={this.onChangeTab}
        activeValue={active}
      >
        {
          menuItem === ExamMenuItem.BankList || menuItem === ExamMenuItem.BankRegister ?
            <PageTab.Item value={ExamType.Question} label="문제">
              {questionComp}
            </PageTab.Item>
            :
            <>
              <PageTab.Item value={ExamType.Question} label="페이퍼">
                {questionComp}
              </PageTab.Item>
              {
                (menuItem === ExamMenuItem.SheetList || menuItem === ExamMenuItem.SheetRegister)
                && (
                  <PageTab.Item value={ExamType.Quiz} label="퀴즈">
                    {quizComp}
                  </PageTab.Item>
                )
              }

            </>
        }
        <PageTab.Item value={ExamType.Coding} label="코딩">
          {codingComp}
        </PageTab.Item>
        {/*<PageTab.Item value={ExamType.Assignment} label="과제">*/}
        {/*  {assignmentComp}*/}
        {/*</PageTab.Item>*/}
        {/*<PageTab.Item value={ExamType.Interview} label="인터뷰">*/}
        {/*  {interviewComp}*/}
        {/*</PageTab.Item>*/}
      </PageTab>
    );
  }
}

export default ExamCategoryTabContainer;
