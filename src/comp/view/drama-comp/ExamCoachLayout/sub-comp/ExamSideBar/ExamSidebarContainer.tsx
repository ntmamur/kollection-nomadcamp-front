import React from 'react';
import { observer } from 'mobx-react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { Box, Button, Divider, Drawer, List, Toolbar, Typography } from '@nara.drama/material-ui';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';

import { WithStyles, withStyles } from './style';


enum ExamMenuItem {
  BankRegister = 'BankRegister',
  BankList = 'BankList',
  SheetRegister = 'SheetRegister',
  SheetList = 'SheetList',
  TestRegister = 'TestRegister',
  TestList = 'TestList',
  None = 'None',
}

export { ExamMenuItem };

interface Props extends WithStyles {
  open: boolean;
  onClose: () => void;
  onRoute: (item: ExamMenuItem) => void;
  menuItem: ExamMenuItem;
}

@autobind
@observer
class ExamSidebarContainer extends ReactComponent<Props> {
  //

  correct(item: ExamMenuItem) {
    const { menuItem } = this.props;
    return menuItem === item;
  }

  render() {
    //
    const { classes, open, onClose, onRoute } = this.props;

    return (
      <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="left"
        open={open}
        paperClasses={classes.paper}
        elevation={3}
      >
        {/*<Toolbar/>*/}
        <Toolbar />
        <Box className={classes.drawerHeader} onClick={() => onClose()}>
          <Button.Icon size={'small'}>
            <KeyboardArrowLeftIcon />
          </Button.Icon>
        </Box>
        <Box p={2} className={classes.drawerPaper}>
          <List>
            <List.Subheader className={classes.listHeader}>
              <Typography variant={'h5'} bold>문제 은행</Typography>
            </List.Subheader>
            <List.Item
              className={classes.listItem}
              selected={this.correct(ExamMenuItem.BankRegister)}
              button
              onClick={() => onRoute(ExamMenuItem.BankRegister)}
            >
              <Typography variant={'subtitle2'} bold>등록</Typography>
            </List.Item>
            <List.Item
              className={classes.listItem}
              selected={this.correct(ExamMenuItem.BankList)}
              button
              onClick={() => onRoute(ExamMenuItem.BankList)}
            >
              <Typography variant={'subtitle2'} bold>목록</Typography>
            </List.Item>

            <br />
            <Divider />
            <br />

            <List.Subheader className={classes.listHeader}>
              <Typography variant={'h5'} bold>시트 관리</Typography>
            </List.Subheader>
            <List.Item
              className={classes.listItem}
              selected={this.correct(ExamMenuItem.SheetRegister)}
              button
              onClick={() => onRoute(ExamMenuItem.SheetRegister)}
            >
              <Typography variant={'subtitle2'} bold>등록</Typography>
            </List.Item>
            <List.Item
              className={classes.listItem}
              selected={this.correct(ExamMenuItem.SheetList)}
              button
              onClick={() => onRoute(ExamMenuItem.SheetList)}
            >
              <Typography variant={'subtitle2'} bold>목록</Typography>
            </List.Item>

            <br />
            <Divider />
            <br />

            <List.Subheader className={classes.listHeader}>
              <Typography variant={'h5'} bold>테스트 관리</Typography>
            </List.Subheader>
            <List.Item
              className={classes.listItem}
              selected={this.correct(ExamMenuItem.TestRegister)}
              button
              onClick={() => onRoute(ExamMenuItem.TestRegister)}
            >
              <Typography variant={'subtitle2'} bold>등록</Typography>
            </List.Item>
            <List.Item
              className={classes.listItem}
              selected={this.correct(ExamMenuItem.TestList)}
              button
              onClick={() => onRoute(ExamMenuItem.TestList)}
            >
              <Typography variant={'subtitle2'} bold>목록</Typography>
            </List.Item>
          </List>
        </Box>
      </Drawer>
    );
  }
}

export default withStyles(ExamSidebarContainer);
