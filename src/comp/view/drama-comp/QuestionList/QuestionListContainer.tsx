import React from 'react';
import { observer } from 'mobx-react';
import { autobind, injectFromName, ReactComponent } from '@nara.drama/prologue';
import {
  MyQuestionDetail,
  Question,
  QuestionList,
  QuestionListTypes,
  QuestionForm,
  QuestionStateKeeper,
} from '@nara.drama/qna';
import { List, Button, Box } from '@nara.drama/material-ui';
import { Divider, Typography } from '@material-ui/core';
import { dialogUtil } from '@nara.drama/prologue-util';
import { dock, WithDock } from '@nara.platform/dock';
import { NWithStyles, nWithStyles } from '~/comp/view/theme';
import {QnaUtil} from "~/comp/view/shared/util";
import {Modal} from "~/comp/view/comp";
import {Editor} from "~/comp/view/comp";


interface Props extends NWithStyles {
  useEmptyList?: boolean;
  groupTags?: string[];
  addQuestionOption?: QuestionListTypes.AddQuestionOption;
  onClickSave?: (event: React.MouseEvent, question: Question, index: number) => void;
  onClickDelete?: (question: Question, index: number) => void;
}

interface State {
  open: boolean;
  selectedQuestion: Question | null;
  selectedIndex: number;
  refreshTrigger: boolean;
}

interface InjectedProps extends WithDock {
  questionStateKeeper: QuestionStateKeeper;
}

@dock()
@injectFromName(QuestionStateKeeper.serviceName)
@observer
@autobind
class QuestionListContainer extends ReactComponent<Props, State, InjectedProps> {
  //
  static defaultProps = {
    onClickSave: () => {},
    onClickDelete: () => {},
  };

  constructor(props: Props) {
    super(props);

    this.state = { open: false, selectedQuestion: null, refreshTrigger: false, selectedIndex: 0 };
  }

  onClickQuestion(event: React.MouseEvent, question: Question, index: number) {
    //
    this.showQuestion(question, index);
  }

  showQuestion(question: Question, index: number) {
    //
    const newQuestion: Question = Question.fromDomain(question);

    this.setState({ open: true, selectedQuestion: newQuestion, selectedIndex: index });
  }

  hideQuestion() {
    //
    this.setState({ open: false });
  }

  editQuestion() {
    //
    const { selectedQuestion } = this.state;

    if (!selectedQuestion) {
      return;
    }

    selectedQuestion.editing = true;
    this.setState({ selectedQuestion });
  }

  async onClickSave(event: React.MouseEvent, question: Question) {
    //
    const { onClickSave } = this.propsWithDefault;
    const { selectedIndex } = this.state;
    const { questionStateKeeper } = this.injected;

    if (!question.id) {
      await onClickSave(event, question, selectedIndex);
    }
    else {
      const res = await questionStateKeeper.save(question);

      this.refreshQuestionList();
    }

    this.hideQuestion();
  }

  async onDeleteQuestion(question: Question) {
    //
    const { onClickDelete } = this.propsWithDefault;
    const { selectedIndex } = this.state;
    const { questionStateKeeper } = this.injected;

    const confirmed = await dialogUtil.confirm({ message: '질문을 삭제하시겠습니까?<br />삭제 된 질문은 복구 될 수 없습니다.' });

    if (!confirmed) {
      return;
    }

    if (!question.id) {
      await onClickDelete(question, selectedIndex);
    }
    else {
      const res = await questionStateKeeper.remove(question.id);

      this.refreshQuestionList();
    }

    this.hideQuestion();
  }

  refreshQuestionList() {
    //
    const { refreshTrigger } = this.state;

    this.setState({ refreshTrigger: !refreshTrigger });
  }

  renderMyQuestionAction(question: Question) {
    //
    return (
      <Box ml={2}>
        <Button variant="outlined" onClick={this.editQuestion}>수정</Button>
        <Button variant="outlined" onClick={() => this.onDeleteQuestion(question)}>삭제</Button>
      </Box>
    );
  }

  render() {
    //
    const { addQuestionOption, groupTags } = this.props;
    const { classes } = this.props;
    const { selectedQuestion, open, refreshTrigger } = this.state;

    // FIXME
    const {
      currentActorId,
      // currentKollectionEditionId,
    } = this.injected.dock;
    const currentKollectionEditionId = 'namoosori-nextree';

    return (
      <>
        <QuestionList
          clientAccountId={QnaUtil.genClientAccountId(currentActorId, currentKollectionEditionId)}
          groupTags={groupTags}
          readerId={currentActorId}
          addQuestionOption={addQuestionOption}
          refreshTrigger={refreshTrigger}
          renderChildren={(questions: Question[]) => {
            if (questions.length > 0) {
              return (
                <List component="nav" aria-label="secondary mailbox folder">
                  {
                    questions.map((question: Question, index: number) => (
                      <List.Item
                        key={'question-' + index}
                        button
                        onClick={(event: React.MouseEvent) => this.onClickQuestion(event, question, index)}
                      >
                        <List.ItemText
                          primary={question.title}
                          primaryTypographyProps={{
                            style: {
                              overflow: 'hidden',
                              textOverflow: 'ellipsis',
                              whiteSpace: 'nowrap',
                            },
                          }}
                        />
                      </List.Item>
                    ))
                  }
                </List>
              );
            }
            else {
              return <Box p={2}><Typography align="center">질문을 작성해 주세요.</Typography></Box>;
            }
          }}
        />
        {
          selectedQuestion ? (
            <Modal
              maxWidth="md"
              open={open}
              onClose={this.hideQuestion}
            >
              <Modal.Content>
                <MyQuestionDetail question={selectedQuestion}>
                  <MyQuestionDetail.Header
                    onEdit={this.editQuestion}
                    renderAction={this.renderMyQuestionAction}
                  />
                  <MyQuestionDetail.Content
                    hideTags
                    renderEditingForm={(question: Question) => (
                      <QuestionForm
                        clientAccountId={question.clientAccountId}
                        writerId={question.writerId}
                        questionId={question.id}
                        question={question}
                        secret={false}
                        options={{
                          onSubmit: this.onClickSave,
                          customButtonContent: (onSubmit) => (
                            <Button
                              color="primary"
                              variant="outlined"
                              onClick={onSubmit}
                            >
                              수정
                            </Button>
                          ),
                        }}
                        hideTags
                      />
                    )}
                    renderContent={(question: Question) => {
                      if (!question.secret && question.answerSummary) {
                        return (
                          <>
                            <Divider className={classes.ntHrspc} />
                            <MyQuestionDetail.AnswerSummary
                              renderQuestion={(question: Question) => (
                                <Editor
                                  readOnly
                                  contentId={question.id}
                                  content={question.answerMessage}
                                  height={475}
                                />
                              )}
                            />
                          </>
                        );
                      }
                      else {
                        return null;
                      }
                    }}
                  />
                </MyQuestionDetail>
              </Modal.Content>
            </Modal>
          ) : null
        }

      </>
    );
  }
}

export default nWithStyles(QuestionListContainer);
