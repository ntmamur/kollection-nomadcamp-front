import React from 'react';
import { observer } from 'mobx-react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { ValidationForm } from '@nara.drama/prologue-util';
import { Button, FormControl, Grid, Typography } from '@nara.drama/material-ui';
import { Lecture, LessonPlan, LessonPlanSelector } from '@nara.drama/course';
import { Troupe } from '@nara.drama/stage';
import { Modal } from '~/comp/view';


interface Props {
  trigger: React.ReactElement;
  subjectId: string;
  troupeId: string;
  findLectureByLessonPlanIdAndClassId: (lessonPlanId: string, classId: string) => Promise<Lecture | null>;
  onSave: () => void;
  onClose: () => void;
}


@autobind
@observer
class LectureFormModalView extends ReactComponent<Props> {
  //
  render() {
    //
    const { trigger, subjectId, troupeId, findLectureByLessonPlanIdAndClassId, onSave, onClose } = this.props;

    return (
      <Modal
        trigger={trigger}
        maxWidth="md"
        onClose={onClose}
      >
        <Modal.Header>Lesson 개설</Modal.Header>
        <Modal.Content>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Typography>* 주의 *</Typography>
              <Typography>저장된 기획을 기준으로 lesson이 개설됩니다. 저장되지 않은 데이터가 있으면 저장 후 개설해주세요.</Typography>
            </Grid>
            <Grid item xs={12}>
              <FormControl.Label>lesson 선택</FormControl.Label>
            </Grid>
            <Grid item xs={12}>
              <ValidationForm.Field name="lessonPlanId">
                {({ form, field, error, helperText }) => (
                  <LessonPlanSelector
                    fullWidth
                    subjectId={subjectId}
                    disabled={!!field.value}
                    error={error}
                    helperText={helperText}
                    onSelect={async (lessonPlan: LessonPlan) => {
                      form.setFieldValue('lessonPlanId', lessonPlan.id, true);
                      form.setFieldValue('lectureId', '', true);
                    }}
                  />
                )}
              </ValidationForm.Field>
            </Grid>
            <ValidationForm.Field name="lectureId">
              {({ form, field, error, helperText }) => {
                const lessonPlanId = form.getFieldProps('lessonPlanId').value;

                return lessonPlanId && (
                  <>
                    <Grid item xs={12}>
                      <FormControl.Label>그룹 선택</FormControl.Label>
                    </Grid>
                    <Grid item xs={12}>
                      {/*FIXME*/}
                      {/*<TroupeSelector*/}
                      {/*  stageKey={stageKey}*/}
                      {/*  fullWidth*/}
                      {/*  error={error}*/}
                      {/*  helperText={helperText}*/}
                      {/*  onSelect={async (troupe: Troupe) => {*/}
                      {/*    const lecture = await findLectureByLessonPlanIdAndClassId(lessonPlanId, troupe.id);*/}
                      {/*    const lectureId = lecture ? lecture.id : '';*/}

                      {/*    form.setFieldValue('lectureId', lectureId, true);*/}
                      {/*  }}*/}
                      {/*/>*/}
                    </Grid>
                  </>
                );
              }}
            </ValidationForm.Field>
          </Grid>
        </Modal.Content>
        <Modal.Actions>
          <Modal.CloseButton>취소</Modal.CloseButton>
          <Button type="submit" color="primary" onClick={onSave}>개설</Button>
        </Modal.Actions>
      </Modal>
    );
  }
}

export default LectureFormModalView;
