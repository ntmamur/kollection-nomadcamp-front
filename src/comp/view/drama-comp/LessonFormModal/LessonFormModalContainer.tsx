import React from 'react';
import { autobind, DramaException, ReactComponent, ServiceInjector } from '@nara.drama/prologue';
import { observer } from 'mobx-react';
import { LectureStateKeeper, LessonPlanStateKeeper, LessonStateKeeper, SubjectStateKeeper } from '@nara.drama/course';
import { dialogUtil, ValidationForm } from '@nara.drama/prologue-util';
import LessonFormModel from './model/LessonFormModel';
import LectureFormModalView from './view/LectureFormModalView';


interface Props {
  trigger: React.ReactElement;
  subjectId: string;
  troupeId: string;
  audienceId: string;
}

interface State {
  initialValues: LessonFormModel;
}

interface InjectedProps {
  lessonStateKeeper: LessonStateKeeper;
  lessonPlanStateKeeper: LessonPlanStateKeeper;
  subjectStateKeeper: SubjectStateKeeper;
  lectureStateKeeper: LectureStateKeeper;
}

@autobind
@observer
class LessonFormModalContainer extends ReactComponent<Props, State, InjectedProps> {
  //
  state: State = {
    initialValues: LessonFormModel.getDefault(),
  };

  validationSchema = LessonFormModel.getValidationSchema();

  async onStartLesson(lessonFormModel: LessonFormModel) {
    //
    const { audienceId } = this.props;
    const { lessonStateKeeper, lessonPlanStateKeeper } = this.injected;
    const { lessonPlan } = lessonPlanStateKeeper;

    if (!lessonPlan) {
      throw new DramaException('LessonFormModal', 'LessonPlan should not be null.');
    }

    const lesson = await lessonStateKeeper.findLessonByLectureIdAndLessonNo(lessonFormModel.lectureId, lessonPlan.lessonNo);

    if (lesson) {
      dialogUtil.alert({ title: '개설 안내', message: '이미 개설되어 있습니다.' });
      return;
    }

    const response = await lessonStateKeeper.start(
      lessonFormModel.lectureId,
      lessonFormModel.lessonPlanId,
      audienceId,
    );

    if (response.entityIds.length) {
      dialogUtil.alert({ title: '개설 성공', message: '개설되었습니다.' });
    } else {
      dialogUtil.alert({ title: '개설 실패', message: '개설 중 오류가 발생하였습니다. 다시 시도해주시기 바랍니다.' });
    }
  }

  async findLectureByLessonPlanIdAndClassId(lessonPlanId: string, classId: string) {
    //
    const { lessonPlanStateKeeper, lectureStateKeeper } = this.injected;
    const lessonPlan = await lessonPlanStateKeeper.findLessonPlanById(lessonPlanId);

    return lectureStateKeeper.findLectureByClassIdAndSubjectId(classId, lessonPlan.subjectId);
  }

  render() {
    //
    const { trigger, subjectId, troupeId } = this.props;
    const { initialValues } = this.state;

    if (!initialValues) {
      return null;
    }

    return (
      <ValidationForm
        initialValues={initialValues}
        validationSchema={this.validationSchema}
        onSubmit={this.onStartLesson}
      >
        {(formikProps) => (
          <LectureFormModalView
            trigger={trigger}
            subjectId={subjectId}
            troupeId={troupeId}
            findLectureByLessonPlanIdAndClassId={this.findLectureByLessonPlanIdAndClassId}
            onSave={formikProps.submitForm}
            onClose={formikProps.resetForm}
          />
        )}
      </ValidationForm>
    );
  }
}

export default ServiceInjector.withContext(
  LessonStateKeeper,
  LessonPlanStateKeeper,
  SubjectStateKeeper,
  LectureStateKeeper
)(LessonFormModalContainer);
