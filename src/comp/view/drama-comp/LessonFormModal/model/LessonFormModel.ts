import { object, string } from 'yup';


class LessonFormModel {
  lessonPlanId: string;
  lectureId: string;

  constructor(lessonPlanId: string, lectureId: string) {
    //
    this.lessonPlanId = lessonPlanId;
    this.lectureId = lectureId;
  }

  static getDefault(): LessonFormModel {
    //
    return new LessonFormModel(
      '',
      '',
    );
  }

  static getValidationSchema() {
    return object({
      lessonPlanId: string().required('개설할 lesson을 선택해주세요.'),
      lectureId: string().required('해당 그룹에 개설된 강좌가 없습니다. 강좌를 먼저 개설해주시기 바랍니다.'),
    });
  }
}

export default LessonFormModel;
