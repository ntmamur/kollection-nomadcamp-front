import React from 'react';
import { observer } from 'mobx-react';
import moment from 'moment';
import {autobind, ReactComponent, Terms} from '@nara.drama/prologue';
import { DailyCommentList, DailyLearningLogList, DailyLearningQuantity, Comment } from '@nara.drama/learninglog';
import { Question, QuestionList } from '@nara.drama/qna';
import { AccordionDetails, Box, Divider, Grid, Typography } from '@material-ui/core';
import { Search } from '@material-ui/icons';
import { TaskGroup } from '@nara.drama/task';
import { TaskGroupAutoSelector } from '~/comp/view/drama-comp';
import { NWithStyles, nWithStyles } from '../../theme';
import { Dictionary, TermKeys } from '~/comp/view/shared/terms';
import {DateUtil, QnaUtil} from "~/comp/view/shared/util";
import { Editor} from "~/comp/view/comp";
import { PageLayout} from "~/comp/view/layout";

interface Props extends NWithStyles {
  //
  actorId: string;
  troupeId: string;
  kollectionEditionId: string;
}

@autobind
@observer
class DailyReviewListContainer extends ReactComponent<Props> {
  //
  renderDetail(dailyLearningQuantity: DailyLearningQuantity | null) {
    //
    const { classes, actorId, troupeId, kollectionEditionId } = this.props;

    if (!dailyLearningQuantity || !dailyLearningQuantity.date) {
      return null;
    }

    return (
      <TaskGroupAutoSelector
        onlyInProgress
        date={dailyLearningQuantity.date.toMoment()}
        troupeId={troupeId}
      >
        {(selectedTaskGroup: TaskGroup | null, defaultSelect: React.ReactNode) => (
          <>
            <Grid container justify="flex-end">
              <Box width="450px" mr={5} mb={1}>
                {defaultSelect}
              </Box>
            </Grid>
            {selectedTaskGroup && (
              <>
                <AccordionDetails>
                  <Box display="block" pr={3} pl={3} overflow="auto" style={{ maxHeight: '700px', width: '100%' }}>
                    <DailyCommentList
                      date={dailyLearningQuantity.date ? dailyLearningQuantity.date.toMoment() : moment()}
                      traineeId={dailyLearningQuantity.trainee ? dailyLearningQuantity.trainee.id : ''}
                      taskGroupId={selectedTaskGroup.id}
                    >
                      <Divider />
                      <DailyCommentList.Content
                        renderComment={(comment: Comment) => (
                          <Editor
                            readOnly
                            contentId={comment.id}
                            content={comment.content}
                            height={475}
                          />
                        )}
                        emptyList={(
                          <Box pt={2} className={classes.nnotSmall} display="flex" justifyContent="center">
                            <Search fontSize="large" />&nbsp;&nbsp;코멘트가 없습니다.
                          </Box>
                        )}
                      />
                    </DailyCommentList>
                  </Box>
                </AccordionDetails>

                <QuestionList
                  readerId={actorId}
                  clientAccountId={QnaUtil.genClientAccountId(actorId, kollectionEditionId)}
                  groupTags={[
                    dailyLearningQuantity.date ? DateUtil.formatToString(dailyLearningQuantity.date.toMoment()) : '',
                    selectedTaskGroup.name,
                  ]}
                >
                  <>
                    <Divider className={classes.ntHrspc} />
                    <AccordionDetails>
                      <Box display="block" pr={3} pl={3} overflow="auto" style={{ maxHeight: '700px', width: '100%' }}>
                        <QuestionList.Content
                          renderQuestion={(question: Question) => (
                            <Editor
                              readOnly
                              contentId={question.id}
                              content={question.answerMessage}
                              height={475}
                            />
                          )}
                        />
                      </Box>
                    </AccordionDetails>
                  </>
                </QuestionList>
              </>
            )}
          </>
        )}
      </TaskGroupAutoSelector>
    );
  }

  render() {
    //
    const { actorId } = this.props;

    return (
      <DailyLearningLogList traineeId={actorId}>
        <PageLayout.Content>
          <Typography component="p">
            <DailyLearningLogList.ReviewCount />
            {Terms(Dictionary[TermKeys.ReviewCountMessage])}
          </Typography>
        </PageLayout.Content>
        <PageLayout.Content>
          <DailyLearningLogList.Content
            renderDetail={this.renderDetail}
          />
        </PageLayout.Content>
        <PageLayout.Content>
          <Box width="100%" display="flex" justifyContent="center">
            <DailyLearningLogList.MoreButton />
          </Box>
        </PageLayout.Content>
      </DailyLearningLogList>
    );
  }
}

export default nWithStyles(DailyReviewListContainer);
