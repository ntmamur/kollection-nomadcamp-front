import React from 'react';
import { observer } from 'mobx-react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { LessonPlanForm, LessonPlanFormTypes } from '@nara.drama/course';
import { QuizSet, QuizSetDetailCard, QuizSetList } from '@nara.drama/exam';
import { Box, Button, Divider } from '@nara.drama/material-ui';
import { Assignment } from '@material-ui/icons';
import { Modal, ModalTypes } from '~/comp/view';


interface Props {
  children: React.ReactNode;
  subjectId?: string;
  lessonPlanId?: string;
  audienceId: string;
  onSuccess?: () => void;
}

@autobind
@observer
class LessonPlanFormWithQuizFieldContainer extends ReactComponent<Props> {
  //
  renderQuizField(params: LessonPlanFormTypes.RenderQuizFieldParams) {
    //
    const { quizId, setQuizId } = params;

    return (
      <Modal trigger={<Button.Icon><Assignment /></Button.Icon>} maxWidth={'md'}>
        {(context: ModalTypes.ModalContextParams) => (
          <>
            <Modal.Header>Quiz 검색</Modal.Header>
            <Modal.Content>
              {
                quizId
                && <QuizSetDetailCard quizSetId={quizId} />
              }
              <QuizSetList>
                <QuizSetList.Header />
                <QuizSetList.Content
                  onClick={((event: React.MouseEvent, quizSet: QuizSet) => {
                    setQuizId(quizSet.id);
                    context.close();
                  })}
                />
                <QuizSetList.Pagination />
              </QuizSetList>
            </Modal.Content>
          </>
        )}
      </Modal>
    );
  }

  render() {
    //
    const { subjectId, lessonPlanId, audienceId, onSuccess, children } = this.props;

    return (
      <LessonPlanForm
        subjectId={subjectId}
        lessonPlanId={lessonPlanId}
        audienceKey={audienceId}
        onSuccess={onSuccess}
      >
        <LessonPlanForm.Form
          renderQuizField={this.renderQuizField}
        />
        <Box my={5}>
          <Divider />
        </Box>
        <LessonPlanForm.ExercisePlanForms />
        {children}
      </LessonPlanForm>
    );
  }
}

export default LessonPlanFormWithQuizFieldContainer;
