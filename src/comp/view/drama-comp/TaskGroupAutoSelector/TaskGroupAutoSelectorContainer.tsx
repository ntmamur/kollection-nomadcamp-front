import React from 'react';
import { observer } from 'mobx-react';
import moment, { Moment } from 'moment';
import { autobind, DramaException, ReactComponent, SortDirection } from '@nara.drama/prologue';
import { TaskGroup, TaskGroupSelector } from '@nara.drama/task';
import { nWithStyles, NWithStyles } from '../../theme';


interface Props extends NWithStyles {
  //
  value?: TaskGroup | null;
  onlyInProgress?: boolean;
  inProgressFirst?: boolean;
  date?: Moment;
  troupeId: string;
  onSelect?: (taskGroup: TaskGroup | null) => void;
  children?: (selectedTaskGroup: TaskGroup | null, defaultSelect: React.ReactNode) => React.ReactNode;
}

interface State {
  taskGroupCount: number;
  value: TaskGroup | null;
}


@autobind
@observer
class TaskGroupAutoSelectorContainer extends ReactComponent<Props, State> {
  //
  static defaultProps = {
    value: undefined,
    onlyInProgress: false,
    inProgressFirst: false,
    date: moment(),
    onSelect: () => {},
    children: (selectedTaskGroup: TaskGroup | null, defaultSelect: React.ReactNode) => defaultSelect,
  };

  state: State = {
    taskGroupCount: 0,
    value: null,
  };

  componentDidMount() {
    //
    this.verifyProps();
  }

  verifyProps() {
    //
    const { onlyInProgress, date } = this.props;

    if (onlyInProgress && !date) {
      throw new DramaException('InProgressTaskSelector', 'If onlyInProgress is true, date should be defined.');
    }
  }

  isControlled() {
    //
    return this.props.value === undefined;
  }

  getSelectedTaskGroup() {
    //
    const { value: valueProps } = this.props;
    const { value: valueState } = this.state;
    const taskGroup = this.isControlled() ? valueState : valueProps;

    return taskGroup || null;
  }

  onInit(taskGroups: TaskGroup[]) {
    //
    this.onSelect(taskGroups[0] || null);
    this.setState({ taskGroupCount: taskGroups.length });
  }

  onSelect(taskGroup: TaskGroup | null) {
    //
    if (this.isControlled()) {
      this.setState({ value: taskGroup });
    }
    else {
      this.propsWithDefault.onSelect(taskGroup);
    }
  }

  render() {
    //
    const { onlyInProgress, inProgressFirst, date, troupeId, children } = this.propsWithDefault;
    const { taskGroupCount } = this.state;
    const taskGroup = this.getSelectedTaskGroup();

    return (
      <TaskGroupSelector
        date={onlyInProgress ? date : undefined}
        classIds={[troupeId]}
        inProgress={onlyInProgress}
        sortingOption={{
          sortingField: 'creationTime',
          sortingDirection: SortDirection.Descending,
          inProgressFirst,
        }}
        onInit={this.onInit}
      >
        {!!taskGroupCount && children(this.getSelectedTaskGroup(),
          <TaskGroupSelector.Select
            textFieldProps={{
              value: taskGroup ? taskGroup.id : '',
              variant: 'outlined',
              fullWidth: true,
            }}
            onSelect={this.onSelect}
          />
        )}
      </TaskGroupSelector>
    );
  }
}

export default nWithStyles(TaskGroupAutoSelectorContainer);
