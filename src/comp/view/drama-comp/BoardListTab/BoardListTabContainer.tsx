import React from 'react';
import _ from 'lodash';
import { observer } from 'mobx-react';
import { Tabs } from '@nara.drama/material-ui';
import { autobind, DramaException, ReactComponent, ServiceInjector, IdName } from '@nara.drama/prologue';
import { Board, BoardCdo, BoardConfig, BoardsStateKeeper, BoardStateKeeper } from '@nara.drama/board';
import { SmartTabs, SmartTabsTypes } from '@nara.drama/prologue-util';


interface Props {
  cineroomId: string;
  castId: string;
  kollectionEditionId: string;
  actor: IdName;
  audienceId: string;
  onInitDefaultBoard?: (boardId: string, boardTitle: string) => void;
  onClick?: (boardId: string, boardTitle: string) => void;
}

interface InjectedProps {
  boardStateKeeper: BoardStateKeeper;
  boardsStateKeeper: BoardsStateKeeper;
}

@autobind
@observer
class BoardListTabContainer extends ReactComponent<Props, {}, InjectedProps> {
  //
  static defaultProps = {
    onInitDefaultBoard: () => {
    },
    onClick: () => {
    },
  };

  componentDidMount() {
    //
    this.init();
  }

  componentDidUpdate(prevProps: Readonly<Props>, prevState: Readonly<{}>, snapshot?: any) {
    //
    const { castId: prevCastId } = prevProps;
    const { castId } = this.props;

    if (!_.isEqual(prevCastId, castId)) {
      this.init();
    }
  }

  async init() {
    const { boardsStateKeeper } = this.injected;
    const { cineroomId, castId } = this.propsWithDefault;

    await this.getDefaultCineroomBoard();

    await boardsStateKeeper.findBoardsByGroupIds(cineroomId, ...castId);
  }

  async getDefaultCineroomBoard() {
    //
    const { cineroomId, kollectionEditionId, actor, audienceId, onInitDefaultBoard } = this.propsWithDefault;
    const { boardStateKeeper, boardsStateKeeper } = this.injected;

    const boards = await boardsStateKeeper.findBoardsByGroupId(cineroomId);
    let cineroomBoardId;
    let cineroomBoardTitle = '공지사항';

    if (boards.length === 0) {
      const config = new BoardConfig(true, true, true, true);
      const boardCdo = new BoardCdo(
        actor,
        config,
        cineroomBoardTitle,
        cineroomId,
        audienceId,
        kollectionEditionId
      );

      const response = await boardStateKeeper.registerBoard(boardCdo);

      cineroomBoardId = response.entityIds[0];
    }
    else {
      const cineroomBoard = this.getCineroomBoard();

      cineroomBoardId = cineroomBoard.id;
      cineroomBoardTitle = cineroomBoard.title;
    }

    onInitDefaultBoard(cineroomBoardId, cineroomBoardTitle);
  }

  onChange(event: React.ChangeEvent, params: SmartTabsTypes.SmartTabParams) {
    //
    const boardId = params.activeValue;

    this.propsWithDefault.onClick(boardId, this.getBoardTitleByBoardId(boardId));
  }

  getCineroomBoard(): Board {
    //
    const { cineroomId } = this.props;
    const { boards } = this.injected.boardsStateKeeper;
    const board = boards.find(board => board.groupId === cineroomId);

    if (!board) {
      throw new DramaException('BoardListTab', 'Board with the groupId should not be null.');
    }

    return board;
  }

  getBoardTitleByBoardId(boardId: string) {
    //
    const { boards } = this.injected.boardsStateKeeper;
    const board = boards.find(board => board.id === boardId);

    if (!board) {
      throw new DramaException('BoardListTab', 'Board with the id should not be null.');
    }

    return board.title;
  }

  render() {
    //
    const { boards } = this.injected.boardsStateKeeper;

    return boards.length > 1 ? (
      <SmartTabs
        defaultActiveValue={this.getCineroomBoard().id}
        onChange={this.onChange}
        renderTab={(params: SmartTabsTypes.RenderTabParams) => (
          <Tabs {...params} />
        )}
        renderTabItem={(params: SmartTabsTypes.RenderTabItemParams) => (
          <Tabs.Tab {...params} />
        )}
      >
        {boards.map((board, index) => (
          <SmartTabs.Item
            key={index}
            value={board.id}
            label={board.title}
          />
        ))}
      </SmartTabs>
    ) : null;
  }
}

export { BoardListTabContainer };
export default ServiceInjector.withContext(
  BoardStateKeeper,
  BoardsStateKeeper
)(BoardListTabContainer);
