import React from 'react';
import { observer } from 'mobx-react';
import { autobind, NameValue, NameValueList, ReactComponent, ServiceInjector } from '@nara.drama/prologue';
import { ProjectRegisterForm } from '@nara.drama/gitmate';
import { dialogUtil } from '@nara.drama/prologue-util';
import { TaskRecordStateKeeper } from '@nara.drama/task';


interface Props {
  //
  taskId: string;
  taskRecordId: string;
  actorId: string;
  actorName: string;
  loginUserEmail: string;
  onSuccess?: () => void;
}

interface InjectedProps {
  taskRecordStateKeeper: TaskRecordStateKeeper;
}


@autobind
@observer
class ProjectConnectionContainer extends ReactComponent<Props, {}, InjectedProps> {
  //
  static defaultProps = {
    onSuccess: () => {},
  };

  async onConnectProject(projectId: string) {
    //
    const { taskRecordId, onSuccess } = this.propsWithDefault;
    const { taskRecordStateKeeper } = this.injected;

    const nameValueList = new NameValueList(new NameValue('projectId', projectId));
    const response = await taskRecordStateKeeper.modify(taskRecordId, nameValueList);

    if (!response.entityIds.length) {
      dialogUtil.alert({ message: '연결 중 오류가 발생하였습니다.<br/>다시 시도하여 주시기 바랍니다.' });
    }
    else {
      onSuccess();
    }
  }

  render() {
    //
    const { actorId, actorName, loginUserEmail } = this.props;

    return (
      <ProjectRegisterForm
        actorKey={actorId}
        traineeName={actorName}
        traineeEmail={loginUserEmail}
        traineeTenantKey={actorId}
        onConnectSuccess={this.onConnectProject}
      />
    );
  }
}

export default ServiceInjector.withContext(
  TaskRecordStateKeeper
)(ProjectConnectionContainer);
