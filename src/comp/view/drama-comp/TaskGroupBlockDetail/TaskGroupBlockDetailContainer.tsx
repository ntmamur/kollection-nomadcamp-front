import React from 'react';
import { observer } from 'mobx-react';
import { autobind, ReactComponent, ServiceInjector } from '@nara.drama/prologue';
import { TaskGroupBlockStateKeeper } from '@nara.drama/task';
import TaskGroupDetail from '../TaskGroupDetail';


interface Props {
  //
  taskGroupBlockId: string;
}

interface InjectedProps {
  taskGroupBlockStateKeeper: TaskGroupBlockStateKeeper;
}


@autobind
@observer
class TaskGroupBlockDetailContainer extends ReactComponent<Props, {}, InjectedProps> {
  //
  componentDidMount() {
    //
    this.init();
  }

  async init() {
    //
    const { taskGroupBlockId } = this.props;
    const { taskGroupBlockStateKeeper } = this.injected;

    await taskGroupBlockStateKeeper.findTaskGroupBlockById(taskGroupBlockId);
  }

  render() {
    //
    const { taskGroupBlock } = this.injected.taskGroupBlockStateKeeper;

    if (!taskGroupBlock) {
      return null;
    }

    return (
      <TaskGroupDetail
        taskGroup={taskGroupBlock}
      />
    );
  }
}

export default ServiceInjector.withContext(
  TaskGroupBlockStateKeeper,
)(TaskGroupBlockDetailContainer);
