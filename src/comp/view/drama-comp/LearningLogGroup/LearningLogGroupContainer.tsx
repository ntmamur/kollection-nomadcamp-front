import React from 'react';
import { observer } from 'mobx-react';
import { Moment } from 'moment';
import { autobind, IdName, ReactComponent, ServiceInjector } from '@nara.drama/prologue';
import { LearningCompositesStateKeeper } from '@nara.drama/learninglog';
import { Question } from '@nara.drama/qna';
import { nWithStyles, NWithStyles } from '~/comp/view/theme';
import LearningLogGroupView from './view/LearningLogGroupView';


interface Props extends NWithStyles {
  //
  date: Moment;
  taskGroupId: string;
  traineeId: string;
  writer: IdName;
  audienceId: string;
  kollectionEditionId: string;
}

interface State {
  activeIndex: number;
  selectedQuestion: Question | null;
}


interface InjectedProps {
  learningCompositesStateKeeper: LearningCompositesStateKeeper;
}


@autobind
@observer
class LearningLogGroupContainer extends ReactComponent<Props, State, InjectedProps> {
  //
  state: State = {
    activeIndex: 0,
    selectedQuestion: null,
  };

  async componentDidMount() {
    //
    await this.init();
  }

  componentDidUpdate(prevProps: Readonly<Props>, prevState: Readonly<State>, snapshot?: any) {
    //
    const { traineeId: prevTraineeId, date: prevDate } = prevProps;
    const { traineeId, date } = this.props;

    if (prevTraineeId !== traineeId || !prevDate.isSame(date)) {
      this.init();
    }
  }

  async init() {
    //
    const { taskGroupId, date, traineeId } = this.props;
    const { learningCompositesStateKeeper } = this.injected;

    await learningCompositesStateKeeper.findLearningCompositesByTraineeIdAndDateAndTaskGroupId(traineeId, date, taskGroupId);
  }

  onChange(event: React.ChangeEvent<{}>, value: number) {
    //
    this.setState({ activeIndex: value });
  }


  render() {
    //
    const { traineeId, date, kollectionEditionId, taskGroupId, audienceId } = this.props;
    const { activeIndex } = this.state;
    const { learningCompositesStateKeeper } = this.injected;
    const { learningComposites } = learningCompositesStateKeeper;
    const topics = learningComposites.map(learningComposite => learningComposite.topic!);
    const targetLearningComposite = learningComposites.length > 0 ? learningComposites[activeIndex] : null;

    return (
      <LearningLogGroupView
        date={date}
        taskGroupId={taskGroupId}
        traineeId={traineeId}
        kollectionEditionId={kollectionEditionId}
        audienceId={audienceId}
        activeIndex={activeIndex}
        topics={topics}
        learningComposite={targetLearningComposite}
        onChange={this.onChange}
      />
    );
    // if (type === KollectieRoleType.Coach) {
    //   return (
    //     <LearningLogGroupView
    //       date={date}
    //       taskGroupId={taskGroupId}
    //       traineeId={traineeId}
    //       kollectionEditionId={kollectionEditionId}
    //       audienceId={audienceId}
    //       activeIndex={activeIndex}
    //       topics={topics}
    //       learningComposite={targetLearningComposite}
    //       onChange={this.onChange}
    //     />
    //   );
    // }
    // else if (type === KollectieRoleType.Reportee) {
    //   return (
    //     <>
    //       <LearningLogGroupForReporteeView
    //         date={date}
    //         taskGroupId={taskGroupId}
    //         traineeId={traineeId}
    //         kollectionEditionId={kollectionEditionId}
    //         audienceId={audienceId}
    //         activeIndex={activeIndex}
    //         topics={topics}
    //         learningComposite={targetLearningComposite}
    //         onChange={this.onChange}
    //       />
    //       {
    //         targetLearningComposite ? (
    //           <Box display="block" pr={3} pl={3} overflow="auto" style={{ maxHeight: '700px', width: '100%' }}>
    //             <DailyCommentList
    //               date={date}
    //               traineeId={traineeId}
    //               taskGroupId={taskGroupId}
    //             >
    //               <Divider />
    //               <DailyCommentList.Content
    //                 emptyList={
    //                   <Box pt={2} className={classes.nnotSmall} display="flex" justifyContent="center">
    //                     <Search fontSize="large" />&nbsp;&nbsp;코멘트가 없습니다.
    //                   </Box>
    //                 }
    //               />
    //             </DailyCommentList>
    //           </Box>
    //         ) : null
    //       }
    //     </>
    //   );
    // }
    // else {
    //   return null;
    // }
  }
}

export default ServiceInjector.withContext(
  LearningCompositesStateKeeper,
)(nWithStyles(LearningLogGroupContainer));
