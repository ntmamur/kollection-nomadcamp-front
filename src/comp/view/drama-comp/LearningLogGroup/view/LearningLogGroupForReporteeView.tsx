import React from 'react';
import { observer } from 'mobx-react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import {
  Paper, Box, Tab, Tabs, Typography, Button, Grid,
} from '@material-ui/core';
import { LearningComposite, LearningCompositeDetail, Topic } from '@nara.drama/learninglog';
import { Moment } from 'moment';
import { Modal, NoData, QuestionTabList } from '~/comp/view';
import { nWithStyles, NWithStyles } from '~/comp/view/theme';


interface Props extends NWithStyles {
  //
  date: Moment;
  taskGroupId: string;
  traineeId: string;
  kollectionEditionId: string;
  audienceId: string;
  activeIndex: number;
  topics: Topic[];
  learningComposite: LearningComposite | null;
  onChange: (event: React.ChangeEvent<{}>, value: number) => void;
}


@autobind
@observer
class LearningLogGroupForReporteeView extends ReactComponent<Props> {
  //
  constructor(props: Props) {
    super(props);

    this.state = { selectedQuestion: null };
  }

  render() {
    //
    const {
      date,
      taskGroupId,
      traineeId,
      kollectionEditionId,
      audienceId,
      activeIndex,
      topics,
      learningComposite,
      onChange,
    } = this.props;
    const { classes } = this.props;

    return learningComposite ? (
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Tabs value={activeIndex} onChange={onChange} className={classes.ntabLine}>
            {topics.map((topic, index) => (
              <Tab key={index} value={index} label={topic.name} />
            ))}
          </Tabs>
        </Grid>
        <LearningCompositeDetail
          learningComposite={learningComposite}
        >
          <Grid item xs={6}>
            <Box py={2}>
              <Typography component="h3" variant="h3">주제</Typography>
              <Box mt={1}>
                <Paper variant="outlined">
                  <Box p={2}>
                    <LearningCompositeDetail.Topic />
                  </Box>
                </Paper>
              </Box>
            </Box>
            <Box py={3}>
              <Typography component="h3" variant="h3">학습 시간</Typography>
              <Box mt={1}>
                <Paper variant="outlined">
                  <Box p={2}>
                    <LearningCompositeDetail.LearningQuantity />
                  </Box>
                </Paper>
              </Box>
            </Box>
            <Box py={2}>
              <QuestionTabList
                date={date}
                taskGroupId={taskGroupId}
                traineeId={traineeId}
                kollectionEditionId={kollectionEditionId}
                audienceId={audienceId}
                answerable={false}
              />
            </Box>
            <Box py={2}>
              <Typography component="h3" variant="h3">참고자료</Typography>
              <Box mt={1}>
                <Paper variant="outlined">
                  <Box p={2}>
                    {
                      learningComposite.bookmarks.length > 0 ? (
                        <Modal
                          trigger={
                            <Button color="primary">
                              {learningComposite.bookmarks.length} 개의 참고자료가 있습니다.
                            </Button>
                          }
                        >
                          <Modal.Header>참고자료 목록</Modal.Header>
                          <Modal.Content>
                            <LearningCompositeDetail.BookmarkList />
                          </Modal.Content>
                        </Modal>
                      ) : (
                        <Button disabled>
                          {learningComposite.bookmarks.length} 개의 참고자료가 있습니다.
                        </Button>
                      )
                    }
                  </Box>
                </Paper>
              </Box>

            </Box>
          </Grid>
          <Grid item xs={6}>
            <Box py={2}>
              <Typography component="h3" variant="h3">학습 노트</Typography>
              <Box mt={1}>
                {/*<Paper variant="outlined">*/}
                <Paper variant="outlined" style={{ height: '400px', overflow: 'auto', padding: '10px' }}>
                  <Box p={3}>
                    <LearningCompositeDetail.Note />
                  </Box>
                </Paper>
              </Box>
            </Box>
          </Grid>
        </LearningCompositeDetail>
      </Grid>
    ) : (
      <NoData text="등록된 러닝로그가 없습니다." />
    );
  }
}

export default nWithStyles(LearningLogGroupForReporteeView);
