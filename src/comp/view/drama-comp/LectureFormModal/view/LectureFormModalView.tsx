import React from 'react';
import { observer } from 'mobx-react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { ValidationForm } from '@nara.drama/prologue-util';
import { Button, FormControl, Grid } from '@nara.drama/material-ui';
import { Course, DatePeriodPicker } from '@nara.drama/course';
import { Modal } from '~/comp/view';


interface Props {
  open: boolean;
  troupeId: string;
  findEnrolledCourseByClassId: (classId: string) => Promise<Course | null>;
  onSave: () => void;
  onClose: () => void;
}


@autobind
@observer
class LectureFormModalView extends ReactComponent<Props> {
  //
  render() {
    //
    const { open, troupeId, findEnrolledCourseByClassId, onSave, onClose } = this.props;

    return (
      <Modal
        open={open}
        maxWidth="md"
        onClose={onClose}
      >
        <Modal.Header>Lecture 설정</Modal.Header>
        <Modal.Content>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <FormControl.Label>기간</FormControl.Label>
            </Grid>
            <Grid item xs={12}>
              <ValidationForm.Field name="datePeriod">
                {({ form }) => (
                  <DatePeriodPicker
                    onChangeStartDate={(startDate) => form.setFieldValue('datePeriod.startDate', startDate)}
                    onChangeEndDate={(endDate) => form.setFieldValue('datePeriod.endDate', endDate)}
                  />
                )}
              </ValidationForm.Field>
            </Grid>

            <Grid item xs={12}>
              <FormControl.Label>그룹 선택</FormControl.Label>
            </Grid>
            {/*<Grid item xs={12}>*/}
              {/*<ValidationForm.Field name="courseId">*/}
              {/*  {({ form, field, error, helperText }) => (*/}
              {/*    // FIXME*/}
              {/*    // <TroupeSelector*/}
              {/*    //   troupeId={troupeId}*/}
              {/*    //   fullWidth*/}
              {/*    //   error={error}*/}
              {/*    //   helperText={helperText}*/}
              {/*    //   onSelect={async (troupe: Troupe) => {*/}
              {/*    //     const course = await findEnrolledCourseByClassId(troupe.id);*/}
              {/*    //     const courseId = course ? course.id : '';*/}
              {/*    //*/}
              {/*    //     form.setFieldValue('courseId', courseId, true);*/}
              {/*    //   }}*/}
              {/*    // />*/}
              {/*  )}*/}
              {/*</ValidationForm.Field>*/}
            {/*</Grid>*/}
          </Grid>
        </Modal.Content>
        <Modal.Actions>
          <Button type="submit" color="primary" onClick={onSave}>개설하기</Button>
        </Modal.Actions>
      </Modal>
    );
  }
}

export default LectureFormModalView;
