import { object, string } from 'yup';
import moment from 'moment';
import { DatePeriod } from '@nara.drama/prologue';


class LectureFormModel {
  courseId: string;
  datePeriod: DatePeriod;

  constructor(courseId: string, datePeriod: DatePeriod) {
    //
    this.courseId = courseId;
    this.datePeriod = datePeriod;
  }

  static getDefault(): LectureFormModel {
    //
    return new LectureFormModel(
      '',
      new DatePeriod(moment(), moment()),
    );
  }

  static getValidationSchema() {
    return object({
      courseId: string().required('해당 그룹에 개설된 과정이 없습니다. 과정을 먼저 개설해주시기 바랍니다.'),
      datePeriod: object({
        startDate: object().required('시작일자를 입력하세요.'),
        endDate: object().required('종료일자를 입력하세요.'),
      }),
    });
  }
}

export default LectureFormModel;
