import React from 'react';
import { autobind, DramaException, ReactComponent, ServiceInjector } from '@nara.drama/prologue';
import { observer } from 'mobx-react';
import { CourseStateKeeper, LectureStateKeeper, SubjectStateKeeper } from '@nara.drama/course';
import { dialogUtil, ValidationForm } from '@nara.drama/prologue-util';
import LectureFormModel from './model/LectureFormModel';
import LectureFormModalView from './view/LectureFormModalView';


interface Props {
  subjectId: string;
  troupeId: string;
  audienceId: string;
  onClose: () => void;
}

interface State {
  initialValues: LectureFormModel;
}

interface InjectedProps {
  subjectStateKeeper: SubjectStateKeeper;
  lectureStateKeeper: LectureStateKeeper;
  courseStateKeeper: CourseStateKeeper;
}

@autobind
@observer
class LectureFormModalContainer extends ReactComponent<Props, State, InjectedProps> {
  //
  state: State = {
    initialValues: LectureFormModel.getDefault(),
  };

  validationSchema = LectureFormModel.getValidationSchema();

  componentDidMount() {
    //
    this.init();
  }

  componentDidUpdate(prevProps: Readonly<Props>, prevState: Readonly<State>, snapshot?: any) {
    //
    const { subjectId: prevSubjectId } = prevProps;
    const { subjectId } = this.props;

    if (prevSubjectId !== subjectId) {
      this.init();
    }
  }

  init() {
    //
    const { subjectId } = this.props;
    const { subjectStateKeeper } = this.injected;

    subjectStateKeeper.findSubjectById(subjectId).catch(() => null);
  }

  async onStartLecture(lectureFormModel: LectureFormModel) {
    //
    const { subjectId, audienceId } = this.props;
    const { lectureStateKeeper } = this.injected;

    const response = await lectureStateKeeper.start(
      lectureFormModel.courseId,
      subjectId,
      lectureFormModel.datePeriod,
      audienceId,
    );

    if (response.entityIds.length) {
      dialogUtil.alert({ title: '개설 성공', message: '수업이 개설되었습니다.' });
      this.onClose();
    } else {
      dialogUtil.alert({ title: '개설 실패', message: '개설 중 오류가 발생하였습니다. 다시 시도해주시기 바랍니다.' });
    }
  }

  findEnrolledCourseByClassId(classId: string) {
    //
    const { subjectStateKeeper, courseStateKeeper } = this.injected;
    const { subject } = subjectStateKeeper;

    if (!subject) {
      throw new DramaException('LectureFormModal', 'Subject should not be null.');
    }

    return courseStateKeeper.findEnrolledCourseByCoursePlanIdAndClassId(subject.coursePlanId, classId).catch(() => null);
  }

  onClose() {
    //
    this.props.onClose();
    this.setState({ initialValues: LectureFormModel.getDefault() });
  }

  render() {
    //
    const { subjectId, troupeId } = this.props;
    const { initialValues } = this.state;

    if (!initialValues) {
      return null;
    }

    return (
      <ValidationForm
        initialValues={initialValues}
        validationSchema={this.validationSchema}
        onSubmit={this.onStartLecture}
      >
        {(formikProps) => (
          <LectureFormModalView
            open={!!subjectId}
            troupeId={troupeId}
            findEnrolledCourseByClassId={this.findEnrolledCourseByClassId}
            onSave={formikProps.submitForm}
            onClose={this.onClose}
          />
        )}
      </ValidationForm>
    );
  }
}

export default ServiceInjector.withContext(
  SubjectStateKeeper,
  CourseStateKeeper,
  LectureStateKeeper,
)(LectureFormModalContainer);
