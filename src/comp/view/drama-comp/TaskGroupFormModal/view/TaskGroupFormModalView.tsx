import React from 'react';
import { observer } from 'mobx-react';
import { Moment } from 'moment';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { ValidationForm } from '@nara.drama/prologue-util';
import { Box, FormControl, Grid } from '@nara.drama/material-ui';
import {DatePeriodPicker, Modal, ProgressButton} from "~/comp/view/comp";

interface Props {
  open: boolean;
  loading: boolean;
  onSave: () => void;
  onClose: () => void;
}

interface State {
  selectedCoachIds: string[];
}


@autobind
@observer
class TaskGroupFormModalView extends ReactComponent<Props, State> {
  //
  state: State = {
    selectedCoachIds: [],
  };

  render() {
    //
    const { open, loading, onSave, onClose } = this.props;

    return (
      <Box width="100%" textAlign="right" onClick={(event: React.MouseEvent) => event.stopPropagation()}>
        <Modal open={open} onClose={onClose} maxWidth="md">
          <Modal.Header onClose={onClose}>Task 설정</Modal.Header>
          <Modal.Content>
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <FormControl.Label>기간</FormControl.Label>
              </Grid>
              <Grid item xs={12}>
                <ValidationForm.Field name="datePeriod">
                  {({ form, field, error, helperText }) => (
                    <DatePeriodPicker
                      onChangeStartDate={(date: Moment) => form.setFieldValue('datePeriod.startDate', date)}
                      onChangeEndDate={(date: Moment) => form.setFieldValue('datePeriod.endDate', date)}
                    />
                  )}
                </ValidationForm.Field>
              </Grid>
            </Grid>
          </Modal.Content>
          <Modal.Actions>
            <ProgressButton
              loading={loading}
              buttonProps={{
                color: 'primary',
                onClick: onSave,
              }}
            >
              개설하기
            </ProgressButton>
          </Modal.Actions>
        </Modal>
      </Box>
    );
  }
}

export default TaskGroupFormModalView;
