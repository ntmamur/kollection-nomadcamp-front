import React from 'react';
import { observer } from 'mobx-react';
import { autobind, IdName, ReactComponent, ServiceInjector } from '@nara.drama/prologue';
import { dialogUtil, ValidationForm } from '@nara.drama/prologue-util';
import { ActorsStateKeeper } from '@nara.drama/stage';
import { RollBookStateKeeper, TaskGroupStateKeeper } from '@nara.drama/task';
import TaskGroupFormModel from './model/TaskGroupFormModel';
import TaskGroupFormModalView from './view/TaskGroupFormModalView';
import { StageRoleType } from '~/nara';


interface Props {
  taskGroupBlockId: string;
  audienceId: string;
  currentCastId: string;
  onClose?: () => void;
}

interface State {
  initialValues: TaskGroupFormModel | null;
  loading: boolean;
}

interface InjectedProps {
  //
  taskGroupStateKeeper: TaskGroupStateKeeper;
  rollBookStateKeeper: RollBookStateKeeper;
  actorsStateKeeper: ActorsStateKeeper;
}

@autobind
@observer
class TaskGroupFormModalContainer extends ReactComponent<Props, State, InjectedProps> {
  //
  static defaultProps = {
    onClose: () => {
    },
  };

  validationSchema = TaskGroupFormModel.getValidationSchema();

  state: State = {
    initialValues: TaskGroupFormModel.getDefault(),
    loading: false,
  };

  async onSave(taskGroupFormModel: TaskGroupFormModel) {
    //
    const { taskGroupStateKeeper, actorsStateKeeper } = this.injected;
    const { taskGroupBlockId, audienceId, onClose } = this.propsWithDefault;
    const { currentCastId } = this.props;

    const taskGroup = await taskGroupStateKeeper.findTaskGroupByTaskGroupBlockIdAndClassId(taskGroupBlockId, currentCastId)
      .catch(() => null);

    if (taskGroup) {
      dialogUtil.alert({ title: '안내', message: 'Task Group이 이미 생성되어 있습니다.' });
      return;
    }

    this.setState({ loading: true });
    try {
      const traineeActors = await actorsStateKeeper.findActorsByCastIdAndHasAnyStageRoles(currentCastId, [StageRoleType.Manager]);
      const traineeActorIdName = traineeActors.map(traineeActor => new IdName(traineeActor.id, traineeActor.name, traineeActor.id));

      const response = await taskGroupStateKeeper.registerByBlock(
        currentCastId,
        traineeActorIdName,
        taskGroupBlockId,
        taskGroupFormModel.datePeriod,
        audienceId,
      );

      if (response.entityIds.length) {
        dialogUtil.alert({ title: '개설 성공', message: '과제가 개설되었습니다.' });
        onClose();
      }
      else {
        dialogUtil.alert({ title: '개설 실패', message: '개설 중 오류가 발생하였습니다. 다시 시도해주시기 바랍니다.' });
      }
    }
    finally {
      this.setState({ loading: false });
    }
  }

  render() {
    //
    const { taskGroupBlockId, onClose } = this.propsWithDefault;
    const { initialValues, loading } = this.state;

    if (!initialValues) {
      return null;
    }

    return (
      <ValidationForm
        initialValues={initialValues}
        validationSchema={this.validationSchema}
        onSubmit={this.onSave}
      >
        {(formikProps) => (
          <TaskGroupFormModalView
            open={!!taskGroupBlockId}
            loading={loading}
            onSave={formikProps.submitForm}
            onClose={() => {
              onClose();
              formikProps.resetForm();
            }}
          />
        )}
      </ValidationForm>
    );
  }
}

export default ServiceInjector.withContext(
  TaskGroupStateKeeper,
  RollBookStateKeeper,
  ActorsStateKeeper,
)(TaskGroupFormModalContainer);
