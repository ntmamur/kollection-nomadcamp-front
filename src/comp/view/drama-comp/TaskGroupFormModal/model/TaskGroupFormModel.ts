import { object } from 'yup';
import moment from 'moment';
import { DatePeriod } from '@nara.drama/prologue';


class TaskGroupFormModel {
  datePeriod: DatePeriod;


  constructor(datePeriod: DatePeriod) {
    //
    this.datePeriod = datePeriod;
  }

  static getDefault(): TaskGroupFormModel {
    //
    return new TaskGroupFormModel(
      new DatePeriod(moment(), moment()),
    );
  }

  static getValidationSchema() {
    return object({
      datePeriod: object({
        startDate: object().required('시작일자를 입력하세요.'),
        endDate: object().required('종료일자를 입력하세요.'),
      }),
    });
  }
}

export default TaskGroupFormModel;
