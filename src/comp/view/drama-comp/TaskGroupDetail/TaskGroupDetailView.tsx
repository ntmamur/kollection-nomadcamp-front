import React from 'react';
import { observer } from 'mobx-react';
import { autobind, ReactComponent, ServiceInjector } from '@nara.drama/prologue';
import {
  CodeContent,
  Task,
  TaskGroup,
  TaskGroupBlock,
  TaskGroupDetail,
  TaskGroupStateKeeper,
  TaskList, VideoContent, VideoContentDetail,
} from '@nara.drama/task';
import { Box, Breadcrumbs, Container, Grid, Link, Paper, Typography, Button } from '@material-ui/core';
import { nWithStyles, NWithStyles } from '../../theme';
import {Modal} from "~/comp/view/comp";

interface Props extends NWithStyles {
  //
  taskGroup: TaskGroupBlock | TaskGroup;
  onClick?: (event: React.MouseEvent, taskGroup: TaskGroup | TaskGroupBlock) => void;
  renderContent?: (task: Task) => React.ReactNode;
}

interface State {
  videoContent: VideoContent;
  open: boolean;
}


@autobind
@observer
class TaskGroupDetailView extends ReactComponent<Props, State> {
  //
  static defaultProps = {
    onClick: () => {},
    renderContent: () => null,
  };

  state = { videoContent: VideoContent.new(), open: false };


  async onClickCodeContent(event: React.MouseEvent, codeContent: CodeContent) {
    return window.open(codeContent.url);
  }


  onClose() {
    this.setState({ open: false });
  }

  onClickVideoContent(event: React.MouseEvent, videoContent: VideoContent) {
    //
    this.setState({ videoContent, open: true });
  }

  render() {
    //
    const { classes, taskGroup, renderContent } = this.props;
    const { videoContent, open } = this.state;

    return (
      <TaskGroupDetail taskGroup={taskGroup}>
        <Paper>
          <Container maxWidth="lg">
            <Box py={8}>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  {/*로케이션 S*/}
                  <div className={classes.task_loc_top}>
                    <Breadcrumbs separator="›" aria-label="breadcrumb">
                      <Link color="inherit" href="">과제</Link>
                      <Typography color="textPrimary">{taskGroup.name}</Typography>
                    </Breadcrumbs>
                  </div>
                  {/*로케이션 E*/}
                  <TaskGroupDetail.Intro
                    action={taskGroup instanceof TaskGroup && (
                      <div>
                        <ul className={classes.task_dtxt_ul}>
                          <li>
                            <Typography>{taskGroup.startDate}~{taskGroup.endDate}</Typography>
                          </li>
                        </ul>
                      </div>
                    )}
                  />
                </Grid>
              </Grid>
            </Box>
          </Container>
        </Paper>
        <Container className={classes.task_detail_view}>
          {/*학습목표 S*/}
          <Grid className={classes.task_detail_conspacing}>
            <h3>학습목표</h3>
            <Paper className={classes.task_detail_paper}>
              <TaskGroupDetail.Goal />
            </Paper>
          </Grid>
          {/*학습목표 E*/}
          {/*학습자료 S*/}
          <Grid className={classes.task_detail_conspacing}>
            <h3>학습자료</h3>
            <Paper className={classes.task_detail_paper}>
              <TaskGroupDetail.ContentGroup
                onClickCodeContent={this.onClickCodeContent}
                onClickVideoContent={this.onClickVideoContent}
              />
              { videoContent.url ? (
                <Modal open={open} onClose={this.onClose} maxWidth="lg">
                  <Modal.Header onClose={this.onClose}>{videoContent.name}</Modal.Header>
                  <Modal.Content>
                    {videoContent.description}
                    <Box height="600px" margin="0 auto" my={5}>
                      <VideoContentDetail videoContent={videoContent} />
                    </Box>
                  </Modal.Content>
                  <Modal.Actions>
                    <Button color="primary" onClick={this.onClose}>확인</Button>
                  </Modal.Actions>
                </Modal>
              ) : null
              }
            </Paper>
          </Grid>
          {/*학습자료 E*/}
          {/*과제 S*/}
          <Grid className={classes.task_detail_conspacing}>
            <h3>과제</h3>
            {taskGroup instanceof TaskGroup && (
              <TaskList taskGroupId={taskGroup.id}>
                <TaskList.Content
                  renderContent={renderContent}
                />
              </TaskList>
            )}
          </Grid>
          {/*과제 E*/}
        </Container>
      </TaskGroupDetail>
    );
  }
}

export default ServiceInjector.withContext(
  TaskGroupStateKeeper,
)(nWithStyles(TaskGroupDetailView));
