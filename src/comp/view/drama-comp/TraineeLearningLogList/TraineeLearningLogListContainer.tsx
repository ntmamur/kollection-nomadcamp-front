import React from 'react';
import { observer } from 'mobx-react';
import { Moment } from 'moment';
import { autobind, IdName, ReactComponent, ServiceInjector } from '@nara.drama/prologue';
import { RollBookTraineeList } from '@nara.drama/task';
import { CommentsStateKeeper, DateUtil, LearningQuantitysStateKeeper } from '@nara.drama/learninglog';
import { Box, Button, TableCell, TableRow, Typography } from '@material-ui/core';
import { nWithStyles, NWithStyles } from '../../theme';
import TraineeLearning from './model/TraineeLearning';
import TraineeLearningQuantity from './model/TraineeLearningQuantity';
import TraineeLearningLogListStateKeeper from './state/TraineeLearningLogListStateKeeper';
import {NoData} from "~/comp/view/comp";


interface Props extends NWithStyles {
  //
  writerId: string;
  taskGroupId: string;
  startDate: Moment;
  endDate: Moment;
  onClick?: (event: React.MouseEvent, traineeId: string, quantity: TraineeLearningQuantity) => void;
}

interface State {
  days: Moment[];
  trainees: IdName[];
}

interface InjectedProps {
  learningQuantitysStateKeeper: LearningQuantitysStateKeeper;
  commentsStateKeeper: CommentsStateKeeper;
  traineeLearningLogListStateKeeper: TraineeLearningLogListStateKeeper;
}

@autobind
@observer
class TraineeLearningLogListContainer extends ReactComponent<Props, State, InjectedProps> {
  //
  static defaultProps = {
    onClick: () => {},
  };

  state: State = {
    days: [],
    trainees: [],
  };

  componentDidMount() {
    //
    this.initDays();
  }

  async componentDidUpdate(prevProps: Readonly<Props>, prevState: Readonly<State>, snapshot?: any) {
    //
    const { startDate: prevStartDate, endDate: prevEndDate } = prevProps;
    const { startDate, endDate } = this.props;

    if (!prevStartDate.isSame(startDate) || !prevEndDate.isSame(endDate)) {
      await this.initDays();
      await this.find();
    }
  }

  initDays() {
    //
    const { startDate, endDate } = this.props;

    this.setState(() => ({
      days: DateUtil.getDaysBetweenDate(startDate, endDate),
    }));
  }

  initTrainees(trainees: IdName[]) {
    //
    this.setState({ trainees });
    this.find();
  }

  async find() {
    //
    const { writerId, taskGroupId, startDate, endDate } = this.props;
    const { days, trainees } = this.state;
    const { learningQuantitysStateKeeper, commentsStateKeeper, traineeLearningLogListStateKeeper } = this.injected;

    const traineeLearnings: TraineeLearning[] = [];

    await Promise.all(trainees.map(async trainee => {
      const learningQuantities = await learningQuantitysStateKeeper.findLearningQuantitiesByTraineeIdAndDatePeriod(
        trainee.id,
        taskGroupId,
        startDate,
        endDate
      );
      const commentExistence = await commentsStateKeeper.findCommentExistences(trainee.id, writerId, taskGroupId, startDate, endDate);
      const dailyLearningMap = learningQuantitysStateKeeper.getDailyLearningMap(learningQuantities);
      const traineeLearning = TraineeLearning.fromModel(days, trainee, dailyLearningMap, commentExistence);

      traineeLearnings.push(traineeLearning);
    }));

    traineeLearningLogListStateKeeper.setTraineeLearnings(traineeLearnings);
  }

  renderHeader() {
    //
    const { days } = this.state;
    const { classes } = this.props;

    return (
      <>
        <TableRow className={classes.tableTopline} style={{}}>
          <TableCell />
          {days.map((day, index) => (
            <TableCell
              key={index}
              variant="head"
              align="center"
              colSpan={5}
            >
              {day.format('YYYY.MM.DD')}
            </TableCell>
          ))}
        </TableRow>
        <TableRow>
          <TableCell />
          {days.map((day, index) => (
            <React.Fragment key={index}>
              <TableCell variant="head" align="center">
                <Box width="22px" className={classes.ntr_case_r}>
                  <Typography>R</Typography>
                </Box>
              </TableCell>
              <TableCell variant="head" align="center">
                <Box width="22px" className={classes.ntr_case_w}>
                  <Typography>W</Typography>
                </Box>
              </TableCell>
              <TableCell variant="head" align="center">
                <Box width="22px" className={classes.ntr_case_c}>
                  <Typography>C</Typography>
                </Box>
              </TableCell>
              <TableCell variant="head" align="center">
                <Box width="22px" className={classes.ntr_case_loc}>
                  <Typography>Loc</Typography>
                </Box>
              </TableCell>
              <TableCell variant="head" align="center" />
            </React.Fragment>
          ))}
        </TableRow>
      </>
    );
  }

  renderContent(trainee: IdName) {
    //
    const { onClick } = this.propsWithDefault;
    const { traineeLearnings } = this.injected.traineeLearningLogListStateKeeper;
    const traineeLearning = traineeLearnings.find(traineeLearning => traineeLearning.trainee.id === trainee.id);

    if (!traineeLearning) {
      return <NoData text="작성된 러닝로그가 없습니다." />;
    }

    return traineeLearning.quantities.map((quantity, index) => (
      <React.Fragment key={index}>
        <TableCell align="center">{quantity.readingTime}<Typography variant="caption" display="block">min</Typography></TableCell>
        <TableCell align="center">{quantity.writingTime}<Typography variant="caption" display="block">min</Typography></TableCell>
        <TableCell align="center">{quantity.codingTime}<Typography variant="caption" display="block">min</Typography></TableCell>
        <TableCell align="center">{quantity.loc}<Typography variant="caption" display="block">ines</Typography></TableCell>
        <TableCell align="center">
          <Button
            size="small"
            variant="outlined"
            color={quantity.commented ? 'default' : 'primary'}
            onClick={(event: React.MouseEvent) => onClick(event, traineeLearning.trainee.id, quantity)}
          >
            이동
          </Button>
        </TableCell>
      </React.Fragment>
    ));
  }

  render() {
    //
    const { taskGroupId } = this.propsWithDefault;

    return (
      <RollBookTraineeList
        taskGroupId={taskGroupId}
        onInit={this.initTrainees}
      >
        <RollBookTraineeList.Content
          cellStyle={{ borderRight: '1px solid #ddd' }}
          renderHeader={this.renderHeader}
          renderContent={this.renderContent}
        />
        <RollBookTraineeList.Pagination />
      </RollBookTraineeList>
    );
  }
}

export default ServiceInjector.with(
  LearningQuantitysStateKeeper,
  CommentsStateKeeper,
  TraineeLearningLogListStateKeeper,
)(nWithStyles(TraineeLearningLogListContainer));
