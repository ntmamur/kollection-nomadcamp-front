import { IdName, makeExtendedObservable } from '@nara.drama/prologue';
import { DateUtil, LearningQuantity } from '@nara.drama/learninglog';
import { Moment } from 'moment';
import TraineeLearningQuantity from './TraineeLearningQuantity';


class TraineeLearning {
  //
  trainee: IdName;
  quantities: TraineeLearningQuantity[];

  constructor(trainee: IdName, quantities: TraineeLearningQuantity[]) {
    //
    this.trainee = trainee;
    this.quantities = quantities;
    makeExtendedObservable(this);
  }

  static fromModel(days: Moment[], trainee: IdName, dailyLearningMap: Map<string, LearningQuantity[]>, commentExistence: boolean[]) {
    //
    const defaultDaillyQuantities = days.map(() => LearningQuantity.new());
    const quantities = days.map((day, dayIndex) => {
      const learningQuanities = dailyLearningMap.get(DateUtil.getFormattedDate(day)) || defaultDaillyQuantities;

      return TraineeLearningQuantity.fromModel(day, learningQuanities, commentExistence[dayIndex]);
    });

    return new TraineeLearning(trainee, quantities);
  }
}

export default TraineeLearning;

