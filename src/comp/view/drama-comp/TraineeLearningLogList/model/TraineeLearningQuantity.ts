import { makeExtendedObservable } from '@nara.drama/prologue';
import { Moment } from 'moment';
import { LearningQuantity, LearningQuantityUtil } from '@nara.drama/learninglog';


class TraineeLearningQuantity {
  //
  date: Moment;
  readingTime: number;
  writingTime: number;
  codingTime: number;
  loc: number;

  commented: boolean;

  constructor(date: Moment, readingTime: number, writingTime: number, codingTime: number, loc: number, commented: boolean) {
    //
    this.date = date;
    this.readingTime = readingTime;
    this.writingTime = writingTime;
    this.codingTime = codingTime;
    this.loc = loc;
    this.commented = commented;
    makeExtendedObservable(this);
  }

  static fromModel(date: Moment, learningQuantities: LearningQuantity[], commented: boolean) {
    //
    const totalQuantity = LearningQuantityUtil.calculate(learningQuantities);

    return new TraineeLearningQuantity(
      date,
      totalQuantity.totalReadingTime,
      totalQuantity.totalWritingTime,
      totalQuantity.totalCodingTime,
      totalQuantity.totalLoc,
      commented,
    );
  }
}

export default TraineeLearningQuantity;

