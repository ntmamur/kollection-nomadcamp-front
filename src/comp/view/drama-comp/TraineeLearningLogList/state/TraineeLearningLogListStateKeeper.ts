import { makeExtendedObservable, mobxService } from '@nara.drama/prologue';
import TraineeLearning from '../model/TraineeLearning';


@mobxService
class TraineeLearningLogListStateKeeper {
  //
  static readonly instanceName = 'traineeLearningLogListStateKeeper';
  static instance: TraineeLearningLogListStateKeeper;

  traineeLearnings: TraineeLearning[] = [];

  constructor() {
    makeExtendedObservable(this);
  }

  setTraineeLearnings(traineeLearnings: TraineeLearning[]) {
    //
    this.traineeLearnings = [...traineeLearnings];
  }

  addTraineeLearning(traineeLearning: TraineeLearning) {
    //
    this.traineeLearnings = [...this.traineeLearnings, traineeLearning];
  }
}

TraineeLearningLogListStateKeeper.instance = new TraineeLearningLogListStateKeeper();
export default TraineeLearningLogListStateKeeper;
