import React from 'react';
import { observer } from 'mobx-react';
import { Box, Paper, Typography } from '@material-ui/core';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { Question, AnswerForm, Answer } from '@nara.drama/qna';
import { Button } from '@nara.drama/material-ui';
import { CheckCircle } from '@material-ui/icons';
import { nWithStyles, NWithStyles } from '~/comp/view/theme';
import { Editor} from "~/comp/view/comp";


interface Props extends NWithStyles {
  question: Question;
  onSuccess: () => void;
  onFail: () => void;
  audienceId: string;
  editable?: boolean;
}

interface State {
  editable: boolean;
}

@observer
@autobind
class AnswerFormContainer extends ReactComponent<Props, State> {
  //
  static defaultProps = {
    editable: true,
  };

  constructor(props: Props) {
    super(props);

    this.state = { editable: false };
  }

  onSuccess() {
    //
    const { onSuccess } = this.props;

    this.setState({ editable: false }, () => onSuccess());
  }

  onFail() {
    //
    const { onFail } = this.props;

    onFail();
  }

  onClickEdit() {
    //
    this.setState({ editable: true });
  }

  renderEditableForm() {
    //
    const { question, audienceId } = this.props;

    return (
      <AnswerForm
        writerId={question.writerId}
        questionId={question.id}
        audienceKey={audienceId}
        onSuccess={this.onSuccess}
        onFail={this.onFail}
        renderAnswerForm={
          (answer: Answer, setAnswer: (name: keyof Answer, value: any) => void, submit: () => void) => (
            <>
              <Box ml={3} mr={3} mt={3} mb={3} display="flex" width="100%" justifyContent="space-between">
                <Box ml={1} display="flex" alignItems="center">
                  <img src="/training/images/icon/reviewa.svg" />
                  <Typography variant="h4">
                    &nbsp;답변하기
                  </Typography>
                </Box>
                <Button variant="contained" color="primary" onClick={submit}>등록</Button>
              </Box>

              <Box width="100%" ml={3} mr={3}>
                <Paper style={{ height: '475px' }}>
                  <Editor
                    contentId={answer.questionId}
                    content={answer.message}
                    onChange={(content: string) => setAnswer('message', content)}
                    height={475}
                  />
                </Paper>
              </Box>
            </>
          )
        }
      />
    );
  }


  renderReadOnly() {
    //
    const { question } = this.props;
    const { classes } = this.props;
    const { editable } = this.propsWithDefault;

    return (
      <AnswerForm
        writerId={question.writerId}
        questionId={question.id}
        renderAnswerForm={
          (answer: Answer) => (
            <>
              <Box ml={3} mr={3} mt={3} mb={3} display="flex" width="100%" justifyContent="space-between">
                <Box ml={1} display="flex" alignItems="center">
                  <img src="/training/images/icon/reviewaOn.svg" />
                  { editable ? (
                    <Typography variant="h4">
                      &nbsp;답변하기
                    </Typography>
                  ) : (
                    <Typography variant="h4">
                      &nbsp;답변
                    </Typography>
                  )}
                  <CheckCircle fontSize="small" color="primary" className={classes.writtenIcon} />
                  {/* TODO */}
                  {/*<Typography variant="body2" className={classes.nGray}>2021.06.29 19:05</Typography>*/}
                </Box>
                {editable ? (
                  <Button variant="outlined" color="primary" onClick={this.onClickEdit}>수정</Button>
                ) : null}
              </Box>

              <Box width="100%" ml={3} mr={3}>
                <Paper variant="outlined" style={{ height: '475px', overflow: 'auto', padding: '10px' }}>
                  <Editor
                    readOnly
                    contentId={answer.id}
                    content={answer.message}
                  />
                </Paper>
              </Box>
            </>
          )
        }
      />
    );
  }

  render() {
    //
    const { question } = this.props;
    const { editable: editableProps } = this.propsWithDefault;
    const { editable } = this.state;

    if (editableProps && (!question.answered || editable)) {
      return this.renderEditableForm();
    }
    else {
      return this.renderReadOnly();
    }
  }
}

export default nWithStyles(AnswerFormContainer);
