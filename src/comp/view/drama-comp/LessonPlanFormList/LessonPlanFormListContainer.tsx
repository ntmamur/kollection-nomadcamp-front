import React from 'react';
import { observer } from 'mobx-react';
import { autobind, Offset, ReactComponent, ServiceInjector } from '@nara.drama/prologue';
import { LessonPlan, LessonPlansStateKeeper, LessonPlanStateKeeper } from '@nara.drama/course';
import { dialogUtil } from '@nara.drama/prologue-util';
import { Accordion, Box, Button, Typography } from '@nara.drama/material-ui';
import LessonPlanFormWithQuizField from '../LessonPlanFormWithQuizField';


interface Props {
  subjectId: string;
  audienceId: string;
  troupeId: string;
  onSuccessRemove?: () => void;
  onFailRemove?: () => void;
}

interface InjectedProps {
  //
  lessonPlansStateKeeper: LessonPlansStateKeeper;
  lessonPlanStateKeeper: LessonPlanStateKeeper;
}

@autobind
@observer
class LessonPlanFormListContainer extends ReactComponent<Props, {}, InjectedProps> {
  //
  static defaultProps = {
    onSuccessRemove: () => {
    },
    onFailRemove: () => {
    },
  };

  componentDidMount() {
    //
    this.init();
  }

  init() {
    //
    const { subjectId } = this.props;

    this.injected.lessonPlansStateKeeper.findLessonPlansBySubjectId(subjectId, Offset.newAscending(0, 1000));
  }

  async onClick(event: React.MouseEvent, lessonPlan: LessonPlan) {
    //
    const { onSuccessRemove, onFailRemove } = this.propsWithDefault;
    const { lessonPlanStateKeeper } = this.injected;
    const confirmed = await dialogUtil.confirm({ title: '삭제', message: `${lessonPlan.name}을 삭제하시겠습니까?` });

    if (!confirmed) {
      return;
    }

    const response = await lessonPlanStateKeeper.remove(lessonPlan.id);

    if (response.entityIds.length) {
      onSuccessRemove();
    } else {
      dialogUtil.alert({ title: '삭제 실패', message: '삭제 중 오류가 발생하였습니다. 다시 시도해주시기 바랍니다.' });
      onFailRemove();
    }
  }

  render() {
    //
    const { audienceId } = this.props;
    const { lessonPlans } = this.injected.lessonPlansStateKeeper;

    return lessonPlans.map((lessonPlan, index) => (
      <Accordion key={index}>
        <Accordion.Summary>
          <Box mr={3} fontWeight="large">
            <Typography color="textSecondary">{lessonPlan.lessonNo}</Typography>
          </Box>
          <Typography>{lessonPlan.name}</Typography>
        </Accordion.Summary>
        <Accordion.Details>
          <Box p={3}>
            <LessonPlanFormWithQuizField
              lessonPlanId={lessonPlan.id}
              audienceId={audienceId}
            >
              <Box width="100%" textAlign="right" pt={3}>
                <Button type="submit" color="primary">저장</Button>
                <Button
                  variant="text"
                  color="primary"
                  onClick={(event: React.MouseEvent) => this.onClick(event, lessonPlan)}
                >
                  삭제
                </Button>
              </Box>
            </LessonPlanFormWithQuizField>
          </Box>
        </Accordion.Details>
      </Accordion>
    ));
  }
}

export default ServiceInjector.withContext(
  LessonPlansStateKeeper,
  LessonPlanStateKeeper
)(LessonPlanFormListContainer);
