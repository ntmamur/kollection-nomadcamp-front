import React from 'react';
import { observer } from 'mobx-react';
import { autobind, ReactComponent, ServiceInjector, Offset } from '@nara.drama/prologue';
import { Dialog } from '@nara.drama/material-ui';
import { CodingRequestStateKeeper, CodingTasksStateKeeper } from '@nara.drama/exam';


interface Props {
  audienceId: string;
  codingRequestId: string;
  troupeId: string;
  onSuccess: () => void;
  actionTrigger: boolean;
}

interface State {

}

interface InjectedProps {
  codingRequestStateKeeper: CodingRequestStateKeeper;
  codingTasksStateKeeper: CodingTasksStateKeeper;
}

@autobind
@observer
class CodingExerciseTriggerContainer extends ReactComponent<Props, State, InjectedProps> {
  //
  state: State ={
  };

  componentDidUpdate(prevProps: Readonly<Props>, prevState: Readonly<State>, snapshot?: any) {
    if (this.props.actionTrigger) {
      this.doAction();
    }
  }

  async doAction() {
    const { onSuccess } = this.props;

    console.log('do Action...');
    await this.findTemplateProjects();
    // await this.findTrainingGroup();
    // await this.trigger();
    onSuccess();
  }

  // async findTrainingGroup() {
  //   const { troupeId } = this.props;
  //   await this.injected.trainingGroupStateKeeper.findTrainingGroupByKey(troupeId);
  // }

  async findTemplateProjects() {
    const { codingRequestId } = this.props;
    const { codingRequestStateKeeper, codingTasksStateKeeper } = this.injected;

    codingRequestStateKeeper.findCodingRequestById(codingRequestId)
      .then(result => {
        codingTasksStateKeeper.findCodingTasksByIds(result.codingTaskIds, Offset.newAscending(0, 100, 'id')).then(result2 => console.log(result2));
      });
  }

  // async trigger() {
  //   const { audienceKey } = this.props;
  //   const { trainingGroupStateKeeper, codingTasksStateKeeper } = this.injected;
  //   const { trainingGroup } = trainingGroupStateKeeper;
  //   const { codingTasks } = codingTasksStateKeeper;
  //   if (trainingGroup) {
  //     codingTasks &&
  //     codingTasks.map(task => task.gitInfo!.refId)
  //       .forEach(projectId => {
  //       ExerciseFlowApiStub.instance.registerExercise(
  //         projectId,
  //         trainingGroup.name,
  //         trainingGroup.gitlabPath,
  //         trainingGroup.coachIds,
  //         trainingGroup.id,
  //         '',
  //         audienceKey
  //       );
  //     });
  //   }
  // }

  render() {
    //
    const { actionTrigger } = this.props;

    return (
      <Dialog open={actionTrigger}>
        <Dialog.Content>
          <Dialog.ContentText align={'center'}>
            학생들의 테스트 제출용 깃을 생성중입니다.
          </Dialog.ContentText>
        </Dialog.Content>
      </Dialog>
    );
  }
}

export default ServiceInjector.withContext(
  CodingRequestStateKeeper,
  CodingTasksStateKeeper,
)(CodingExerciseTriggerContainer);
