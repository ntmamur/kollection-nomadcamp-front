import React from 'react';
import { autobind, ReactComponent, ServiceInjector } from '@nara.drama/prologue';
import { observer } from 'mobx-react';
import { Box } from '@nara.drama/material-ui';
import {
  CommentFeedbackStateKeeper,
  CommentForm,
  CommentList,
  CommentListTypes,
  SubCommentForm,
  SubCommentList,
  SubCommentListTypes,
} from '@nara.drama/feedback';
import { PostStateKeeper } from '@nara.drama/board';


interface Props {
  postingId: string;
  citizenId: string;
  actorName: string;
}

interface State {
  repliable: boolean;
}

interface InjectedProps {
  commentFeedbackStateKeeper: CommentFeedbackStateKeeper;
  postStateKeeper: PostStateKeeper;
}

@autobind
@observer
class PostCommentListContainer extends ReactComponent<Props, State, InjectedProps> {
  //
  state: State = {
    repliable: false,
  };

  async componentDidMount() {
    await this.setRepliable();
  }

  async setRepliable() {
    const { postingId } = this.props;
    const { postStateKeeper } = this.injected;
    const bool = await postStateKeeper.findPostRepliableById(postingId);

    this.setState({ repliable: bool });
  }


  renderSub(params: CommentListTypes.RenderSubParams) {
    //
    const { comment } = params;
    const { actorName, citizenId } = this.props;
    const { repliable } = this.state;

    return comment.expanded && (
      <SubCommentList
        commentId={comment.id}
        userId={citizenId}
        writerName={actorName}
      >
        {(context: SubCommentListTypes.SubCommentListContextParams) => (
          <>
            <SubCommentList.Content />
            {repliable ?
              <Box pt={3}>
                <SubCommentForm
                  commentId={comment.id}
                  onSuccess={context.init}
                  onFail={context.init}
                  userId={citizenId}
                  writerName={actorName}
                />
              </Box>
              :
              null
            }
          </>
        )}
      </SubCommentList>
    );
  }

  render() {
    //
    const { postingId, actorName, citizenId } = this.props;
    const { repliable } = this.state;

    return (
      <CommentList
        sourceEntityId={postingId as string}
        sourceEntityName="Namoosori"
        userId={citizenId}
        writerName={actorName}
      >
        {(context: CommentListTypes.CommentListContextParams) => (
          repliable ?
            <>
              <CommentForm
                sourceEntityId={postingId as string}
                sourceEntityName="Namoosori"
                onSuccess={context.init}
                onFail={context.init}
                userId={citizenId}
                writerName={actorName}
              />
              <CommentList.Content renderSub={this.renderSub} />
            </>
            :
            <CommentList.Content renderSub={this.renderSub} />
        )}
      </CommentList>
    );
  }
}

export { PostCommentListContainer };
export default ServiceInjector.withContext(
  CommentFeedbackStateKeeper,
  PostStateKeeper,
)(PostCommentListContainer);
