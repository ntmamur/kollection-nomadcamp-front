import React from 'react';
import { observer } from 'mobx-react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { Box, Button } from '@nara.drama/material-ui';
import { QueuePlayNextOutlined } from '@material-ui/icons';
import { CoursePlanForm } from '@nara.drama/course';
import { Modal } from '~/comp/view';


interface Props {
  programId: string;
  audienceKey: string;
  onSuccess?: () => void;
}

interface State {
  open: boolean;
}

@autobind
@observer
class CoursePlanFormModal extends ReactComponent<Props, State> {
  //
  static defaultProps = {
    onSuccess: () => {
    },
  };

  state: State = {
    open: false,
  };

  onOpen() {
    //
    this.setState({ open: true });
  }

  onClose() {
    //
    this.setState({ open: false });
  }

  onSuccess() {
    //
    this.onClose();
    this.propsWithDefault.onSuccess();
  }

  render() {
    //
    const { programId, audienceKey } = this.propsWithDefault;
    const { open } = this.state;

    return (
      <>
        <Button variant="outlined" color="primary" startIcon={<QueuePlayNextOutlined fontSize="small" />} onClick={this.onOpen}>등록하기</Button>
        <Modal open={open} maxWidth="lg" onClose={this.onClose}>
          <Modal.Header onClose={this.onClose}>Course 기획 등록</Modal.Header>
          <Modal.Content>
            <CoursePlanForm
              programId={programId}
              audienceKey={audienceKey}
              onSuccess={this.onSuccess}
            >
              <CoursePlanForm.Form />
              <Box width="100%" textAlign="right">
                <Button type="submit" color="primary">등록</Button>
              </Box>
            </CoursePlanForm>
          </Modal.Content>
        </Modal>
      </>
    );
  }
}

export default CoursePlanFormModal;
