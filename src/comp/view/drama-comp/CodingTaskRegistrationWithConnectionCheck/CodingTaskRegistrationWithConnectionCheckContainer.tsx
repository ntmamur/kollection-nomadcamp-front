import React from 'react';
import { observer } from 'mobx-react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { CodingTaskRegistration } from '@nara.drama/exam';
import { dialogUtil } from '@nara.drama/prologue-util';


interface Props {
  audienceId: string;
  onSuccess: () => void;
  toList: () => void;
}

interface State {
  processing: boolean;
}


@autobind
@observer
class CodingTaskRegistrationWithConnectionCheckContainer extends ReactComponent<Props, State> {
  //
  state: State = {
    processing: true,
  };

  async check(refId: string) {
    // const foundGit = await this.injected.exerciseTemplatesStateKeeper.existsExerciseTemplate(refId);
    // return foundGit ? foundGit.webUrl : '';
    dialogUtil.alert({ message: '준비중입니다.' });
    return '';
  }

  render() {
    //
    const { audienceId, onSuccess, toList } = this.props;

    return (
      <CodingTaskRegistration
        audienceKey={audienceId}
        modelCodeConnectionTest={this.check}
        gitInfoConnectionTest={this.check}
        taskImage={`${process.env.BASE_PATH}/images/exam/coding_task.svg`}
        onSuccess={onSuccess}
        toList={toList}
      />
    );
  }
}

export default CodingTaskRegistrationWithConnectionCheckContainer;
