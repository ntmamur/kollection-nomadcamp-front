import React from 'react';
import { observer } from 'mobx-react';
import { Moment } from 'moment';
import { autobind, IdName, ReactComponent, ServiceInjector } from '@nara.drama/prologue';
import { Comment, CommentForm, DailyCommentList } from '@nara.drama/learninglog';
import { ActorStateKeeper } from '@nara.drama/stage';
import { Box, Button, Divider, Paper, Typography } from '@material-ui/core';
import { dialogUtil } from '@nara.drama/prologue-util';
import { Search } from '@material-ui/icons';
import { Editor } from '../../comp';
import { nWithStyles, NWithStyles } from '../../theme';


interface Props extends NWithStyles {
  //
  taskGroupId: string;
  date: Moment;
  traineeId: string;
  writer: IdName;
  audienceId: string;
}

interface State {
  editing: boolean;
}

interface InjectedProps {
  actorStateKeeper: ActorStateKeeper;
}


@autobind
@observer
class CommentFormContainer extends ReactComponent<Props, State, InjectedProps> {
  //
  state = { editing: false };

  async componentDidMount() {
    //
    await this.init();
  }

  componentDidUpdate(prevProps: Readonly<Props>, prevState: Readonly<{}>, snapshot?: any) {
    //
    const { traineeId: prevTraineeId } = prevProps;
    const { traineeId } = this.props;

    if (prevTraineeId !== traineeId) {
      this.init();
    }
  }

  async init() {
    //
    const { traineeId } = this.props;
    const { actorStateKeeper } = this.injected;

    await actorStateKeeper.findActorById(traineeId);
  }

  renderReadOnly(comment: Comment) {
    //
    const { classes } = this.props;
    const { date, traineeId, taskGroupId } = this.props;

    return (
      <>
        <Box ml={0} mb={2} display="flex" alignItems="center">
          <img src="/training/images/icon/reviewaOn.svg" />
          <Typography variant="h3">
            &nbsp;일일 코멘트
          </Typography>
        </Box>
        <Box display="block" pr={3} pl={3} overflow="auto" style={{ maxHeight: '700px', width: '100%' }}>
          <DailyCommentList
            date={date}
            traineeId={traineeId}
            taskGroupId={taskGroupId}
          >
            <DailyCommentList.Content
              emptyList={
                <Box pt={2} className={classes.nnotSmall} display="flex" justifyContent="center">
                  <Search fontSize="large" />&nbsp;&nbsp;코멘트가 없습니다.
                </Box>
              }
            />
          </DailyCommentList>
        </Box>
        <Box mb={3}>
          <Divider />
        </Box>
        <Paper style={{ height: '505px', padding: '20px' }}>
          <Editor
            readOnly
            contentId={comment.id}
            content={comment.content}
          />
        </Paper>
      </>
    );
  }

  renderForm(comment: Comment, setContent: (content: string) => void) {
    //
    const { classes } = this.props;
    const { date, traineeId, taskGroupId } = this.props;

    return (
      <>
        <Box ml={0} mb={2} display="flex" alignItems="center">
          <img src="/training/images/icon/reviewa.svg" />
          <Typography variant="h3">
            &nbsp;일일 코멘트
          </Typography>
        </Box>
        <Box display="block" pr={3} pl={3} overflow="auto" style={{ maxHeight: '700px', width: '100%' }}>
          <DailyCommentList
            date={date}
            traineeId={traineeId}
            taskGroupId={taskGroupId}
          >
            <DailyCommentList.Content
              emptyList={
                <Box pt={2} className={classes.nnotSmall} display="flex" justifyContent="center">
                  <Search fontSize="large" />&nbsp;&nbsp;코멘트가 없습니다.
                </Box>
              }
            />
          </DailyCommentList>
        </Box>
        <Box mb={3}>
          <Divider />
        </Box>
        <Paper style={{ height: '505px' }}>
          <Editor
            contentId={comment.id}
            content={comment.content}
            onChange={setContent}
            height={505}
          />
        </Paper>
      </>
    );
  }

  onClickEdit() {
    //
    this.setState({
      editing: true,
    });
  }

  onSuccess() {
    //
    this.setState({
      editing: false,
    });
  }

  handleInvalid() {
    //
    dialogUtil.alert({ title: '안내', message: '코멘트 내용을 입력해주세요.' });
  }

  render() {
    //
    const { taskGroupId, date, writer, audienceId } = this.props;
    const { editing } = this.state;
    const { actor } = this.injected.actorStateKeeper;

    if (!actor) {
      return null;
    }
    const trainee = new IdName(actor.id, actor.name);

    return (
      <CommentForm
        taskGroupId={taskGroupId}
        date={date}
        trainee={trainee}
        writer={writer}
      >
        <CommentForm.Form
          renderField={(comment: Comment, setContent: (content: string) => void) => {
            if (!comment.content && !editing) {
              this.setState({ editing: true });
              return null;
            }

            if (!editing) {
              return this.renderReadOnly(comment);
            }
            else {
              return this.renderForm(comment, setContent);
            }
          }}
        />
        <Box mt={3} width="100%" display="flex" justifyContent="center">
          { editing ? (
            <CommentForm.Action
              buttonProps={{
                size: 'large',
                variant: 'contained',
                color: 'primary',
              }}
              handleInvalid={this.handleInvalid}
              audienceKey={audienceId}
              onSuccess={this.onSuccess}
            />
          ) : (
            <Button variant="contained" size="large" onClick={this.onClickEdit}>수정</Button>
          )}

        </Box>
      </CommentForm>
    );
  }
}

export default ServiceInjector.withContext(
  ActorStateKeeper,
)(nWithStyles(CommentFormContainer));
