import React from 'react';
import { observer } from 'mobx-react';
import { autobind, NameValue, NameValueList, ReactComponent, ServiceInjector } from '@nara.drama/prologue';
import {
  FinishTaskAction,
  Task,
  TaskGroup,
  TaskGroupStateKeeper,
  TaskRecordsStateKeeper,
  TaskRecordStateKeeper,
} from '@nara.drama/task';
import { Box, Button, Divider, Grid, Typography } from '@material-ui/core';
import { Project, ProjectCommitList } from '@nara.drama/gitmate';
import { dialogUtil } from '@nara.drama/prologue-util';
import { nWithStyles, NWithStyles } from '../../theme';
import TaskGroupDetail from '../TaskGroupDetail';
import {ProjectConnectionForm} from "~/comp/view/drama-comp";


interface Props extends NWithStyles {
  //
  taskGroupId: string;
  actorId: string;
  actorName: string;
  traineeEmail: string;
  onClick?: (event: React.MouseEvent, taskGroup: TaskGroup) => void;
  onSuccess?: () => void;
}

interface InjectedProps {
  taskGroupStateKeeper: TaskGroupStateKeeper;
  taskRecordStateKeeper: TaskRecordStateKeeper;
  taskRecordsStateKeeper: TaskRecordsStateKeeper;
}


@autobind
@observer
class TraineeTaskGroupDetailContainer extends ReactComponent<Props, {}, InjectedProps> {
  //
  static defaultProps = {
    onClick: () => {},
    onSuccess: () => {},
  };


  componentDidMount() {
    //
    this.init();
  }

  async init() {
    //
    const { taskGroupId, actorId } = this.props;
    const { taskGroupStateKeeper, taskRecordsStateKeeper } = this.injected;

    await taskGroupStateKeeper.findTaskGroupById(taskGroupId);
    await taskRecordsStateKeeper.findTaskRecordsByTraineeIdAndTaskGroupId(actorId, taskGroupId);
  }

  async onClickRemove(taskRecordId: string) {
    //
    const { taskRecordStateKeeper } = this.injected;


    const confirmed = await dialogUtil.confirm({ message: '연결 된 git URL을 삭제 하시겠습니까?' });

    if (confirmed) {
      const nameValueList = new NameValueList(new NameValue('projectId', ''));
      const response = await taskRecordStateKeeper.modify(taskRecordId, nameValueList);

      if (!response.entityIds.length) {
        dialogUtil.alert({ message: '연결 중 오류가 발생하였습니다.<br/>다시 시도하여 주시기 바랍니다.' });
      }
      else {
        this.init();
      }

    }
  }

  async onConfirmFinishTask(totalLoc: number, completedAmount: number) {
    //
    return dialogUtil.confirm({
      message: `목표 Loc: ${totalLoc}, 현재 Loc: ${completedAmount} <br /><br />목표 loc에 도달하지 못했습니다.<br />학습 완료하시겠습니까?`,
    });
  }

  onSuccessFinishTask() {
    //
    this.init();
  }

  renderContent(task: Task) {
    //
    const { classes, actorId, actorName, traineeEmail } = this.propsWithDefault;
    const { taskIdMap } = this.injected.taskRecordsStateKeeper;
    const targetRecord = taskIdMap.get(task.id);

    if (!targetRecord) {
      return null;
    }

    return (
      <Box mt={1}>
        {targetRecord.projectId ? (
          <>
            <ProjectCommitList projectId={targetRecord.projectId} limit={3}>
              <Grid container className={classes.task_detail_tablebox}>
                <ProjectCommitList.Project>
                  {(project: Project) => (
                    <Grid className={classes.task_detail_table_tinfo}>
                      <Grid className={classes.task_detail_table_tinfo1}>
                        <Typography>
                          {project.gitWebUrl}
                        </Typography>
                      </Grid>
                      <Grid className={classes.task_detail_table_tinfo2}>
                        <div className={classes.ico_task_list} /><Typography>총 {project.totalLinesOfCode}라인</Typography>
                      </Grid>
                    </Grid>
                  )}
                </ProjectCommitList.Project>
                <Grid className={classes.task_detail_table}>
                  <ProjectCommitList.Content />
                </Grid>
              </Grid>
            </ProjectCommitList>
            <Divider />
            <Box pt={2} className={classes.nnotSmall} display="flex" justifyContent="center">
              <Box mr={1}>
                <Button variant="contained" size="large" onClick={() => this.onClickRemove(targetRecord.id)}>연결해제</Button>
              </Box>
              {
                !targetRecord.finished ? (
                  <Box>
                    <FinishTaskAction
                      traineeId={actorId}
                      taskId={task.id}
                      onConfirm={this.onConfirmFinishTask}
                      onSuccess={this.onSuccessFinishTask}
                    />
                  </Box>
                ) : null
              }

            </Box>
          </>
        ) : (
          <ProjectConnectionForm
            taskId={task.id}
            taskRecordId={targetRecord.id}
            actorId={actorId}
            actorName={actorName}
            loginUserEmail={traineeEmail}
            onSuccess={this.init}
          />
        )}
      </Box>
    );
  }


  render() {
    //
    const { taskGroup } = this.injected.taskGroupStateKeeper;

    if (!taskGroup) {
      return null;
    }

    return (
      <TaskGroupDetail
        taskGroup={taskGroup}
        renderContent={this.renderContent}
      />
    );
  }
}

export default ServiceInjector.withContext(
  TaskGroupStateKeeper,
  TaskRecordStateKeeper,
  TaskRecordsStateKeeper
)(nWithStyles(TraineeTaskGroupDetailContainer));
