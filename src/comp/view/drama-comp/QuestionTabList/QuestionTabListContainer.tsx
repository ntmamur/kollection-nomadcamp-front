import React from 'react';
import { observer } from 'mobx-react';
import { autobind, Offset, ReactComponent, ServiceInjector } from '@nara.drama/prologue';
import {
  Box, Button, Divider, Grid, Paper, Typography,
} from '@material-ui/core';
import {
  Question,
  QuestionList,
  QuestionsStateKeeper,
  QuestionStateKeeper,
} from '@nara.drama/qna';
import { TaskGroupStateKeeper } from '@nara.drama/task';
import { dialogUtil } from '@nara.drama/prologue-util';
import { Moment } from 'moment';
import { nWithStyles, NWithStyles } from '~/comp/view/theme';
import QuestionTabListView from './view/QuestionTabListView';
import {DateUtil, QnaUtil} from "~/comp/view/shared/util";
import {Modal} from "~/comp/view/comp";
import {CoachQuestionDetail} from "~/comp/view/drama-comp";


interface Props extends NWithStyles {
  //
  date: Moment;
  taskGroupId: string;
  traineeId: string;
  kollectionEditionId: string;
  audienceId: string;
  answerable?: boolean;
}

interface State {
  totalCount: number;
  selectedQuestion: Question | null;
  activeTabIndex: number;
  refreshTrigger: boolean;
}

interface InjectedProps {
  taskGroupStateKeeper: TaskGroupStateKeeper;
  questionsStateKeeper: QuestionsStateKeeper;
  questionStateKeeper: QuestionStateKeeper;
}

@autobind
@observer
class QuestionTabListContainer extends ReactComponent<Props, State, InjectedProps> {
  //
  static defaultProps = {
    answerable: true,
  };

  constructor(props: Props) {
    super(props);

    this.state = { totalCount: 0, selectedQuestion: null, activeTabIndex: 0, refreshTrigger: false };
  }

  componentDidMount() {
    //
    this.init();
  }

  componentDidUpdate(prevProps: Readonly<Props>) {
    //
    const { date: prevDate, taskGroupId: prevTaskGroupId } = prevProps;
    const { date, taskGroupId } = this.props;

    if (
      DateUtil.formatToString(prevDate) !== DateUtil.formatToString(date)
      || prevTaskGroupId !== taskGroupId
    ) {
      this.init();
    }
  }

  async init() {
    //
    const { taskGroupId, traineeId, kollectionEditionId } = this.props;
    const { taskGroupStateKeeper, questionsStateKeeper } = this.injected;

    await taskGroupStateKeeper.findTaskGroupById(taskGroupId);

    const groupTags = this.getGroupTags();

    questionsStateKeeper.setGroupId(QnaUtil.genClientAccountId(traineeId, kollectionEditionId));
    const questions = await questionsStateKeeper.findQuestionsByTags(groupTags, Offset.newDescending(0, 15, 'creationTime'));

    this.setState({
      selectedQuestion: questions.results[0],
      totalCount: questions.results.length,
    });
  }


  async onClickQuestion(clickedQuestion: Question, index: number) {
    //
    const question = await this.findQuestionById(clickedQuestion.id);

    this.setState({ selectedQuestion: question, activeTabIndex: index });
  }

  async findQuestionById(questionId: string) {
    //
    const { questionStateKeeper } = this.injected;
    return questionStateKeeper.findQuestionById(questionId);
  }

  getGroupTags() {
    //
    const { date } = this.props;
    const { taskGroupStateKeeper } = this.injected;
    const { taskGroup } = taskGroupStateKeeper;

    if (!taskGroup) {
      return [];
    }

    return [DateUtil.formatToString(date), taskGroup.name];
  }

  async onSuccess() {
    //
    const { refreshTrigger, activeTabIndex } = this.state;
    const { questionsStateKeeper } = this.injected;
    const { questions } = questionsStateKeeper;

    const savedQuestion = questions[activeTabIndex];
    const question = await this.findQuestionById(savedQuestion.id);

    this.setState({ refreshTrigger: !refreshTrigger, selectedQuestion: question });
  }

  onFail() {
    //
    dialogUtil.alert({ title: '안내', message: '답변 등록 중 에러가 발생했습니다.' });
  }

  render() {
    //
    const {
      kollectionEditionId,
      traineeId,
      audienceId,
    } = this.props;
    const { classes } = this.props;
    const { answerable } = this.propsWithDefault;
    const { totalCount, selectedQuestion, activeTabIndex, refreshTrigger } = this.state;
    const groupTags = this.getGroupTags();

    return (
      <>
        <Typography component="h3" variant="h3" display="block">Q&A</Typography>
        <Box mt={1}>
          <Paper variant="outlined">
            <Box p={2}>
              { totalCount > 0 ? (
                <Modal
                  maxWidth="lg"
                  trigger={
                    <Button color="primary">
                      {totalCount} 개의 질문이 있습니다.
                    </Button>
                  }
                  className={classes.modal}
                >
                  {(modalContext: any) => (
                    <div className={classes.nmodalQna}>
                      <Typography component="h3" className={classes.nmodal_tit}>Q&A</Typography>
                      {/* Tab S */}
                      <Paper square>
                        <QuestionList
                          clientAccountId={QnaUtil.genClientAccountId(traineeId, kollectionEditionId)}
                          groupTags={groupTags}
                          readerId={traineeId}
                          refreshTrigger={refreshTrigger}
                          renderChildren={(questions: Question[]) => (
                            <QuestionTabListView
                              questions={questions}
                              activeTabIndex={activeTabIndex}
                              onClickQuestion={(question: Question, index: number) => this.onClickQuestion(question, index)}
                            />
                          )}
                        />
                      </Paper>
                      {
                        selectedQuestion ? (
                          <CoachQuestionDetail
                            question={selectedQuestion}
                            audienceId={audienceId}
                            onSuccess={this.onSuccess}
                            onFail={this.onFail}
                            answerable={answerable}
                          />
                        ) : null
                      }
                      <Divider />
                      <Grid container justify="center">
                        <Box mt={2} mb={2}>
                          <Grid item>
                            <Button variant="contained" color="primary" onClick={() => modalContext.close()}>확인</Button>
                          </Grid>
                        </Box>
                      </Grid>
                    </div>
                  )}
                </Modal>
              ) : (
                <Button disabled>
                  {totalCount} 개의 질문이 있습니다.
                </Button>
              )}
            </Box>
          </Paper>
        </Box>
      </>
    );
  }
}

export default ServiceInjector.withContext(
  TaskGroupStateKeeper,
  QuestionsStateKeeper,
  QuestionStateKeeper,
)(nWithStyles(QuestionTabListContainer));
