import React from 'react';
import { observer } from 'mobx-react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import {
  Box, Tab, Tabs, Typography,
} from '@material-ui/core';
import { Question } from '@nara.drama/qna';
import { CheckCircle } from '@material-ui/icons';
import { nWithStyles, NWithStyles } from '~/comp/view/theme';


interface Props extends NWithStyles {
  //
  questions: Question[];
  activeTabIndex: number;
  onClickQuestion: (question: Question, index: number) => void;
}


@autobind
@observer
class QuestionTabListView extends ReactComponent<Props> {
  //
  render() {
    //
    const {
      questions,
      onClickQuestion,
      activeTabIndex,
    } = this.props;

    const { classes } = this.props;

    return (
      <Tabs
        value={activeTabIndex}
        indicatorColor="primary"
        textColor="primary"
        aria-label=""
      >
        {
          questions.map((question: Question, index: number) => (
            <Tab
              key={'QuestionTab-' + index}
              onClick={() => onClickQuestion(question, index)}
              label={
                <Box display="flex" alignItems="center">
                  <Box>
                    <Typography variant="body1">질문 {index + 1}</Typography>
                  </Box>
                  {
                    question.answered ? (
                      <Box mt={1}>
                        <CheckCircle fontSize="small" color="primary" className={classes.writtenIcon} />
                      </Box>
                    ) : null
                  }
                </Box>
              }
            />
          ))
        }
      </Tabs>
    );
  }
}

export default nWithStyles(QuestionTabListView);
