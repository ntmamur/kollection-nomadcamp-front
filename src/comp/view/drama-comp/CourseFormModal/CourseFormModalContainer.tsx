import React from 'react';
import { observer } from 'mobx-react';
import { autobind, ReactComponent, ServiceInjector } from '@nara.drama/prologue';
import { ValidationForm } from '@nara.drama/prologue-util';
import { ActorsStateKeeper } from '@nara.drama/stage';
import { CourseStateKeeper, RollBookStateKeeper } from '@nara.drama/course';
import { dock, WithDock } from '@nara.platform/dock';
import CourseFormModel from './model/CourseFormModel';
import CourseFormModalView from './view/CourseFormModalView';


interface Props {
  coursePlanId: string;
  audienceKey: string;
  onClose?: () => void;
}

interface State {
  initialValues: CourseFormModel | null;
  loading: boolean;
}

interface InjectedProps extends WithDock {
  //
  courseStateKeeper: CourseStateKeeper;
  rollBookStateKeeper: RollBookStateKeeper;
  actorsStateKeeper: ActorsStateKeeper;
}

@dock()
@autobind
@observer
class CourseFormModalContainer extends ReactComponent<Props, State, InjectedProps> {
  //
  static defaultProps = {
    onClose: () => {
    },
  };

  validationSchema = CourseFormModel.getValidationSchema();

  state: State = {
    initialValues: null,
    loading: false,
  };

  componentDidMount() {
    //
    this.init();
  }

  init() {
    //
    // FIXME
    // const { currentActorKey, currentActorName, loginUserEmail } = this.injected.dock;
    // const coach = new Coach(currentActorKey, currentActorName);
    // const contact = new Contact();
    //
    // contact.email = loginUserEmail;
    // coach.contact = contact;
    //
    // this.setState({
    //   initialValues: CourseFormModel.getDefault(coach, ''),
    // });
  }

  async onSave(courseFormModel: CourseFormModel) {
    //
    // const { coursePlanId, audienceKey, onClose } = this.propsWithDefault;
    // const {
    //   courseStateKeeper,
    //   rollBookStateKeeper,
    //   troupeActorsStateKeeper,
    // } = this.injected;
    //
    // const course = await courseStateKeeper.findEnrolledCourseByCoursePlanIdAndClassId(coursePlanId, courseFormModel.classId)
    //   .catch(() => null);
    //
    // if (course) {
    //   dialogUtil.alert({ title: '안내', message: 'Course가 이미 생성되어 있습니다.' });
    //   return;
    // }
    //
    // this.setState({ loading: true });
    // try {
    //   const troupeActors = await troupeActorsStateKeeper.findActiveTroupeActorsByTroupeId(courseFormModel.classId, Offset.newAscending(0, 1000));
    //   const trainees = troupeActors.results.map((troupeActor) => new IdName(troupeActor.actorKey, troupeActor.actorName, troupeActor.actorKey));
    //
    //   const courseRegisteredResponse = await courseStateKeeper.register(
    //     coursePlanId,
    //     courseFormModel.datePeriod,
    //     courseFormModel.classId,
    //     courseFormModel.coach,
    //     audienceKey,
    //   );
    //   const courseId = courseRegisteredResponse.entityIds[0];
    //   const registeredCourse = await courseStateKeeper.findCourseById(courseId);
    //
    //   const rollBookRegisteredResponse = await rollBookStateKeeper.register(new RollBookCdo(
    //     new IdName(registeredCourse.id, registeredCourse.name, ''),
    //     trainees,
    //     audienceKey,
    //   ));
    //
    //   if (rollBookRegisteredResponse.entityIds.length) {
    //     dialogUtil.alert({ title: '개설 성공', message: '수업이 개설되었습니다.' });
    //     onClose();
    //   } else {
    //     dialogUtil.alert({ title: '개설 실패', message: '개설 중 오류가 발생하였습니다. 다시 시도해주시기 바랍니다.' });
    //   }
    // } finally {
    //   this.setState({ loading: false });
    // }
  }

  render() {
    //
    const { coursePlanId, onClose } = this.propsWithDefault;
    const { initialValues, loading } = this.state;
    const { currentTroupeId } = this.injected.dock;

    if (!initialValues) {
      return null;
    }

    return (
      <ValidationForm
        initialValues={initialValues}
        validationSchema={this.validationSchema}
        onSubmit={this.onSave}
      >
        {(formikProps) => (
          <CourseFormModalView
            open={!!coursePlanId}
            loading={loading}
            troupeId={currentTroupeId}
            onSave={formikProps.submitForm}
            onClose={() => {
              onClose();
              formikProps.resetForm();
            }}
          />
        )}
      </ValidationForm>
    );
  }
}

export default ServiceInjector.withContext(
  CourseStateKeeper,
  RollBookStateKeeper,
  ActorsStateKeeper,
)(CourseFormModalContainer);
