import { object, string, bool } from 'yup';
import moment from 'moment';
import { DatePeriod, DramaException } from '@nara.drama/prologue';
import { Coach, Course } from '@nara.drama/course';


class CourseFormModel {
  datePeriod: DatePeriod;
  classId: string;
  className: string;

  coach: Coach;
  usernamePrefix: string;

  constructor(datePeriod: DatePeriod, classId: string, className: string, coach: Coach, usernamePrefix: string) {
    //
    this.datePeriod = datePeriod;
    this.classId = classId;
    this.className = className;
    this.coach = coach;
    this.usernamePrefix = usernamePrefix;
  }

  static getDefault(coach: Coach, usernamePrefix: string): CourseFormModel {
    //
    return new CourseFormModel(
      new DatePeriod(moment(), moment()),
      '',
      '',
      coach,
      usernamePrefix,
    );
  }

  static getValidationSchema() {
    return object({
      datePeriod: object({
        startDate: object().required('시작일자를 입력하세요.'),
        endDate: object().required('종료일자를 입력하세요.'),
      }),
      classId: string().required('그룹을 선택하세요.'),
      className: string().required('그룹을 선택하세요.'),
      coach: object({
        id: string().required('coach id를 입력하세요.'),
        name: string().required('coach 이름을 입력하세요.'),
        contact: object({
          email: string().email().required('이메일을 입력하세요.'),
          phone: string(),
        }),
      }).required('coach 정보를 입력하세요.'),
    });
  }

  static fromModel(course: Course, usernamePrefix: string): CourseFormModel {
    //
    if (!course.datePeriod || !course.coach) {
      throw new DramaException('CourseFormModel', 'DatePeriod and coach should not be null.');
    }

    return new CourseFormModel(
      course.datePeriod,
      course.classId,
      course.name,
      course.coach,
      usernamePrefix,
    );
  }
}

export default CourseFormModel;
