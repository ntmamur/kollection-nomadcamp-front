import React from 'react';
import { observer } from 'mobx-react';
import { Moment } from 'moment';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { ValidationForm } from '@nara.drama/prologue-util';
import { Box, FormControl, Grid, TextField } from '@nara.drama/material-ui';
import { Coach, DatePeriodPicker } from '@nara.drama/course';
import { Email } from '@material-ui/icons';
import { Modal, ProgressButton } from '~/comp/view';


interface Props {
  open: boolean;
  loading: boolean;
  troupeId: string;
  onSave: () => void;
  onClose: () => void;
}

interface State {
  selectedCoachIds: string[];
}


@autobind
@observer
class CourseFormModalView extends ReactComponent<Props, State> {
  //
  state: State = {
    selectedCoachIds: [],
  };

  render() {
    //
    const { open, loading, troupeId, onSave, onClose } = this.props;

    return (
      <Box width="100%" textAlign="right" onClick={(event: React.MouseEvent) => event.stopPropagation()}>
        <Modal open={open} onClose={onClose} maxWidth="md">
          <Modal.Header onClose={onClose}>Course 설정</Modal.Header>
          <Modal.Content>
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <FormControl.Label>coach 정보</FormControl.Label>
              </Grid>
              <Grid item xs={12}>
                <ValidationForm.Field name="coach">
                  {({ form, field, error }) => {
                    const coach = field.value as Coach;

                    return (
                      <Grid container spacing={3}>
                        <Grid item xs={12}>
                          <ValidationForm.Field
                            component={TextField}
                            fullWidth
                            name="name"
                            placeholder="이름"
                            error={error && !coach.name}
                            value={coach.name}
                            onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                              form.setFieldValue(`coach.${event.target.name}`, event.target.value)}
                          />
                        </Grid>
                        <Grid item xs={12}>
                          <ValidationForm.Field
                            component={TextField}
                            fullWidth
                            name="contact.email"
                            placeholder="이메일"
                            error={error && (!coach.contact || !coach.contact.email)}
                            value={coach.contact ? coach.contact.email : ''}
                            InputProps={{
                              startAdornment: <Email fontSize="small" />,
                            }}
                            onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                              form.setFieldValue(`coach.${event.target.name}`, event.target.value)}
                          />
                        </Grid>
                      </Grid>
                    );
                  }}
                </ValidationForm.Field>
              </Grid>

              <Grid item xs={12}>
                <FormControl.Label>기간</FormControl.Label>
              </Grid>
              <Grid item xs={12}>
                <ValidationForm.Field name="datePeriod">
                  {({ form, field, error, helperText }) => (
                    <DatePeriodPicker
                      onChangeStartDate={(date: Moment) => form.setFieldValue('datePeriod.startDate', date)}
                      onChangeEndDate={(date: Moment) => form.setFieldValue('datePeriod.endDate', date)}
                    />
                  )}
                </ValidationForm.Field>
              </Grid>

              <Grid item xs={12}>
                <FormControl.Label>그룹 선택</FormControl.Label>
              </Grid>
              <ValidationForm.Field name="classId">
                {({ form, field, error, helperText }) => (
                  <>
                    <Grid item xs={12}>
                      {/*<TroupeSelector*/}
                      {/*  troupeId={troupeId}*/}
                      {/*  fullWidth*/}
                      {/*  error={error}*/}
                      {/*  helperText={helperText}*/}
                      {/*  onSelect={(troupe: Troupe) => {*/}
                      {/*    form.setFieldValue('classId', troupe.id, true);*/}
                      {/*    form.setFieldValue('className', troupe.name, true);*/}
                      {/*  }}*/}
                      {/*/>*/}
                    </Grid>
                    <Grid item xs={12}>
                      {/* fixme: */}
                      {/*<TroupeActorList*/}
                      {/*  troupeId={troupeId}*/}
                      {/*  troupeId={field.value as string}*/}
                      {/*>*/}
                      {/*  <TroupeActorList.Content />*/}
                      {/*  <TroupeActorList.Pagination />*/}
                      {/*</TroupeActorList>*/}
                    </Grid>
                  </>
                )}
              </ValidationForm.Field>
            </Grid>
          </Modal.Content>
          <Modal.Actions>
            <ProgressButton
              loading={loading}
              buttonProps={{
                color: 'primary',
                onClick: onSave,
              }}
            >
              개설하기
            </ProgressButton>
          </Modal.Actions>
        </Modal>
      </Box>
    );
  }
}

export default CourseFormModalView;
