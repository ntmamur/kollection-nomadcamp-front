import React from 'react';
import { observer } from 'mobx-react';
import { Create, DeleteOutline } from '@material-ui/icons';
import { autobind, ReactComponent, ServiceInjector } from '@nara.drama/prologue';
import { CoursePlanDetail, CoursePlanForm, CoursePlanStateKeeper } from '@nara.drama/course';
import { dialogUtil } from '@nara.drama/prologue-util';
import { Box, Button } from '@nara.drama/material-ui';

import { Modal } from '~/comp/view';


interface Props {
  coursePlanId: string;
  audienceKey: string;
  onSuccess?: () => void;
}

interface InjectedProps {
  //
  coursePlanStateKeeper: CoursePlanStateKeeper;
}

@autobind
@observer
class CoursePlanDetailWithActionContainer extends ReactComponent<Props, {}, InjectedProps> {
  //
  static defaultProps = {
    onSuccess: () => {
    },
  };

  renderCoursePlanAction(): React.ReactNode {
    //
    const { coursePlanId, audienceKey } = this.props;

    return (
      <Box display="flex" justifyContent="flex-end" width="100%">
        <Modal trigger={<Button color="primary" variant="outlined" startIcon={<Create fontSize="small" />}>수정하기</Button>} maxWidth="lg">
          <Modal.Header>Course 기획 수정</Modal.Header>
          <Modal.Content>
            <CoursePlanForm
              audienceKey={audienceKey}
              coursePlanId={coursePlanId}
            >
              <CoursePlanForm.Form />
              <Box width="100%" textAlign="right">
                <Modal.CloseButton type="submit">저장</Modal.CloseButton>
              </Box>
            </CoursePlanForm>
          </Modal.Content>
        </Modal>
        <Box ml={2}>
          <Button color="primary" variant="outlined" startIcon={<DeleteOutline fontSize="small" />} onClick={this.onClickRemove}>삭제하기</Button>
        </Box>
      </Box>
    );
  }

  async onClickRemove() {
    //
    const { coursePlanId, onSuccess } = this.propsWithDefault;
    const { coursePlanStateKeeper } = this.injected;

    const confirmed = await dialogUtil.confirm({ title: '삭제', message: '삭제하시겠습니까?' });

    if (!confirmed) {
      return;
    }

    const response = await coursePlanStateKeeper.remove(coursePlanId);

    if (response.entityIds.length) {
      onSuccess();
    }
    else {
      dialogUtil.alert({ title: '삭제 실패', message: '삭제 중 오류가 발생하였습니다. 다시 시도해주시기 바랍니다.' });
    }
  }

  render() {
    //
    const { coursePlanId } = this.props;

    return (
      <CoursePlanDetail
        coursePlanId={coursePlanId}
        action={this.renderCoursePlanAction()}
      />
    );
  }
}

export default ServiceInjector.withContext(
  CoursePlanStateKeeper
)(CoursePlanDetailWithActionContainer);
