import React from 'react';
import { observer } from 'mobx-react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { Box, Button } from '@nara.drama/material-ui';
import { QueuePlayNext } from '@material-ui/icons';
import { BlockForm } from '@nara.drama/task';
import {Modal} from "~/comp/view/comp";


interface Props {
  audienceId: string;
  onSuccess?: () => void;
}

interface State {
  open: boolean;
}

@autobind
@observer
class TaskGroupBlockFormModalContianer extends ReactComponent<Props, State> {
  //
  static defaultProps = {
    onSuccess: () => {},
  };

  state: State = {
    open: false,
  };

  onOpen() {
    //
    this.setState({ open: true });
  }

  onClose() {
    //
    this.setState({ open: false });
  }

  onSuccess() {
    //
    this.onClose();
    this.propsWithDefault.onSuccess();
  }

  render() {
    //
    const { audienceId } = this.propsWithDefault;
    const { open } = this.state;

    return (
      <>
        <Button variant="outlined" color="primary" startIcon={<QueuePlayNext fontSize="small" />} onClick={this.onOpen}>등록하기</Button>
        <Modal open={open} maxWidth="lg" onClose={this.onClose}>
          <Modal.Header onClose={this.onClose}>과제 기획 등록</Modal.Header>
          <Modal.Content>
            <BlockForm
              audienceKey={audienceId}
              onSuccess={this.onSuccess}
            >
              <BlockForm.TaskCategoryForm />
              <BlockForm.TaskGroupBlockForm />
              <BlockForm.TaskBlockForms />
              <Box width="100%" textAlign="right">
                <Button type="submit" color="primary">등록</Button>
              </Box>
            </BlockForm>
          </Modal.Content>
        </Modal>
      </>
    );
  }
}

export default TaskGroupBlockFormModalContianer;
