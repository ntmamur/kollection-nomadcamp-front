import React from 'react';
import { observer } from 'mobx-react';
import moment, { Moment } from 'moment';
import { autobind, IdName, ReactComponent, ServiceInjector } from '@nara.drama/prologue';
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  Paper,
  Typography,
} from '@material-ui/core';
import _ from 'lodash';
import { LearningLogForm, LearningLogStateKeeper, Note } from '@nara.drama/learninglog';
import { ProjectWithLocModel, RefreshableCommitList } from '@nara.drama/gitmate';
import { TaskRecordsStateKeeper } from '@nara.drama/task';
import { Question, QuestionCdo, QuestionStateKeeper } from '@nara.drama/qna';
import { dialogUtil } from '@nara.drama/prologue-util';
import RateReviewIcon from '@material-ui/icons/RateReview';
import { NWithStyles, nWithStyles } from '../../theme';
import {QuestionFormModal, QuestionList} from "~/comp/view/drama-comp";
import {Editor} from "~/comp/view/comp";
import {DateUtil} from "~/comp/view/shared/util";


interface Props extends NWithStyles {
  //
  taskGroupId: string;
  taskGroupName: string;
  date: Moment;
  traineeIdName: IdName;
  audienceKey: string;
}

interface State {
  open: boolean;
  lastWrittenTime: Moment | null;
  setCurrentLearningLogProps: (key: string, value: any) => void;
  questionMap: Map<string, Question[]>;
}


interface InjectedProps {
  learningLogStateKeeper: LearningLogStateKeeper;
  taskRecordsStateKeeper: TaskRecordsStateKeeper;
  questionStateKeeper: QuestionStateKeeper;
}


@autobind
@observer
class LearningLogFormWithPreviewContainer extends ReactComponent<Props, State, InjectedProps> {
  //
  state: State = {
    open: false,
    lastWrittenTime: null,
    setCurrentLearningLogProps: () => {},
    questionMap: new Map(),
  };

  async componentDidMount() {
    //
    await this.init();
  }

  componentDidUpdate(prevProps: Readonly<Props>, prevState: Readonly<State>, snapshot?: any) {
    //
    const { taskGroupId: prevTaskGroupId } = prevProps;
    const { taskGroupId } = this.props;

    if (prevTaskGroupId !== taskGroupId) {
      this.init();
    }
  }

  async init() {
    //
    const { taskGroupId, date, traineeIdName } = this.props;
    const { learningLogStateKeeper, taskRecordsStateKeeper } = this.injected;
    const prevDate = moment(date).clone().subtract(1, 'day');

    await taskRecordsStateKeeper.findTaskRecordsByTraineeIdAndTaskGroupId(traineeIdName.id, taskGroupId);
    const prevDateLearningLog = await learningLogStateKeeper.findLearningLogByTraineeIdAndDate(traineeIdName.id, prevDate);

    this.setState({ lastWrittenTime: prevDateLearningLog ? prevDateLearningLog.writtenTime : prevDate });
  }

  setCurrentLearningLogProps(setCurrentLearningLogProps: (key: string, value: any) => void) {
    this.setState({ setCurrentLearningLogProps });
  }

  onOpen() {
    //
    this.setState({ open: true });
  }

  onClose() {
    //
    this.setState({ open: false });
  }

  setLocFromProjects(projects: ProjectWithLocModel[]) {
    const { setCurrentLearningLogProps } = this.state;
    const totalLoc = _.sum(projects.map(checkedProject => checkedProject.loc));

    setCurrentLearningLogProps('learningQuantity.loc', totalLoc);
  }

  onClickQuestionSave(event: React.MouseEvent, question: Question, closeModal?: any) {
    //
    const { questionMap } = this.state;
    const questionKey = this.makeMapKey();

    if (!question.title || !question.sentences) {
      dialogUtil.alert({ title: '안내', message: '제목과 내용을 작성해주세요.' });
      return;
    }

    let newQuestions: Question[] = [];
    const questions = questionMap.get(questionKey);

    if (questions) {
      newQuestions = [...questions];
    }

    newQuestions.push(question);

    questionMap.set(questionKey, newQuestions);

    this.setState({ questionMap }, () => {
      if (closeModal && closeModal.close && typeof closeModal.close === 'function') {
        closeModal.close();
      }
    });
  }

  onClickQuestionEdit(event: React.MouseEvent, question: Question, selectedIndex: number) {
    //
    const questions = this.getQuestions();

    if (!questions) {
      return;
    }

    questions[selectedIndex] = question;

    this.setQuestionMap(questions);
  }

  onClickQuestionDelete(question: Question, selectedIndex: number) {
    //
    const questions = this.getQuestions();

    if (!questions) {
      return;
    }

    const newQuestions = questions.filter((filteredQuestion: Question, index: number) => selectedIndex !== index);

    this.setQuestionMap(newQuestions);
  }

  setQuestionMap(questions: Question[]) {
    //
    const { questionMap } = this.state;
    const questionKey = this.makeMapKey();

    questionMap.set(questionKey, questions);

    this.setState({ questionMap });
  }

  getQuestions() {
    //
    const { questionMap } = this.state;
    const questionKey = this.makeMapKey();

    return questionMap.get(questionKey);
  }

  makeMapKey() {
    //
    const { taskGroupId, date } = this.props;
    return taskGroupId + '/' + date;
  }

  async onSuccess() {
    const { questionMap } = this.state;
    const res = await this.registerQuestions();

    if (res) {
      questionMap.clear();
      dialogUtil.alert({ title: '안내', message: '저장되었습니다.' });
    }
    else {
      dialogUtil.alert({ title: '안내', message: '문제가 발생했습니다.<br />관리자에게 문의하세요.' });
    }
  }

  async registerQuestions(): Promise<boolean> {
    //
    const { audienceKey } = this.props;
    const { questionMap } = this.state;
    const { questionStateKeeper } = this.injected;

    const allQuestion: Question[] = [];

    questionMap.forEach(
      (questions: Question[], key: string) => allQuestion.push(...questions)
    );

    if (allQuestion.length > 0) {
      const allQuestionCdo = allQuestion
        .map(
          (question: Question) => QuestionCdo.fromModel(question, audienceKey)
        );

      const response = await questionStateKeeper.registerQuestions(allQuestionCdo);

      return response.entityIds.length > 0;
    }
    else {
      return true;
    }

  }

  onBeforeClear() {
    //
    return dialogUtil.confirm({ message: '초기화 하시겠습니까?<br />초기화 이후 저장 버튼 클릭 시 반영됩니다.' });
  }

  getQuestionsForTask() {
    //
    const { questionMap } = this.state;
    const questionMapKey = this.makeMapKey();

    return questionMap.get(questionMapKey) || [];
  }

  render() {
    //
    const { classes, taskGroupId, date, traineeIdName, audienceKey, taskGroupName } = this.props;
    const { open, lastWrittenTime } = this.state;
    const { taskRecords } = this.injected.taskRecordsStateKeeper;
    const projectIds = taskRecords.map(taskRecord => taskRecord.projectId);

    if (!lastWrittenTime) {
      return null;
    }

    return (
      <>
        <LearningLogForm
          date={date}
          taskGroupId={taskGroupId}
          trainee={traineeIdName}
          audienceKey={audienceKey}
          setCurrentLearningLogProps={this.setCurrentLearningLogProps}
          defaultEmptyBookmark={false}
        >
          <Grid item xs={4}>
            <Box width="500px" display="flex" justifyContent="flex-end" alignItems="center">
              <LearningLogForm.Action
                onBeforeClear={this.onBeforeClear}
                saveButtonProps={{ color: 'primary', size: 'large' }}
                clearButtonProps={{ size: 'large' }}
                onSuccess={this.onSuccess}
              />
            </Box>
          </Grid>
          <Grid item xs={12}>
            <Box mt={1} mb={1} />
            <Divider />
            <Box mt={3} />
          </Grid>
          <Grid item xs={4}>
            <Box overflow="auto" pr={1} height="575px">
              <Box py={2}>
                <Typography component="h3" variant="h3">주제</Typography>
                <LearningLogForm.TopicForm />
              </Box>
              <Box py={3}>
                <Typography component="h3" variant="h3">학습 시간</Typography>
                <Box mt={1}>
                  <Paper variant="outlined">
                    <Box p={2}>
                      <LearningLogForm.LearningQuantityForm onClickLoc={this.onOpen} />
                    </Box>
                  </Paper>
                </Box>
              </Box>
              <Box py={2}>
                <Box display="flex" alignItems="center" justifyContent="space-between">
                  <Box>
                    <Typography component="h3" variant="h3">질문</Typography>
                  </Box>
                  <Box mt={1}>
                    <QuestionFormModal
                      date={date}
                      taskGroupName={taskGroupName}
                      onClickQuestionSave={this.onClickQuestionSave}
                    />
                  </Box>
                </Box>
                <Box mt={1}>
                  <Paper variant={'outlined'}>
                    <QuestionList
                      useEmptyList={false}
                      groupTags={[ DateUtil.formatToString(date), taskGroupName ]}
                      addQuestionOption={{
                        questions: this.getQuestionsForTask(),
                        type: 'unshift',
                      }}
                      onClickSave={this.onClickQuestionEdit}
                      onClickDelete={this.onClickQuestionDelete}
                    />
                  </Paper>
                </Box>
              </Box>
              <Box py={2}>
                <Box display="flex" alignItems="center" justifyContent="space-between">
                  <Box>
                    <Typography component="h3" variant="h3">참고자료</Typography>
                  </Box>
                  <LearningLogForm.BookmarkFormButton />
                </Box>
                <Box mt={1}>
                  <Paper variant={'outlined'}>
                    <Box p={1}>
                      <LearningLogForm.BookmarkForm
                        renderEmptyList={() => (
                          <Box p={1} width="100%"><Typography align="center">참고 자료를 추가해 주세요.</Typography></Box>
                        )}
                      />
                    </Box>
                  </Paper>
                </Box>
              </Box>
            </Box>
          </Grid>
          <Grid item xs={8}>
            <Box py={2} ml={4}>
              <Box ml={0} display="flex" alignItems="center">
                <RateReviewIcon />
                <Typography variant="h3">
                  &nbsp;학습 노트
                </Typography>
              </Box>
              <LearningLogForm.NoteForm
                renderContentField={(note: Note, setContent: (content: string) => void) => (
                  <Paper style={{ height: '485px' }}>
                    <Editor
                      contentId={note.id}
                      content={note.content}
                      onChange={setContent}
                    />
                  </Paper>
                )}
              />
            </Box>
          </Grid>
          {/*<Box mt={3}>*/}
          {/*  <Divider />*/}
          {/*  <LearningLogForm.Action*/}
          {/*    buttonProps={{ color: 'primary', size: 'large' }}*/}
          {/*    onSuccess={this.onSuccess}*/}
          {/*  />*/}
          {/*</Box>*/}
        </LearningLogForm>
        <RefreshableCommitList
          traineeTenantKey={traineeIdName.id}
          projectIds={projectIds}
          since={date.clone().startOf('date')}
          until={date.clone().endOf('date')}
          onInit={this.setLocFromProjects}
        >
          <Dialog open={open} onClose={this.onClose} fullWidth>
            <DialogTitle>레파지토리 확인</DialogTitle>
            <DialogContent>
              <Grid className={classes.list_cont_spacing}>
                <Typography><RefreshableCommitList.TotalCount /> 개의 프로젝트 푸시 내역이 있습니다.</Typography>
              </Grid>
              <RefreshableCommitList.Content
                onRefresh={this.setLocFromProjects}
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={this.onClose}>확인</Button>
            </DialogActions>
          </Dialog>
        </RefreshableCommitList>
      </>
    );
  }
}

export default ServiceInjector.withContext(
  LearningLogStateKeeper,
  TaskRecordsStateKeeper,
  QuestionStateKeeper,
)(nWithStyles(LearningLogFormWithPreviewContainer));
