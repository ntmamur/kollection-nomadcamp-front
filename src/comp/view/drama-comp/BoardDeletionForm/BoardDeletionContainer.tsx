import React from 'react';
import { autobind, ReactComponent, ServiceInjector } from '@nara.drama/prologue';
import { observer } from 'mobx-react';
import { CommentFeedbackStateKeeper } from '@nara.drama/feedback';
import dynamic from 'next/dynamic';


const BoardDeletion = dynamic<any>(
  () => import('@nara.drama/board').then(module => module.BoardDeletion),
  { ssr: false }
);

interface Props {
  boardId: string;
  onClickBack: () => void;
}

interface InjectedProps {
  commentFeedbackStateKeeper: CommentFeedbackStateKeeper;
}

@autobind
@observer
class BoardDeletionContainer extends ReactComponent<Props, {}, InjectedProps> {

  onDeleteFeedback(postIds: string[]) {
    const { commentFeedbackStateKeeper } = this.injected;

    //  관련 commentFeedback 전부 삭제
    commentFeedbackStateKeeper.removeCommentFeedbacks(postIds);
  }

  render() {

    const { boardId, onClickBack } = this.props;

    return (
      <BoardDeletion boardId={boardId} buttonText="게시판 삭제" onSuccess={onClickBack} onDeleteFeedback={this.onDeleteFeedback} />
    );
  }
}

export { BoardDeletionContainer };
export default ServiceInjector.withContext(
  CommentFeedbackStateKeeper,
)(BoardDeletionContainer);
