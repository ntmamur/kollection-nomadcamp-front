import React from 'react';
import { observer } from 'mobx-react';
import { autobind, ReactComponent, ServiceInjector } from '@nara.drama/prologue';
import { Grid, Paper, Typography } from '@nara.drama/material-ui';
import {
  Exercise,
  ExerciseReferenceForm,
  ExercisesStateKeeper,
  ExerciseTable,
  Lesson,
  LessonSelector,
} from '@nara.drama/course';


interface Props {
  lectureId: string;
}

interface State {
  exerciseId: string;
}

interface InjectedProps {
  exercisesStateKeeper: ExercisesStateKeeper;
}


@autobind
@observer
class LectureReferenceFormContainer extends ReactComponent<Props, State, InjectedProps> {
  //
  state: State = {
    exerciseId: '',
  };

  onSelectLesson(lesson: Lesson) {
    //
    this.setState({ exerciseId: '' });
    this.injected.exercisesStateKeeper.findExercisesByLessonId(lesson.id);
  }

  onClickExercise(event: React.MouseEvent, exercise: Exercise) {
    //
    this.setState({ exerciseId: exercise.id });
  }

  render() {
    //
    const { lectureId } = this.props;
    const { exerciseId } = this.state;
    const { exercises } = this.injected.exercisesStateKeeper;

    return (
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <LessonSelector lectureId={lectureId} onSelect={this.onSelectLesson} />
        </Grid>
        <Grid item xs>
          <ExerciseTable exercises={exercises} onClick={this.onClickExercise} />
        </Grid>
        <Grid item xs>
          <Paper style={{ height: '100%' }}>
            {exerciseId ? (
              <ExerciseReferenceForm exerciseId={exerciseId} />
            ) : (
              <Typography color="textSecondary" align="center">exercise를 선택하세요.</Typography>
            )}
          </Paper>
        </Grid>
      </Grid>
    );
  }
}

export default ServiceInjector.withContext(
  ExercisesStateKeeper
)(LectureReferenceFormContainer);
