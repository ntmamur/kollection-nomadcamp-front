import React from 'react';
import { observer } from 'mobx-react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { Box, Grid } from '@material-ui/core';
import { Question, QuestionDetail } from '@nara.drama/qna';
import { nWithStyles, NWithStyles } from '../../theme';
import {AnswerForm} from "~/comp/view/drama-comp";


interface Props extends NWithStyles {
  //
  question: Question | null;
  audienceId: string;
  onSuccess: () => void;
  onFail: () => void;
  answerable?: boolean;
}


@autobind
@observer
class CoachQuestionDetail extends ReactComponent<Props> {
  //
  static defaultProps = {
    answerable: true,
  };

  render() {
    //
    const { question, audienceId, onSuccess, onFail } = this.props;
    const { classes } = this.props;
    const { answerable } = this.propsWithDefault;

    if (!question) {
      return null;
    }

    if (answerable) {
      return (
        <Grid container>
          {/* 질문 S */}
          <Grid item xs={5} component="div" className={classes.queWrap}>
            <Box display="block">
              <QuestionDetail
                question={question}
              >
                <QuestionDetail.Header />
                <Box ml={11} overflow="auto" className={classes.ncommentQnawrap}>
                  <QuestionDetail.Content
                    hideTags
                  />
                </Box>
              </QuestionDetail>
            </Box>
          </Grid>
          <Grid item container xs={7} alignItems="flex-start" alignContent="flex-start">
            <AnswerForm
              question={question}
              audienceId={audienceId}
              onSuccess={onSuccess}
              onFail={onFail}
              editable={answerable}
            />
          </Grid>
        </Grid>
      );
    }
    else if (question.answered) {
      return (
        <Grid container>
          {/* 질문 S */}
          <Grid item xs={5} component="div" className={classes.queWrap}>
            <Box display="block">
              <QuestionDetail
                question={question}
              >
                <QuestionDetail.Header />
                <Box ml={11} overflow="auto" className={classes.ncommentQnawrap}>
                  <QuestionDetail.Content
                    hideTags
                  />
                </Box>
              </QuestionDetail>
            </Box>
          </Grid>
          <Grid item container xs={7} alignItems="flex-start" alignContent="flex-start">
            <AnswerForm
              question={question}
              audienceId={audienceId}
              onSuccess={onSuccess}
              onFail={onFail}
              editable={answerable}
            />
          </Grid>
        </Grid>
      );
    }
    else {
      return (
        <Grid container>
          {/* 질문 S */}
          <Grid item xs={12} component="div" className={classes.queWrap}>
            <Box display="block">
              <QuestionDetail
                question={question}
              >
                <QuestionDetail.Header />
                <Box ml={11} overflow="auto" className={classes.ncommentQnawrap}>
                  <QuestionDetail.Content
                    hideTags
                  />
                </Box>
              </QuestionDetail>
            </Box>
          </Grid>
        </Grid>
      );
    }
  }
}

export default nWithStyles(CoachQuestionDetail);
