import React from 'react';
import { observer } from 'mobx-react';
import { autobind, ReactComponent, ServiceInjector } from '@nara.drama/prologue';
import { SubjectDetail, SubjectForm, SubjectStateKeeper } from '@nara.drama/course';
import { dialogUtil } from '@nara.drama/prologue-util';
import { Box, Button } from '@nara.drama/material-ui';
import { Modal } from '~/comp/view';


interface Props {
  subjectId: string;
  audienceKey: string;
  onSuccess?: () => void;
}

interface InjectedProps {
  //
  subjectStateKeeper: SubjectStateKeeper;
}

@autobind
@observer
class SubjectDetailWithActionContainer extends ReactComponent<Props, {}, InjectedProps> {
  //
  static defaultProps = {
    onSuccess: () => {
    },
  };

  renderAction(): React.ReactNode {
    //
    const { subjectId, audienceKey } = this.props;

    return (
      <Box width="100%" display="flex" justifyContent="flex-end">
        <Modal trigger={<Button color="primary">수정하기</Button>} maxWidth="lg">
          <Modal.Header>Subject 수정</Modal.Header>
          <Modal.Content>
            <SubjectForm
              audienceKey={audienceKey}
              subjectId={subjectId}
            >
              <SubjectForm.Form />
              <Box width="100%" textAlign="right">
                <Modal.CloseButton type="submit">저장</Modal.CloseButton>
              </Box>
            </SubjectForm>
          </Modal.Content>
        </Modal>
        <Box ml={2}>
          <Button variant="text" color="primary" onClick={this.onClickRemove}>삭제하기</Button>
        </Box>
      </Box>
    );
  }

  async onClickRemove() {
    //
    const { subjectId, onSuccess } = this.propsWithDefault;
    const { subjectStateKeeper } = this.injected;

    const confirmed = await dialogUtil.confirm({ title: '삭제', message: '삭제하시겠습니까?' });

    if (!confirmed) {
      return;
    }

    const response = await subjectStateKeeper.remove(subjectId);

    if (response.entityIds.length) {
      onSuccess();
    }
    else {
      dialogUtil.alert({ title: '삭제 실패', message: '삭제 중 오류가 발생하였습니다. 다시 시도해주시기 바랍니다.' });
    }
  }

  render() {
    //
    const { subjectId } = this.props;

    return (
      <SubjectDetail
        subjectId={subjectId}
        action={this.renderAction()}
      />
    );
  }
}

export default ServiceInjector.withContext(
  SubjectStateKeeper
)(SubjectDetailWithActionContainer);
