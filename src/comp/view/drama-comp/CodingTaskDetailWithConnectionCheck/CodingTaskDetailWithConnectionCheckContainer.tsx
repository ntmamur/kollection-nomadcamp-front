import React from 'react';
import { observer } from 'mobx-react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { CodingTaskDetail } from '@nara.drama/exam';
import { dialogUtil } from '@nara.drama/prologue-util';


interface Props {
  toList: () => void;
  codingTaskId: string;
}


@autobind
@observer
class CodingTaskDetailWithConnectionCheckContainer extends ReactComponent<Props> {
  //
  async check(refId: string) {
    // const foundGit = await this.injected.exerciseTemplatesStateKeeper.existsExerciseTemplate(refId);
    // return foundGit ? foundGit.webUrl : '';
    dialogUtil.alert({ message: '준비중입니다.' });
    return '';
  }

  render() {
    //
    const { toList, codingTaskId } = this.props;

    return (
      <CodingTaskDetail
        id={codingTaskId as string}
      >
        <CodingTaskDetail.BasicInfo
          modelCodeConnectionTest={this.check}
          gitInfoConnectionTest={this.check}
          taskImage={`${process.env.BASE_PATH}/images/exam/coding_task.svg`}
          toList={toList}
        />
      </CodingTaskDetail>
    );
  }
}

export default CodingTaskDetailWithConnectionCheckContainer;
