import React from 'react';
import { observer } from 'mobx-react';
import { Moment } from 'moment';
import { autobind, ReactComponent, Terms } from '@nara.drama/prologue';
import {
  Button,
} from '@material-ui/core';
import { Add } from '@material-ui/icons';
import { Question } from '@nara.drama/qna';
import {Modal} from "~/comp/view/comp";
import {QuestionForm} from "~/comp/view/drama-comp";
import {DateUtil} from "~/comp/view/shared/util";
import {Dictionary, TermKeys} from "~/comp/view/shared/terms";


interface Props {
  //
  date: Moment;
  taskGroupName: string;
  onClickQuestionSave: (event: React.MouseEvent, question: Question, closeModal?: any) => void;
}


@autobind
@observer
class QuestionFormModalView extends ReactComponent<Props> {
  //
  render() {
    //
    const { date, taskGroupName, onClickQuestionSave } = this.props;

    return (
      <Modal
        maxWidth="md"
        trigger={
          <Button variant="contained" style={{ backgroundColor: '#fff', minWidth: '44px', height: '42px' }}>
            <Add />
          </Button>
        }
      >
        {(close: object) => (
          <>
            <Modal.Header>{Terms(Dictionary[TermKeys.QnaRegistration])}</Modal.Header>
            <Modal.Content>
              <QuestionForm
                date={DateUtil.formatToString(date)}
                taskGroupName={taskGroupName}
                onClick={(event: React.MouseEvent, question: Question) => onClickQuestionSave(event, question, close)}
              />
            </Modal.Content>
          </>
        )}
      </Modal>
    );
  }
}

export default QuestionFormModalView;
