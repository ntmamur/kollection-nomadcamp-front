import dynamic from 'next/dynamic';


const BoardListTab = dynamic<any>(
  () => import('./BoardListTab').then(module => module),
  { ssr: false }
);

const PostCommentList = dynamic<any>(
  () => import('./PostCommentList').then(module => module),
  { ssr: false }
);

export { BoardListTab };
export { PostCommentList };


// drama-qna
export { default as CoachQuestionDetail } from './CoachQuestionDetail';
export { default as QuestionList } from './QuestionList';
export { default as AnswerForm } from './AnswerForm';
export { default as QuestionFormModal } from './QuestionFormModal';
export { default as QuestionForm } from './QuestionForm';
export { default as QuestionTabList } from './QuestionTabList';

// drama-task
export { default as TraineeTaskGroupList } from './TraineeTaskGroupList';
export { default as TraineeTaskGroupDetail } from './TraineeTaskGroupDetail';
export { default as ProjectConnectionForm } from './ProjectConnectionForm';
export { default as TaskGroupDetail } from './TaskGroupDetail';
export { default as TaskGroupAutoSelector } from './TaskGroupAutoSelector';
export { default as TaskGroupBlockFormModal } from './TaskGroupBlockFormModal';
export { default as TaskGroupFormModal } from './TaskGroupFormModal';
export { default as TaskGroupBlockDetail } from './TaskGroupBlockDetail';

// drama-learninglog
export { default as LearningLogFormWithPreview } from './LearningLogFormWithPreview';
export { default as DailyReviewList } from './DailyReviewList';
export { default as TraineeLearningLogList, TraineeLearningLogListTypes } from './TraineeLearningLogList';
export { default as CommentForm } from './CommentForm';
export { default as LearningLogGroup } from './LearningLogGroup';

