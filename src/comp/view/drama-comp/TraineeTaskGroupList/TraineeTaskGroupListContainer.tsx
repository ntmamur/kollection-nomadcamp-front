import React from 'react';
import { observer } from 'mobx-react';
import {autobind, ReactComponent, ServiceInjector, Terms} from '@nara.drama/prologue';
import { AutoPagination } from '@nara.drama/prologue-util';
import { MyTaskGroupRecordsStateKeeper, TaskGroup, TaskGroupList } from '@nara.drama/task';
import { Typography } from '@material-ui/core';
import { Dictionary, TermKeys } from '~/comp/view/shared/terms';
import {NoData} from "~/comp/view/comp";
import { PageLayout} from "~/comp/view/layout";

interface Props {
  actorId: string;
  castId: string;
  onClick?: (event: React.MouseEvent, taskGroup: TaskGroup) => void;
}

interface State {
  taskGroupCount: number;
}

interface InjectedProps {
  myTaskGroupRecordsStateKeeper: MyTaskGroupRecordsStateKeeper;
}


@autobind
@observer
class TraineeTaskGroupListContainer extends ReactComponent<Props, State, InjectedProps> {
  //
  static defaultProps = {
    onClick: () => {
    },
  };

  state: State = {
    taskGroupCount: 0,
  };

  onInit(taskGroupIds: string[]) {
    //
    const { actorId } = this.props;
    const { myTaskGroupRecordsStateKeeper } = this.injected;

    myTaskGroupRecordsStateKeeper.findMyTaskGroupRecordsByTraineeIdAndTaskGroupIds(actorId, taskGroupIds);
    this.setState({ taskGroupCount: taskGroupIds.length });
  }

  render() {
    //
    const { castId, onClick } = this.propsWithDefault;
    const { taskGroupCount } = this.state;

    if (!castId) {
      return null;
    }

    return (
      <TaskGroupList
        classIds={[castId]}
        onInit={this.onInit}
      >
        {taskGroupCount ? (
          <>
            {/* fixme: pageLayout 위치 고민.. */}
            <PageLayout.Content>
              <Typography component="p">
                <AutoPagination.TotalCount />개의 과목이 있습니다.
              </Typography>
            </PageLayout.Content>
            <PageLayout.Content>
              <TaskGroupList.Content
                onClick={onClick}
              />
            </PageLayout.Content>
          </>
        ) : (
          <NoData text={Terms(Dictionary[TermKeys.NoTaskMessage])}/>
        )}
      </TaskGroupList>
    );
  }
}

export default ServiceInjector.withContext(
  MyTaskGroupRecordsStateKeeper,
)(TraineeTaskGroupListContainer);
