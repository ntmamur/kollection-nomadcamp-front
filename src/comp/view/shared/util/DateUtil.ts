import moment, { Moment } from 'moment';


class DateUtil {
  private static readonly dateFormat = 'YYYY-MM-DD';

  static formatToString(date: Moment) {
    //
    return date.format(this.dateFormat);
  }

  static formatFromString(date: string) {
    //
    return moment(date, this.dateFormat);
  }

  static formatFromStringWithTime(date: string) {
    //
    let dateMoment = this.formatFromString(date);

    if (date === this.formatToString(moment())) {
      dateMoment = moment().set({
        year: dateMoment.year(),
        month: dateMoment.month(),
        date: dateMoment.date(),
      });
    }

    return dateMoment;
  }
}

export default DateUtil;
