class QnaUtil {
  static genClientAccountId(actorKey: string, kollectionEditionId: string) {
    return `${actorKey}-${kollectionEditionId}`;
  }
}

export default QnaUtil;
