import { Term } from '@nara.drama/prologue';
import {SharedTermsKeySet, TermsKeySet} from '~/comp/view/shared';

export const SharedDictionary: TermsKeySet = {
  done: Term.newWithRussian('완료', 'Done', 'Выполнено'),
  ok: Term.newWithRussian('OK', 'OK', 'OK'),
  cancel: Term.newWithRussian('취소', 'Cancel', 'Отмена'),
  board: Term.newWithRussian('', '', ''),
  learningLog: Term.newWithRussian('', '', ''),
  qna: Term.newWithRussian('', '', ''),
  review: Term.newWithRussian('', '', ''),
  task: Term.newWithRussian('', '', ''),
};
