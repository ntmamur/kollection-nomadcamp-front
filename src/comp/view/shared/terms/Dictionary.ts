import { Term } from '@nara.drama/prologue';
import { TermsKeySet } from '~/comp/view/shared/terms';


export const Dictionary: TermsKeySet = {
  learningLogMenu: Term.newWithRussian('러닝로그', 'Learning Log', 'Журнал'),
  reviewMenu: Term.newWithRussian('리뷰', 'Review', 'Обзор'),
  qnaMenu: Term.newWithRussian('Q & A', 'Q & A', 'Q & A'),
  boardMenu: Term.newWithRussian('게시판', 'Board', 'Доска'),
  taskMenu: Term.newWithRussian('과제', 'Task', 'Задача'),

  //Qna Part  i18n
  qnaRegistration: Term.newWithRussian('질문 작성하기', 'Registration', 'Напишите'),
  qnaPageDescription: Term.newWithRussian(
    `나무소리 교육 프로그램 혹은 훈련에 관해 궁금하신 게 있으신가요?\n 코치에게 질문을 남겨주세요.`,
    `Do you have any questions about the tree sound education program or training?\nLeave a question to the coach.`,
    'У вас есть какие-либо вопросы о программе или обучении Namoosori?\nОставьте вопрос тренеру.'
  ),
  qnaEmptyMessage: Term.newWithRussian(' 등록된 질문이 없습니다.', 'There is no question.', 'Нет вопросов.'),

  //Board Part  i18n
  boardPageDescription: Term.newWithRussian(
    '나무소리 교육에 대한 새로운 소식을 확인해 보세요.',
    'Get the latest news on Namoosori Education Platform.',
    'Получайте последние новости на Образовательной платформе Namoosori.'),

  // Task Part i18n
  taskPageDescription: Term.newWithRussian(
    '프로그래밍 기술 학습을 위해 코치가 직접 선별한 내게 맞는 과제를 제공합니다.\n문법을 하나씩 배워가며 역량을 키워보세요.',
    'To learn programming skills, we provide you with hand-picked assignments. \nLearn step by step and develop your skills.',
    'Мы предоставляем вам тщательно подобранные задания.\nНаучитесь шаг за шагом и развить свои навыки.'
  ),

  //Learning Log Part i18n
  learningLogDescription: Term.newWithRussian(
    '매일 학습한 내용을 기록하세요.',
    'Record what you learn every day.',
    'Запишите, что вы узнали каждый день.'),
  datePickerMessage: Term.newWithRussian(
    '날짜 변경시 저장하지 않은 데이터는 삭제됩니다.',
    '*Unsaved data will be deleted \nwhen the date is changed.',
    '*Несохраненные данные будут \nудалены при изменении даты.'),
  noActiveTaskMessage: Term.newWithRussian('진행 중인 과제가 없습니다.','There are no assignments','Нет заданий.'),

  //Review Part i18n
  reviewPageDescription: Term.newWithRussian(
    '러닝로그에 작성하신 학습일지에 대한 코멘트 및\nQ&A에 질문하신 답변을 확인하실 수 있습니다.',
    'You can check comments on Learning Log and \nanswers to questions asked in Q&A part.',
    'Вы можете проверить комментарии в журнале обучения и\nответы на вопросы, заданные в разделе Q&A.'
  ),
  reviewCountMessage: Term.newWithRussian( '개의 리뷰가 있습니다.', 'Review(s)', 'Комментарии'),
  noTaskMessage: Term.newWithRussian('등록된 과제가 없습니다.','No registered assignments','Нет зарегистрированных заданий'),
};
