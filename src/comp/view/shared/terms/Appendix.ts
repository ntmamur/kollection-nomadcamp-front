import { Term } from '@nara.drama/prologue';


export interface TermsKeySet {
  learningLogMenu: Term;
  reviewMenu: Term;
  qnaMenu: Term;
  boardMenu: Term;
  taskMenu: Term;
  qnaRegistration: Term;
  qnaPageDescription: Term;
  qnaEmptyMessage: Term;
  boardPageDescription: Term;
  taskPageDescription: Term;
  learningLogDescription: Term;
  datePickerMessage: Term;
  noActiveTaskMessage: Term;
  reviewPageDescription: Term;
  reviewCountMessage: Term;
  noTaskMessage: Term;
}

export enum TermKeys {
  LearningLogMenu = 'learningLogMenu',
  ReviewMenu = 'reviewMenu',
  QnaMenu = 'qnaMenu',
  BoardMenu = 'boardMenu',
  TaskMenu = 'taskMenu',
  QnaRegistration = 'qnaRegistration',
  QnaPageDescription = 'qnaPageDescription',
  QnaEmptyMessage = 'qnaEmptyMessage',
  BoardPageDescription = 'boardPageDescription',
  TaskPageDescription = 'taskPageDescription',
  NoActiveTaskMessage = 'noActiveTaskMessage',
  LearningLogDescription = 'learningLogDescription',
  DatePickerMessage = 'datePickerMessage',
  NoTaskMessage = 'noTaskMessage',
  ReviewPageDescription = 'reviewPageDescription',
  ReviewCountMessage = 'reviewCountMessage'
}
export interface SharedTermsKeySet {
  board: Term;
  learningLog: Term;
  qna: Term;
  review: Term;
  task: Term;
  done: Term;
  ok: Term;
  cancel: Term;
}
export enum SharedTermsKey {
  Done = 'done',
  Ok = 'ok',
  Cancel = 'cancel'
}
