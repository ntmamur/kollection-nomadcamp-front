import React from 'react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import {
  EditorState,
  ContentState,
  convertToRaw,
} from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import { Editor, EditorProps } from 'react-draft-wysiwyg';
import htmlToDraft from 'html-to-draftjs';


interface Props extends Omit<EditorProps, 'onChange'> {
  contentId: string;
  content?: string;
  defaultContent?: string;
  onChange?: (content: string) => void;
}

interface State {
  editorState: EditorState;
}

@autobind
class RichTextEditorContainer extends ReactComponent<Props, State> {
  //
  static defaultProps = {
    defaultContent: '',
    content: '',
    onChange: () => {},
  };

  constructor(props: Props) {
    super(props);

    this.state = {
      editorState: this.getEmptyEditorState(),
    };
  }

  componentDidMount() {
    //
    this.syncContent();
  }

  componentDidUpdate(prevProps: Readonly<Props>, prevState: Readonly<State>, snapshot?: any) {
    //
    const { contentId: prevContentId } = prevProps;
    const { contentId } = this.props;

    if (prevContentId !== contentId) {
      this.syncContent();
    }
  }

  clearContent() {
    //
    this.setState({ editorState: EditorState.createEmpty() });
  }

  onEditorStateChange(editorState: EditorState) {
    //
    const contentState = editorState.getCurrentContent();
    const rawDraftContentState = convertToRaw(contentState);
    const editorToHtml = draftToHtml(rawDraftContentState);

    this.propsWithDefault.onChange(editorToHtml);
    this.setState({ editorState });
  }

  syncContent() {
    //
    const { editorState } = this.state;
    const { content } = this.propsWithDefault;

    if (content) {
      const blocksFromHtml = htmlToDraft(content);

      if (blocksFromHtml) {
        const { contentBlocks, entityMap } = blocksFromHtml;
        const contentState = ContentState.createFromBlockArray(contentBlocks, entityMap);
        const prevEditorState = EditorState.push(editorState, contentState, editorState.getLastChangeType());
        const newEditorState = EditorState.moveFocusToEnd(prevEditorState);

        this.setState({
          editorState: newEditorState,
        });
      }
    }
    else {
      this.setState({
        editorState: this.getEmptyEditorState(),
      });
    }
  }

  getEmptyEditorState() {
    //
    const { defaultContent } = this.propsWithDefault;

    return EditorState.createWithContent(
      ContentState.createFromText(defaultContent)
    );
  }

  render() {
    //
    const { content, onChange, ...otherProps } = this.props;
    const { editorState } = this.state;

    //fixme: type error
    return (
      <Editor
        {...otherProps}
        //@ts-ignore
        editorState={editorState}
        localization={{
          locale: 'ko',
        }}
        //@ts-ignore
        onEditorStateChange={this.onEditorStateChange}
      />
    );
  }
}

export default RichTextEditorContainer;
