import React from 'react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import RichTextEditor from './RichTextEditor/RichTextEditorContainer';
import MarkdownEditor from './MarkdownEditor/MarkdownEditorContainer';


interface Props {
  contentId: string;
  content?: string;
  onChange?: (content?: string) => void;
  type?: 'RichText' | 'Markdown';
  height?: number;
  readOnly?: boolean;
  }

@autobind
class EditorContainer extends ReactComponent<Props> {
  //
  static defaultProps = {
    content: '',
    onChange: () => {},
    type: 'Markdown',
    height: 485,
    readOnly: false,
  };

  renderEditor() {
    const { contentId, content } = this.props;
    const { type, onChange, height, readOnly } = this.propsWithDefault;

    if (type === 'RichText') {
      return (
        <RichTextEditor
          contentId={ contentId }
          content={ content }
          onChange={ onChange }
        />
      );
    }
    else if (type === 'Markdown') {
      return (
        <MarkdownEditor
          contentId={ contentId }
          content={ content }
          onChange={ onChange }
          height={ height }
          readOnly={ readOnly }
        />
      );
    }
    else {
      throw new Error('Type must be define');
    }
  }

  render() {
    return this.renderEditor();
  }
}

export default EditorContainer;
