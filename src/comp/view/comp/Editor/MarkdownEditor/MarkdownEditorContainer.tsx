import React from 'react';
import MEDitor from '@uiw/react-md-editor';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { MDEditorProps} from "@uiw/react-md-editor";


interface Props extends Omit<MDEditorProps, 'onChange'> {
  contentId: string;
  content?: string;
  defaultContent?: string;
  onChange?: (value?: string) => void;
  height?: number;
  readOnly?: boolean;
}

interface State {
  markdown: string | undefined;
}

@autobind
class MarkdownEditorContainer extends ReactComponent<Props, State> {
  //
  static defaultProps = {
    content: '',
    defaultContent: '',
    onChange: () => {},
    height: 485,
    readOnly: false,
  };

  constructor(props: Props) {
    super(props);

    this.state = {
      markdown: '',
    };
  }

  componentDidMount() {
    this.syncContent();
  }

  componentDidUpdate(prevProps: Readonly<Props>, prevState: Readonly<State>, snapshot?: any) {
    //
    const { contentId: prevContentId } = prevProps;
    const { contentId } = this.props;

    if (prevContentId !== contentId) {
      this.syncContent();
    }
  }

  syncContent() {
    const { content, defaultContent } = this.propsWithDefault;

    if (content) {
      this.setState({
        markdown: content,
      });
    }
    else {
      this.setState({
        markdown: defaultContent,
      });
    }
  }

  updateMarkdown(value: string | undefined) {
    this.setState({ markdown: value });
    this.propsWithDefault.onChange(value);
  }


  render() {
    const { contentId, defaultContent, content, onChange, height, readOnly, ...otherProps } = this.propsWithDefault;
    const value = this.state.markdown;

    return readOnly ? (
      <MEDitor.Markdown
        {...otherProps}
        source={value}
      />
    ) : (
      <MEDitor
        {...otherProps}
        value={value}
        onChange={this.updateMarkdown}
        height={height}
      />
    );
  }
}


export default MarkdownEditorContainer;
