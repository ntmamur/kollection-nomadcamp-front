import React from 'react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { Box } from '@nara.drama/material-ui';
import { Search } from '@material-ui/icons';
import { Typography } from '@material-ui/core';
import { NWithStyles, nWithStyles } from '~/comp/view/theme';


interface Props extends NWithStyles {
  simple?: boolean;
  text?: string;
}

@autobind
class NoDataView extends ReactComponent<Props> {
  //
  static defaultProps = {
    simple: false,
    text: '등록된 글이 없습니다.',
  };

  render() {
    //
    const { classes, simple, text } = this.propsWithDefault;

    return simple ? (
      <Box height="100%" display="flex" justifyContent="center" flexDirection="column">
        <Box textAlign="center">
          <Typography component="p" variant="body1">
            <Box>현재 진행 중인 과제가 없습니다.</Box>
          </Typography>
        </Box>
      </Box>
    ) : (
      <Box className={classes.nno_wrap}>
        <Box className={classes.nnot_con} display="flex" justifyContent="center">
          <Search fontSize="large" />&nbsp;&nbsp;{text}
        </Box>
      </Box>
    );
  }
}

export default nWithStyles(NoDataView);
