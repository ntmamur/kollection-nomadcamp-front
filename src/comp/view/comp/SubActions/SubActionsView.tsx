import React, { ReactNode } from 'react';
import { autobind, ReactComponent } from '@nara.drama/prologue';

import { BoxProps } from '@material-ui/core';
import { Grid, Box } from '@nara.drama/material-ui';
import { WithStyles, withStyles } from './style';


type Props = {
  left?: ReactNode;
  right?: ReactNode;
} & WithStyles & BoxProps;

@autobind
class SubActionsView extends ReactComponent<Props> {
  //
  render() {
    //
    const { classes, children, left, right, ...rest } = this.props;

    if (children) {
      return (
        <Box my={2} {...rest}>
          <Grid container>
            {children}
          </Grid>
        </Box>
      );
    }
    else {
      return (
        <Box my={2} {...rest}>
          <Grid container>
            <Grid item xs className={classes.left}>
              {left}
            </Grid>
            <Grid item xs className={classes.right}>
              {right}
            </Grid>
          </Grid>
        </Box>
      );
    }
  }
}

export default withStyles(SubActionsView);
