import SubActionsView from './SubActionsView';
import Left from './sub-comp/Left';
import Right from './sub-comp/Right';


type SubActionsComponent = typeof SubActionsView & {
  Left: typeof Left;
  Right: typeof Right;
};

const SubActions = SubActionsView as SubActionsComponent;

SubActions.Left = Left;
SubActions.Right = Right;

export default SubActions;
