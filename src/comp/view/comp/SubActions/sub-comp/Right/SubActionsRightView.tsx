import React, { ReactNode } from 'react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { Grid } from '@nara.drama/material-ui';
import { WithStyles, withStyles } from '../../style';


interface Props extends WithStyles {
  children: ReactNode;
}

@autobind
class SubActionsView extends ReactComponent<Props> {
  //
  render() {
    //
    const { classes, children } = this.props;

    return (
      <Grid item xs className={classes.right}>
        {children}
      </Grid>
    );
  }
}

export default withStyles(SubActionsView);
