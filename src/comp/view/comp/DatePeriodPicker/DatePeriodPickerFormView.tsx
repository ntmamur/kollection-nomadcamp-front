import React from 'react';
import { autobind, DatePeriod, ReactComponent } from '@nara.drama/prologue';
import { observer } from 'mobx-react';
import { Moment } from 'moment';
import { Box, DatePicker, Grid, Typography } from '@nara.drama/material-ui';
import { KeyboardDatePickerProps } from '@material-ui/pickers';


interface Props extends Omit<KeyboardDatePickerProps, 'value' | 'error' | 'helperText' | 'onChange'> {
  defaultDatePeriod?: DatePeriod;
  onChangeStartDate?: (date: Moment) => void;
  onChangeEndDate?: (date: Moment) => void;
}

interface State {
  //
  datePeriod: DatePeriod;
}

@autobind
@observer
class DatePeriodPickerFormView extends ReactComponent<Props, State> {
  //
  static defaultProps = {
    disableToolbar: true,
    fullWidth: true,
    format: 'YYYY-MM-DD',
    onChangeStartDate: () => {},
    onChangeEndDate: () => {},
  };

  state: State = {
    datePeriod: DatePeriod.now(),
  };

  componentDidMount() {
    //
    this.init();
  }

  init() {
    //
    const { defaultDatePeriod } = this.props;

    this.setState({ datePeriod: defaultDatePeriod || DatePeriod.now() });
  }

  onChangeStartDate(startDate: Moment) {
    //
    const { onChangeStartDate, onChangeEndDate } = this.propsWithDefault;
    const { datePeriod } = this.state;

    this.setState(({ datePeriod: prevDatePeriod }) => ({
      datePeriod: {
        ...prevDatePeriod,
        startDate,
      } as DatePeriod,
    }));
    onChangeStartDate(startDate);

    if (startDate.isAfter(datePeriod.endDate)) {
      this.setState(({ datePeriod: prevDatePeriod }) => ({
        datePeriod: {
          ...prevDatePeriod,
          endDate: startDate,
        } as DatePeriod,
      }));
      onChangeEndDate(startDate);
    }
  }

  onChangeEndDate(endDate: Moment) {
    //
    const { onChangeStartDate, onChangeEndDate } = this.propsWithDefault;
    const { datePeriod } = this.state;

    this.setState(({ datePeriod: prevDatePeriod }) => ({
      datePeriod: {
        ...prevDatePeriod,
        endDate,
      } as DatePeriod,
    }));
    onChangeEndDate(endDate);

    if (endDate.isBefore(datePeriod.startDate)) {
      this.setState(({ datePeriod: prevDatePeriod }) => ({
        datePeriod: {
          ...prevDatePeriod,
          startDate: endDate,
        } as DatePeriod,
      }));
      onChangeStartDate(endDate);
    }
  }

  render() {
    //
    const { defaultDatePeriod, onChangeStartDate, onChangeEndDate, ...otherProps } = this.propsWithDefault;
    const { datePeriod } = this.state;

    return (
      <Grid container spacing={3} display="flex" alignItems="center">
        <Grid item xs={5}>
          <DatePicker
            label="시작"
            {...otherProps}
            value={datePeriod.startDate}
            onChange={this.onChangeStartDate}
          />
        </Grid>
        <Grid item xs>
          <Box width="100%" textAlign="center">
            <Typography>~</Typography>
          </Box>
        </Grid>
        <Grid item xs={5}>
          <DatePicker
            label="종료"
            {...otherProps}
            value={datePeriod.endDate}
            onChange={this.onChangeEndDate}
          />
        </Grid>
      </Grid>
    );
  }
}

export default DatePeriodPickerFormView;
