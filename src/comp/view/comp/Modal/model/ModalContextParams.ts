interface ModalContextParams {
  //
  close: () => void;
}

export default ModalContextParams;
