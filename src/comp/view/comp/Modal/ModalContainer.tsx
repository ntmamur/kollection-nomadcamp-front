import React from 'react';
import { ReactComponent, autobind } from '@nara.drama/prologue';

import { DialogProps } from '@material-ui/core';
import { Trigger, TriggerTypes, Dialog } from '@nara.drama/material-ui';


interface Props {
  children: React.ReactNode;
  trigger?: React.ReactElement;
  open?: boolean;
  maxWidth?: DialogProps['maxWidth'];
  fullWidth?: DialogProps['fullWidth'];
  className?: string;
  onClose?: DialogProps['onClose'];
}

interface State {
  open: boolean;
}

@autobind
class ModalContainer extends ReactComponent<Props, State> {
  //
  static defaultProps = {
    trigger: undefined,
    open: undefined,
    maxWidth: 'sm',
    fullWidth: true,
    className: '',
    onClose: () => {
    },
  };


  renderChildren(closeCallback?: (event: React.SyntheticEvent, ...rest: any[]) => void) {
    //
    const { children } = this.props;
    let targetChildren = children;

    if (typeof children === 'function' && closeCallback) {
      targetChildren = children({ close: closeCallback });
    }

    return targetChildren;
  }

  render() {
    //
    const { trigger, open, onClose, className, children, ...otherProps } = this.props;

    return trigger ? (
      <Trigger
        element={trigger}
        onClose={onClose}
      >
        {(context: TriggerTypes.TriggerContextParams) => (
          <Dialog
            {...otherProps}
            className={className}
            open={open || context.open}
            onClose={context.onClose}
          >
            {this.renderChildren(context.onClose)}
          </Dialog>
        )}
      </Trigger>
    ) : (
      <Dialog
        {...otherProps}
        className={className}
        open={open || false}
        onClose={onClose}
      >
        {this.renderChildren()}
      </Dialog>
    );
  }
}

export default ModalContainer;
