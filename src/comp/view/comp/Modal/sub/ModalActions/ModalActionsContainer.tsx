import React from 'react';
import autobind from 'autobind-decorator';

import { Dialog } from '@nara.drama/material-ui';


interface Props {
  children: React.ReactNode;
  className?: string;
}

@autobind
class ModalActionsContainer extends React.Component<Props> {
  //
  static defaultProps = {
    className: '',
  };


  render() {
    //
    const { className, children, ...otherProps } = this.props;

    return (
      <Dialog.Actions
        className={className}
        {...otherProps}
      >
        {children}
      </Dialog.Actions>
    );
  }
}

export default ModalActionsContainer;

