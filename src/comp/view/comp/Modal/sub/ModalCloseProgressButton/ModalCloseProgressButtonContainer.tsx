import React from 'react';
import { autobind, ReactComponent, NaraException } from '@nara.drama/prologue';

import { ButtonProps, CircularProgressProps } from '@material-ui/core';
import { Trigger } from '@nara.drama/material-ui';
import ProgressButton from '../../../ProgressButton';


interface Props extends ButtonProps {
  //
  children?: React.ReactNode;
  onClickWithClose?: (event: React.MouseEvent<HTMLButtonElement>, close: () => void) => void;
  loading?: boolean;
  progressProps?: CircularProgressProps;
}

@autobind
class ModalCloseProgressButtonContainer extends ReactComponent<Props> {
  //
  static defaultProps = {
    children: '취소',
    color: 'secondary',
    onClick: () => {
    },
    onClickWithClose: undefined,
  };

  static contextType = Trigger.Context;
  context!: React.ContextType<typeof Trigger.Context>;


  componentDidMount() {
    //
    const { open } = this.context;

    if (!open) {
      throw new NaraException('Modal', 'Modal is not controlled by trigger. You cannot use Modal.CloseButton.');
    }
  }

  async onClick(event: React.MouseEvent<HTMLButtonElement>) {
    //
    const { onClose } = this.context;
    const { onClick, onClickWithClose } = this.propsWithDefault;

    if (onClickWithClose) {
      onClickWithClose(event, onClose);
    }
    else {
      onClick(event);
      onClose();
    }
  }

  render() {
    //
    const { loading, progressProps, onClickWithClose, children, ...otherProps } = this.props;

    return (
      <ProgressButton
        loading={loading}
        buttonProps={{
          ...otherProps,
          onClick: this.onClick,
        }}
        progressProps={{
          ...progressProps,
        }}
      >
        {children}
      </ProgressButton>
    );
  }
}

export default ModalCloseProgressButtonContainer;
