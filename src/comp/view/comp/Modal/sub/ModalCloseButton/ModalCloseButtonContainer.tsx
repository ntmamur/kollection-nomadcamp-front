import React from 'react';
import { autobind, NaraException } from '@nara.drama/prologue';

import { ButtonProps } from '@material-ui/core';
import { Button, Trigger } from '@nara.drama/material-ui';


interface Props extends ButtonProps {
  //
  onClickWithClose?: (event: React.MouseEvent<HTMLButtonElement>, close: () => void) => void;
  children?: React.ReactNode;
}

@autobind
class ModalCloseButtonContainer extends React.Component<Props> {
  //
  static defaultProps = {
    children: '취소',
    color: 'secondary',
    onClick: () => {
    },
  };

  static contextType = Trigger.Context;
  context!: React.ContextType<typeof Trigger.Context>;


  componentDidMount() {
    //
    const { open } = this.context;

    if (!open) {
      throw new NaraException('Modal', 'Modal is not controlled by trigger. You cannot use Modal.CloseButton.');
    }
  }

  onClick(event: React.MouseEvent<HTMLButtonElement>, close: () => void) {
    //
    if (this.props.onClickWithClose) {
      this.props.onClickWithClose(event, close);
    }
    else {
      this.props.onClick!(event);
      close();
    }
  }

  render() {
    //
    const { onClose } = this.context;
    const { color, children, onClickWithClose, ...otherProps } = this.props;

    return (
      <Button
        {...otherProps}
        color={color}
        onClick={(event: React.MouseEvent<HTMLButtonElement>) => this.onClick(event, onClose)}
      >
        {children}
      </Button>
    );
  }
}

export default ModalCloseButtonContainer;
