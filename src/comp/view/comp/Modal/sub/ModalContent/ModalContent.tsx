import React from 'react';
import autobind from 'autobind-decorator';

import { DialogContentProps } from '@material-ui/core';
import { Dialog } from '@nara.drama/material-ui';


interface Props {
  children: React.ReactNode;
  dividers?: DialogContentProps['dividers'];
  className?: string;
}

@autobind
class ModalContent extends React.Component<Props> {
  //
  static defaultProps = {
    dividers: true,
    className: '',
  };


  render() {
    //
    const { children, dividers, className } = this.props;

    return (
      <Dialog.Content
        dividers={dividers}
        className={className}
      >
        {children}
      </Dialog.Content>
    );
  }
}

export default ModalContent;
