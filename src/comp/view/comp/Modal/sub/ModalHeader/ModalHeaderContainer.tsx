import React from 'react';
import { autobind, ReactComponent } from '@nara.drama/prologue';

import { Trigger, Dialog } from '@nara.drama/material-ui';


interface Props {
  closable?: boolean;
  children?: React.ReactNode;
  close?: () => void;
  className?: string;
  onClose?: () => void;
}

@autobind
class ModalHeaderContainer extends ReactComponent<Props> {
  //
  static defaultProps = {
    closable: true,
    children: null,
    close: () => {
    },
    className: '',
  };

  static contextType = Trigger.Context;
  context!: React.ContextType<typeof Trigger.Context>;


  render() {
    //
    const { children, closable, close: injectedClose, onClose, ...otherProps } = this.props;
    const { onClose: onCloseByTrigger } = this.context;

    return (
      <Dialog.Title
        {...otherProps}
        closable={closable}
        onClose={onClose || onCloseByTrigger}
      >
        {children}
      </Dialog.Title>
    );
  }
}

export default ModalHeaderContainer;
