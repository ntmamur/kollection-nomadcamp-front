import React from 'react';
import { observer } from 'mobx-react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { Avatar, Box, Divider, Grid, Typography } from '@nara.drama/material-ui';
import { TripOrigin } from '@material-ui/icons';


@autobind
@observer
class CourseInfoView extends ReactComponent {
  //
  render() {
    //
    return (
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Box mt={8} display="flex" alignItems="center">
            <TripOrigin color="primary" fontSize="small" />&nbsp;<Typography variant="h4">강사 소개</Typography>
          </Box>
          <Box mb={2} mt={2} width="100%">
            <Divider style={{ backgroundColor: '#666' }} />
          </Box>
        </Grid>
        <Grid container item xs={12} spacing={3}>
          <Grid item xs={1}>
            <Box display="flex" justifyContent="center">
              <Avatar
                style={{ width: '70px', height: '70px' }}
                src={`${process.env.BASE_PATH}/images/course/namoosori_jin.png`}
              />
            </Box>
          </Grid>
          <Grid item xs={11}>
            <Typography bold paragraph variant="h5">진권기 상무</Typography>
            <Grid container item xs={6}>
              <Box width="100px">
                <Typography gutterBottom color="textSecondary" variant="body2">컨텐츠 개발</Typography>
              </Box>
              <Box py={1} px={2} display="flex" height="27px">
                <Divider orientation="vertical" flexItem />
              </Box>
              <Box>
                <Typography display="inline" gutterBottom variant="body2">나무소리 컨텐츠 개발중</Typography>
              </Box>
            </Grid>
            <Grid container item xs={6}>
              <Box width="100px">
                <Typography gutterBottom color="textSecondary" variant="body2">컨설팅</Typography>
              </Box>
              <Box py={1} px={2} display="flex" height="27px">
                <Divider orientation="vertical" flexItem />
              </Box>
              <Box>
                <Typography display="inline" gutterBottom variant="body2">포스코 MES 3.0 MSA 구축</Typography>
              </Box>
            </Grid>
            <Grid container item xs={6}>
              <Box width="100px">
                <Typography gutterBottom color="textSecondary" variant="body2">IT강의</Typography>
              </Box>
              <Box py={1} px={2} display="flex" height="27px">
                <Divider orientation="vertical" flexItem />
              </Box>
              <Box>
                <Typography variant="body2">서울대/명지대/단국대/성균관대/백석대</Typography>
                <Typography gutterBottom variant="body2">한국소프트웨어협회/한국전력/포스코ICT</Typography>
              </Box>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

export default CourseInfoView;
