import React from 'react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { ButtonProps as BaseButtonProps, CircularProgressProps } from '@material-ui/core';
import { Button, Progress } from '@nara.drama/material-ui';
import { WithStyles, withStyles } from './style';


interface Props extends WithStyles {
  loading?: boolean;
  buttonProps?: BaseButtonProps;
  progressProps?: CircularProgressProps;
}

@autobind
class ProgressButtonView extends ReactComponent<Props> {
  //
  render() {
    //
    const { children, loading, buttonProps, progressProps, classes } = this.props;

    return (
      <Button
        {...buttonProps}
        disabled={loading || buttonProps?.disabled}
        className={classes.button}
      >
        {loading ? <Progress.Circular className={classes.progress} {...progressProps} /> : null}
        {children}
      </Button>
      //   {...buttonProps}
      //   onClick={onClick}
      //   disabled={loading}
      //   className={classes.button}
      // >
      //   {loading ? <Progress.Circular className={classes.progress} {...progressProps} /> : null}
      //   {children}
      // </Button>
    );
  }
}

export default withStyles(ProgressButtonView);
