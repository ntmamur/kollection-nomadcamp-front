import React from 'react';
import { autobind, ReactComponent, SortDirection } from '@nara.drama/prologue';
import { observer } from 'mobx-react';
import moment, { Moment } from 'moment';
import { Box, IconButton, Typography } from '@material-ui/core';

import { ChevronLeft, ChevronRight } from '@material-ui/icons';
import { DateUtil } from '@nara.drama/learninglog';


interface Props {
  endDate?: Moment;
  limit?: number;
  dateFormat?: string;
  onChange?: (startDate: Moment, endDate: Moment) => void;
}

@autobind
@observer
class DatePeriodNavigationView extends ReactComponent<Props> {
  //
  static defaultProps = {
    endDate: moment(),
    limit: 5,
    sortDirection: SortDirection.Descending,
    dateFormat: 'YYYY.MM.DD',
    onChange: () => {},
  };

  componentDidMount() {
    //
    const { endDate, onChange } = this.propsWithDefault;

    onChange(this.getStartDate(), endDate);
  }

  getStartDate() {
    //
    const { endDate, limit } = this.propsWithDefault;

    return endDate.clone().subtract(limit - 1, 'days');
  }

  onClickPrev() {
    //
    const { endDate: prevEndDate, limit, onChange } = this.propsWithDefault;
    const startDate = DateUtil.getPrevDate(this.getStartDate(), limit);
    const endDate = DateUtil.getPrevDate(prevEndDate, limit);

    onChange(startDate, endDate);
  }

  onClickNext() {
    //
    const { endDate: prevEndDate, limit, onChange } = this.propsWithDefault;
    const startDate = DateUtil.getNextDate(this.getStartDate(), limit);
    const endDate = DateUtil.getNextDate(prevEndDate, limit);

    onChange(startDate, endDate);
  }

  render() {
    //
    const { endDate, dateFormat } = this.propsWithDefault;
    const startDate = this.getStartDate();

    return (
      <Box display="flex" alignItems="center" justifyContent="center">
        <IconButton onClick={this.onClickPrev}>
          <ChevronLeft />
        </IconButton>
        <Typography variant="h4">
          &nbsp;&nbsp;{startDate.format(dateFormat)} ~ {endDate.format(dateFormat)}&nbsp;&nbsp;
        </Typography>
        <IconButton onClick={this.onClickNext}>
          <ChevronRight />
        </IconButton>
      </Box>
    );
  }
}

export default DatePeriodNavigationView;
