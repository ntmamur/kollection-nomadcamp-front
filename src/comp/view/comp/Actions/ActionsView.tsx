import React, { Children, cloneElement } from 'react';
import { autobind, ReactComponent } from '@nara.drama/prologue';

import { BoxProps, Grid, Box } from '@material-ui/core';


type Props = BoxProps;

@autobind
class ActionsView extends ReactComponent<Props> {
  //
  render() {
    //
    const { children, ...rest } = this.props;

    return (
      <Box my={4} {...rest}>
        <Grid container spacing={2} justify="center">
          {Children.map(children, (child) => (
            <Grid item xs={2}>
              {React.isValidElement(child) && cloneElement(child, {
                fullWidth: true,
                ...child.props,
              })}
            </Grid>
          ))}
        </Grid>
      </Box>
    );
  }
}

export default ActionsView;
