import React, { ContextType } from 'react';
import { ReactComponent, autobind } from '@nara.drama/prologue';
import { AppContext, dialogUtil } from '@nara.drama/prologue-util';
import { ThemeOptions, ThemeProvider } from '../theme';
import DialogView from './view/DialogView';


interface Props {
  children: React.ReactNode;
  defaultTheme?: string;
  themes?: ThemeOptions[];
}

@autobind
class ReactAppConsumerContainer extends ReactComponent<Props> {
  //
  static contextType = AppContext.Context;

  context!: ContextType<typeof AppContext.Context>;

  componentDidMount() {
    //
    const { defaultTheme } = this.props;
    const { value: settings, setValue } = this.context.settings;

    if (defaultTheme) {
      setValue({
        ...settings,
        theme: defaultTheme,
      });
    }
  }

  render() {
    //
    const { children, themes } = this.props;
    const { value: settings } = this.context.settings;

    return (
      <ThemeProvider themes={themes} theme={settings.theme}>
        <dialogUtil.Viewer
          renderDialog={(params) => (<DialogView {...params} />)}
        />

        {children}
      </ThemeProvider>
    );
  }
}

/**
 * ReactApp Provider 컴포넌트, 필수 컴포넌트입니다.
 */
@autobind
class ReactAppContainer extends ReactComponent<Props> {
  //
  render() {
    //
    const { children, ...otherProps } = this.props;

    return (
      <AppContext.Provider>
        <ReactAppConsumerContainer {...otherProps}>
          {children}
        </ReactAppConsumerContainer>
      </AppContext.Provider>
    );
  }
}

export default ReactAppContainer;
