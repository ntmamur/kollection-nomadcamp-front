import React from 'react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { observer } from 'mobx-react';
import { CheckCircle, ErrorRounded, HelpRounded, InfoRounded } from '@material-ui/icons';

import { Box, Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core';
import { dialogTypes } from '@nara.drama/prologue-util';
import { WithStyles, withStyles } from './style';


interface Props extends WithStyles {
  index: number;
  dialog: dialogTypes.Model;
  onClose: (index: number, button: dialogTypes.ButtonModel | null) => void;
}

@autobind
@observer
class DialogContainer extends ReactComponent<Props> {
  //
  renderTitle(dialog: dialogTypes.Model) {
    //
    const title = dialog.title;
    const noticeType = dialog.noticeType;

    return title && (
      <DialogTitle>
        <Box>
          {this.renderTitleIcon(noticeType)}
          {title}
        </Box>
      </DialogTitle>
    );
  }

  renderTitleIcon(noticeType: dialogTypes.NoticeType): React.ReactNode {
    //
    const { classes } = this.props;

    switch (noticeType) {
      case dialogTypes.NoticeType.Check:
        return <CheckCircle className={`${classes.icon} ${classes.question}`}/>;
      case dialogTypes.NoticeType.Info:
        return <InfoRounded className={`${classes.icon} ${classes.info}`}/>;
      case dialogTypes.NoticeType.Danger:
        return <ErrorRounded className={`${classes.icon} ${classes.error}`}/>;
      case dialogTypes.NoticeType.Question:
        return <HelpRounded className={`${classes.icon} ${classes.question}`}/>;
      default:
        return null;
    }
  }

  renderContent(dialog: dialogTypes.Model) {
    //
    const message = dialog.message;

    if (typeof message === 'string') {
      return (
        <DialogContent
          dangerouslySetInnerHTML={{ __html: message }}
        />
      );
    } else {
      return (
        <DialogContent>
          {message}
        </DialogContent>
      );
    }
  }

  renderActions(index: number, dialog: dialogTypes.Model) {
    //
    const { onClose } = this.props;
    const confirmButton = dialog.confirmButton;
    const cancelButton = dialog.cancelButton;

    return (
      <DialogActions>
        {cancelButton && (
          <Button color="default" onClick={() => onClose(index, cancelButton)}>
            {cancelButton.content}
          </Button>
        )}
        <Button color="primary" onClick={() => onClose(index, confirmButton)}>
          {confirmButton.content}
        </Button>
      </DialogActions>
    );
  }

  render() {
    //
    const { index, dialog } = this.props;

    return (
      <Dialog
        key={`dialog-${index}`}
        open
      >
        {this.renderTitle(dialog)}
        {this.renderContent(dialog)}
        {this.renderActions(index, dialog)}
      </Dialog>
    );
  }
}

export default withStyles(DialogContainer);
