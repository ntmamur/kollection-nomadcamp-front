export default {
  htmlFontSize: 18,
  fontSize: 12,
  h1: {
    fontWeight: 500,
    letterSpacing: '-0.24px',
    fontSize: '24px',
  },
  h2: {
    fontWeight: 500,
    letterSpacing: '-0.24px',
    fontSize: '22px',
  },
  h3: {
    fontWeight: 500,
    letterSpacing: '-0.06px',
    fontSize: '20px',
  },
  h4: {
    fontWeight: 500,
    letterSpacing: '-0.06px',
    fontSize: '18px',
  },
  h5: {
    fontWeight: 500,
    letterSpacing: '-0.05px',
    fontSize: '16px',
  },
  h6: {
    fontWeight: 500,
    fontSize: '14px',
    letterSpacing: '-0.05px',
  },
  subtitle1: {
    fontSize: '15px',
  },
  subtitle2: {
    fontSize: '14px',
  },
  body2: {
    fontSize: '12px',
  },
  caption: {
    fontSize: '12px',
  },
  overline: {
    fontWeight: 500,
    fontSize: '12px',
  },
};
