import _ from 'lodash';
import { createTheme, responsiveFontSizes } from '@material-ui/core/styles';

import { Theme, ThemeConfig, ThemeOptions, ThemeType } from './Types';

// Admin theme spacing
// import base from './baseDense';
// import themes from './AdminThemes';
// import typography from './typographyDense';

// User theme spacing
import base from './base';
import typography from './typography';


const baseOptions: ThemeOptions = {
  ...base,
  typography,
};

export const createThemeCustom = (themes: ThemeOptions[], config: ThemeConfig = {}): Theme => {
  let themeOptions = themes.find((theme) => theme.name === config.theme);

  if (!themeOptions) {
    console.warn(new Error(`The theme ${config.theme} is not valid`));
    [themeOptions] = themes;
  }

  let theme = createTheme(
    _.merge(
      {},
      baseOptions,
      themeOptions,
      { direction: config.direction },
    )
  );

  if (config.responsiveFontSizes) {
    theme = responsiveFontSizes(theme);
  }

  return theme as Theme;
};

export { ThemeType };
