import type {
  Palette as MuiPalette,
  TypeBackground as MuiTypeBackground,
} from '@material-ui/core/styles/createPalette';

import { ThemeOptions as MuiThemeOptions } from '@material-ui/core/styles';
import type { Shadows as MuiShadows } from '@material-ui/core/styles/shadows';
import { Theme as MuiTheme } from '@material-ui/core/styles/createTheme';


type Direction = 'ltr' | 'rtl';

export enum ThemeType {
  Light = 'Light',
  OneDark = 'OneDark',
  Unicorn = 'Unicorn',
}


interface TypeBackground extends MuiTypeBackground {
  dark: string;
}

interface Palette extends MuiPalette {
  background: TypeBackground;
}

export interface Theme extends MuiTheme {
  name: string;
  palette: Palette;
}

export interface ThemeConfig {
  direction?: Direction;
  responsiveFontSizes?: boolean;
  theme?: string;
}

export interface ThemeOptions extends MuiThemeOptions {
  name?: string;
  direction?: Direction;
  typography?: Record<string, any>;
  palette?: Record<string, any>;
  shadows?: MuiShadows;
  shape?: Record<string, any>;
}
