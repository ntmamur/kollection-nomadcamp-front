import * as colors from '@material-ui/core/colors';

import { ThemeOptions, ThemeType } from './Types';
import { softShadows, strongShadows } from './shadows';


const themes: ThemeOptions[] = [
  {
    name: ThemeType.Light,
    overrides: {
      MuiButton: {
        containedSecondary: {
          color: '#443DF6',
          backgroundColor: '#E9E8FB',
          '&:hover': {
            backgroundColor: 'rgb(183 181 253)',
          },
        },
        outlinedSecondary: {
          border: '1px solid #C8C6FA',
          color: '#443DF6',
          backgroundColor: 'rgba(68, 61, 246, 0.1)',
        },
      },
      MuiBadge: {
        colorSecondary: {
          color: '#443DF6',
          backgroundColor: '#E9E8FB',
        },
      },
      MuiInputBase: {
        input: {
          '&::placeholder': {
            opacity: 1,
            color: colors.blueGrey[600],
          },
        },
      },
    },
    palette: {
      common: { black: '#000', white: '#fff' },
      type: 'light',
      action: {
        active: colors.blueGrey[600],
      },
      // background: {
      //   // default: colors.common.white,
      //   default: '#F2F3FD',
      //   dark: '#f4f7fd',
      //   paper: colors.common.white,
      // },

      background: {
        // default: colors.common.white,
        default: '#F2F3FD',
        dark: '#fff',
        paper: colors.common.white,
      },

      primary: {
        main: '#443DF6',
      },
      secondary: {
        main: '#7D78F7',
        contrastText: '#fff',
      },
      text: {
        primary: colors.blueGrey[900],
        secondary: colors.blueGrey[400],
      },
      red: {
        main: '#f44336',
      },
      pink: {
        main: '#e91e63',
      },
      cyan: {
        main: '#00bcd4',
      },
      deepPurple: {
        main: '#673ab7',
      },
    },
    shadows: softShadows,
    shape: { borderRadius: 4 },
  },

  {
    name: ThemeType.OneDark,
    overrides: {},
    palette: {
      type: 'dark',
      action: {
        active: 'rgba(255, 255, 255, 0.54)',
        hover: 'rgba(255, 255, 255, 0.04)',
        selected: 'rgba(255, 255, 255, 0.08)',
        disabled: 'rgba(255, 255, 255, 0.26)',
        disabledBackground: 'rgba(255, 255, 255, 0.12)',
        focus: 'rgba(255, 255, 255, 0.12)',
      },
      background: {
        default: '#282C34',
        dark: '#1c2025',
        paper: '#282C34',
      },
      primary: {
        main: '#443DF6',
      },
      secondary: {
        main: '#7D78F7',
        // contrastText: '#fff',
      },
      text: {
        primary: '#e6e5e8',
        secondary: '#adb0bb',
      },
      red: {
        main: '#f44336',
      },
      pink: {
        main: '#e91e63',
      },
      cyan: {
        main: '#00bcd4',
      },
      deepPurple: {
        main: '#673ab7',
      },
    },
    shadows: strongShadows,
    shape: { borderRadius: 4 },
  },

  {
    name: ThemeType.Unicorn,
    overrides: {},
    palette: {
      type: 'dark',
      action: {
        active: 'rgba(255, 255, 255, 0.54)',
        hover: 'rgba(255, 255, 255, 0.04)',
        selected: 'rgba(255, 255, 255, 0.08)',
        disabled: 'rgba(255, 255, 255, 0.26)',
        disabledBackground: 'rgba(255, 255, 255, 0.12)',
        focus: 'rgba(255, 255, 255, 0.12)',
      },
      background: {
        default: '#2a2d3d',
        dark: '#222431',
        paper: '#2a2d3d',
      },
      primary: {
        main: '#443DF6',
      },
      secondary: {
        main: '#7D78F7',
      },
      text: {
        primary: '#f6f5f8',
        secondary: '#9699a4',
      },
    },
    shadows: strongShadows,
    shape: { borderRadius: 4 },
  },
];

export default themes;
