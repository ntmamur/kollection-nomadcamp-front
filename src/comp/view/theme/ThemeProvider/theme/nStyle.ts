
import { createStyles, Theme, WithStyles as MuiWithStyles, withStyles as muiWithStyles } from '@material-ui/core/styles';
import { deepOrange, deepPurple, green, pink } from '@material-ui/core/colors';


const style = (theme: Theme) => createStyles({
  /* 해상도 최소사이즈 */
  nw_1280: {
    minWidth: '1280px',
  },
  nw2_1280: {
    minWidth: '800px',
    left: '0px',
  },
  /* nodata S */
  nno_wrap: {
    width: '100%',
    height: '450px',
    marginTop: '30px',
    position: 'relative',
    borderTop: '1px solid #ddd',
    borderBottom: '1px solid #ddd',
  },
  nnot_con: {
    position: 'relative',
    top: '45%',
    margin: 'auto',
    fontSize: '16px',
    color: '#666',
    lineHeight: '37px',
  },
  nnotSmall: {
    position: 'relative',
    margin: 'auto',
    fontSize: '16px',
    color: '#666',
    lineHeight: '37px',
  },
  /* nodata E */
  /* 과제 S */
  background: {
    backgroundColor: '#fff',
    color: 'red',
  },
  container_spacing: {
    marginBottom: '40px;',
  },
  list_cont_spacing: {
    marginTop: theme.spacing(5),
    marginBottom: theme.spacing(5),
    color: '#263238',
  },
  assignment_mainimg: {
    width: '211px',
    height: '147px',
    background: 'url("/training/images/icon/assignment-icon.png")no-repeat center top',
  },
  task_tit_spacing: {
    marginTop: '25px',
  },
  card_w: {
    width: '258px',
  },
  ico_coach: {
    width: '22px',
    height: '18px',
    background: 'url("/training/images/task/ico_person.png")no-repeat left top',
    marginTop: '2px',
    marginRight: '-5px',
    display: 'inline-block',
  },
  font_g14: {
    fontSize: '0.95rem',
    color: '#546E7A',
  },
  card_t_img_w: {
    width: '100%',
  },
  ncardsum_wrap: {
    height: '128px',
    border: '1px solid #ececec',
  },
  ncardsum2_wrap: {
    width: '350px',
    height: '129px',
    border: '1px solid #ececec',
  },
  card2_t_img_w: {
    width: '100%',
  },
  card_bottom_1: {
    display: 'inline-block',
    color: '#979797',
    fontSize: '0.95rem',
    marginLeft: '9px',
  },
  card_b_ico_ongoing: {
    width: '17px',
    height: '17px',
    background: 'url("/training/images/task/ico_schedule.png")no-repeat left top',
    display: 'inline-block',
    verticalAlign: 'middle',
    marginTop: '-3px',
    marginRight: '5px',
  },
  card_b_ico_complet: {
    width: '17px',
    height: '17px',
    background: 'url("/training/images/task/ico_complet.png")no-repeat left top',
    display: 'inline-block',
    verticalAlign: 'middle',
    marginTop: '-1px',
    marginRight: '5px',
  },
  card_b_ico_waite: {
    width: '17px',
    height: '17px',
    background: 'url("/training/images/task/ico_waite.png")no-repeat left top',
    display: 'inline-block',
    verticalAlign: 'middle',
    marginTop: '-4px',
    marginRight: '5px',
  },
  card_b_layout: {
    justifyContent: 'space-between',
  },
  task_ongoing: {
    color: '#443DF6',
  },
  task_complet: {
    color: '#8097A2',
  },
  txt_waite: {
    color: '#F44336',
  },
  task_loc_top: {
    marginTop: '50px;',
  },
  task_dtxt_ul: {
    marginTop: '15px',
    '& li': {
      display: 'inline-block',
      marginRight: '60px',
      color: '#546E7A',
      '& p': {
        fontSize: '0.95rem',
        display: 'inline-block',
      },
    },
  },
  ico_coach_spacing: {
    display: 'inline-block',
  },
  ico_coach_detail: {
    width: '22px',
    height: '18px',
    background: 'url("/training/images/task/ico_person.png")no-repeat left top',
    marginBottom: '-4px',
    marginRight: '3px',
    display: 'inline-block',
  },
  ico_coach_time: {
    width: '22px',
    height: '18px',
    background: 'url("/training/images/task/ico_task_time.png")no-repeat left 2px',
    marginBottom: '-4px',
    marginRight: '0px',
    display: 'inline-block',
  },
  task_detail_conspacing: {
    margin: '30px 0 30px 0',
    float: 'left',
    width: '100%',
    '& h3': {
      fontSize: '1.3rem',
      marginBottom: '10px',
    },
  },
  task_detail_paper: {
    padding: '25px 30px 25px 30px',
    task_detail_paper: 'padding:10px',
    float: 'left',
    width: '100%',
  },
  task_detail_ul: {
    '& li': {
      listStyle: 'none',
      width: '50%',
      display: 'inline-block',
      fontSize: '1rem',
      padding: '5px 10px 5px 25px',
      verticalAlign: 'top',
      background: 'url("/training/images/task/ico_done.png")no-repeat left 5px',
    },
  },
  task_detail_view: {
    marginBottom: '40px',
    display: 'flex',
    flexDirection: 'column',
  },
  heading: {
    marginLeft: '18px',
    lineHeight: '40px',
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '14.4%',
    color: theme.palette.text.secondary,
    flexShrink: 0,
    display: 'flex',
    alignItems: 'center',
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
  },
  task_detail_mtxt: {

  },
  task_detail_mtxt_spacing: {
    padding: '10px 30px 30px 30px',
    flexDirection: 'column',

  },
  ico_task_copy: {
    background: 'url("/training/images/task/ico_task_copy.png")no-repeat center 11px',
    width: '44x',
    minWidth: '44px',
    height: '44px',
    backgroundColor: '#E9E8FB',
    padding: '0px',
  },
  ico_task_download: {
    background: 'url("/training/images/task/ico_task_download.png")no-repeat center 11px',
    width: '44x',
    minWidth: '44px',
    height: '44px',
    backgroundColor: '#E9E8FB',
    padding: '0px',
  },
  ico_task_view: {
    background: 'url("/training/images/task/ico_task_view.png")no-repeat center 11px',
    width: '44x',
    minWidth: '44px',
    height: '44px',
    backgroundColor: '#E9E8FB',
    padding: '0px',
  },
  task_detail_tit_txt: {
    fontSize: '1rem',
    color: '#263238',
    verticalAlign: 'middle',
  },
  task_detail_tit: {
    width: '70px',
  },
  task_detail_content: {
    width: '750px',
    '& button': {
      marginRight: '15px',
    },
  },
  nstyle_input_copy: {
    width: '600px',
    marginRight: '15px',
    '& input': {
      fontSize: '1rem',
      color: '#78909C',
    },
  },
  task_detail_box: {
    marginTop: '10px',
    marginBottom: '10px',
  },
  task_detail_link_download: {
    width: '600px',
    display: 'block',
    border: '1px solid #CBCBCB',
    borderRadius: '3px',
    height: '43px',
    float: 'left',
    marginRight: '15px',
  },
  task_detail_linkbox: {
    height: '41px',
    padding: '8px 8px 8px 9px',
  },
  task_detail_ico_file: {
    background: 'url("/training/images/task/ico_task_file.png")no-repeat center 14px',
    width: '20x',
    minWidth: '20px',
    height: '44px',
    padding: '0px',
    float: 'left',
    marginLeft: '10px',
    marginRight: '10px',
  },
  task_detail_urlcopy: {
    width: '670px',
    marginRight: '15px',
    '& input': {
      fontSize: '1rem',
      color: '#78909C',
    },
  },
  btn_base_t14: {
    width: '44x',
    height: '44px',
    backgroundColor: '#443DF6',
    padding: '0px',
    color: '#fff',
    fontSize: '1rem',
    marginLeft: theme.spacing(1),
  },
  task_detail_taskcopy: {
    margin: '18px 0 18px 0',
  },
  task_detail_tablebox: {
    margin: '18px 0 18px 0',
  },
  task_detail_table: {
    width: '100%',
  },
  task_detail_table_tinfo1: {
    color: '#443DF6',
    fontSize: '1rem',
  },
  task_detail_table_tinfo: {
    display: 'flex',
    justifyContent: 'space-between',
    width: '100%',
    margin: '10px 0 10px 0',
  },
  ico_task_list: {
    background: 'url("/training/images/task/ico_task_list.png")no-repeat center 2px',
    width: '20px',
    minWidth: '20px',
    height: '20px',
    padding: '0px',
    marginRight: '5px',
    float: 'left',
  },
  task_detail_table_tinfo2: {
    display: 'flex',
    alignItemsL: 'center',

  },
  table_trlink: {
    '& tr': {
      cursor: 'pointer',
    },
  },
  ncolor_rpoint: {
    color: '#F44336',
  },
  /* 과제 E */

  /* 포탈 S */
  layout_w100: {
    width: '100%',
    maxWidth: '100%',
    height: '100%',
    marginTop: '60px',
    padding: '0px',
  },
  layout_pl: {
    width: '320px',
    height: '100%',
    borderRight: '1px solid #E0E0E0',
    float: 'left',
  },
  ico_trainee_portal1: {
    background: 'url("/training/images/icon/ico_trainee_p1.png")no-repeat left 2px',
    width: '28px',
    minWidth: '28px',
    height: '28px',
    padding: '0px',
    marginTop: '-3px',
    marginRight: '10px',
  },
  ico_trainee_portal2: {
    background: 'url("/training/images/icon/ico_trainee_p2.png")no-repeat 3px 0px',
    width: '28px',
    minWidth: '28px',
    height: '28px',
    padding: '0px',
    marginTop: '-3px',
    marginRight: '10px',
  },
  ico_trainee_portal3: {
    background: 'url("/training/images/icon/ico_trainee_p3.png")no-repeat 1px top',
    width: '28px',
    minWidth: '28px',
    height: '28px',
    padding: '0px',
    marginTop: '-3px',
    marginRight: '10px',
  },
  box_conter: {
    backgroundColor: '#443DF6',
    height: '20px',
    width: '40px',
    fontSize: '1rem',
    color: '#fff',
    lineHeight: '23px',
    textAlign: 'center',
    padding: '0 5px 0 5px',
    borderRadius: '20px',
  },
  roundpaper_list: {
    borderRadius: '12px',
    border: '1px solid #E0E0E0',
    padding: '5px 0px 5px 0px',
    margin: '38px 20px 0 20px',
    backgroundColor: '#fff',
    cursor: 'pointer',
  },
  roundpaper_list_align: {
    display: 'flex',
    justifyContent: 'space-between',
    borderBottom: '1px solid #E0E0E0',
    padding: '10px 20px 10px 20px',
    '&:last-child': {
      borderBottom: '0px',
    },
  },
  roundpaper_list_talign: {
    display: 'flex',
    alignItems: 'center',
  },
  trainee_pl_spacing: {
    marginTop: '45px',
  },
  trainee_pl_tit: {
    fontSize: '1.3rem',
    marginLeft: '20px',
    color: '#263238',
  },
  nbtn_more: {
    width: '30px',
    height: '30px',
    border: '0px',
    backgroundColor: '#fff',
    background: 'url("/training/images/icon/nico_more.png")no-repeat center center',
    float: 'right',
    margin: '-30px 10px 0 0',
    cursor: 'pointer',
  },
  trainee_pl_listbox: {
    height: '235px',
    cursor: 'pointer',
    '& li': {
      borderBottom: '1px solid #E0E0E0',
      listStyle: '0px',
      padding: '15px 20px 15px 20px',
    },
    '& last-child': {
      borderBottom: '0px',
    },
  },
  trainee_pl_sm: {
    fontSize: '0.95rem',
    color: '#78909C',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    width: '100%',
  },
  trainee_pl_la: {
    fontSize: '1.05rem',
    color: '#263238',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    width: '100%',
  },
  trinee_pl_smr: {
    fontSize: '0.95rem',
    color: '#263238',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    width: '100%',
  },
  trainee_pl_ps: {
    display: 'flex',
    justifyContent: 'space-between',
    textAlign: 'right',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
  },
  trainee_pl_sm2: {
    fontSize: '0.95rem',
    color: '#78909C',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    width: '350px',
  },
  trainee_pl_listbox2: {
    height: '235px',
    '& li': {
      borderBottom: '1px solid #E0E0E0',
      listStyle: '0px',
      padding: '15px 20px 15px 20px',
    },
    '& last-child': {
      borderBottom: '0px',
    },
  },
  nmodal_pl_git: {
    width: 613,
    backgroundColor: theme.palette.background.paper,
    borderRadius: '4px',
    boxShadow: theme.shadows[5],
  },
  nmodal_pl_box: {
    display: 'flex',
    padding: theme.spacing(1),
    alignItems: 'center',
    justifyContent: 'center',
  },
  nmodal_pl_git_con: {
    padding: theme.spacing(4),
  },
  task_pl_input: {
    width: '400px',
  },
  nmodal_tit: {
    borderBottom: '1px solid #CECECE',
    fontSize: '1.1rem',
    fontWeight: 'bold',
    padding: theme.spacing(3),
    color: '#202021',
  },
  /* 포탈 E */
  /* nlogin S */
  nl_ob: {
    width: '1280px',
    margin: 'auto',
  },
  nl_wrap: {
    minHeight: '98vh',
    minWidth: '1280px',
  },
  nl_lb: {
    width: '440px',
    marginLeft: '0px',
  },
  nl_rb: {
    width: '820px',
  },
  nl_logo: {
    background: 'url("/training/images/logo/nlogo_namoosori.png")no-repeat left center',
    width: '210px',
    height: '104px',
    marginBottom: '5px',
  },
  nl_con: {
    display: 'flex',
    flexDirection: 'column',
    minWidth: '100%',
  },
  nl_h2: {
    fontSize: '20px',
    color: '#222',
    lineHeight: '26px',
    fontWeight: 'normal',
  },
  nl_dot: {
    width: '44px',
    height: '16px',
    background: 'url("/training/images/main/nl_dot.png")no-repeat center 10px',
    display: 'inline-block',
  },
  nl_input: {
    backgroundColor: '#F6F7FA',
  },
  ntxt_g14: {
    color: '#888',
    fontSize: '1rem',
  },
  nl_btn: {
    padding: '12px 0 12px 0',
    fontSize: '16px',
    fontWeight: 'bold',
    marginTop: '20px',
  },
  nl_sp1: {
    margin: '40px 0 40px 0',
  },
  nl_line: {
    width: '15px',
    height: '1px',
    backgroundColor: '#aaa',
  },
  nl_footer: {
    fontSize: '12px',
    color: '#666',
    lineHeight: '20px',
  },
  nl_footer_line: {
    display: 'inline-block',
    margin: '-2px 6px 0 6px',
    width: '1px',
    height: '11px',
    backgroundColor: '#C6C6C6',
    verticalAlign: 'middle',
  },
  nl_img: {
    marginBottom: '-80px',
    marginRight: '10px',
    textAlign: 'left',
    background: 'url("/training/images/main/namoosori_login.jpg")no-repeat -60px center',
    width: '820px',
    height: '554px',
    backgroundSize: '859px 554px',
  },
  nl_img2: {
    marginBottom: '-80px',
    marginRight: '10px',
    textAlign: 'left',
    background: 'url("/training/images/main/namoosori_login2.jpg")no-repeat -50px center',
    width: '820px',
    height: '554px',
    backgroundSize: '859px 554px',
  },
  /* nlogin E */
  /* 과제 메인 S */
  ntr_wrap: {
    marginTop: '100px',
    marginBottom: '40px',
    minHeight: '820px',
    border: '1px solid #dcdcdc',
    borderRadius: '28px',
    margin: 'auto',
  },
  ntr_swrap: {
    margin: 'auto',
  },
  ntr_lbox: {
    borderRight: '1px solid #dcdcdc',
    padding: '27px 45px 0px 45px',
  },
  ntr_rbox: {
    padding: '45px 45px 0px 45px',
  },
  ntr_welcome: {
    fontSize: '1.5rem',
    color: '#000',
  },
  ntr_mimg: {
    background: 'url("/training/images/task/ntr_mimg.png")no-repeat center center',
    height: '245px',
    width: '100%',
  },
  ntr_tit_wrap: {
    borderBottom: '1px solid #000',
    paddingBottom: '10px',
  },
  ntr_tit: {
    fontSize: '1.4rem',
    color: '#000',
  },
  ntr_link: {
    textDecoration: 'none',
  },
  ntr_list_g: {
    color: '#999',
    lineHeight: '40px',
    display: '-webkit-box',
    WebkitBoxOrient: 'vertical',
    WebkitLineClamp: 1,
    whiteSpace: 'normal',
    wordBreak: 'break-all',
  },
  ntr_bar_stxt: {
    margin: 'auto',
    marginTop: '-32px',
    display: 'block',
    color: '#222',
    fontSize: '1rem',
  },
  ntr_list_g2: {
    color: '#222',
    display: '-webkit-box',
    WebkitBoxOrient: 'vertical',
    WebkitLineClamp: 2,
    whiteSpace: 'normal',
    wordBreak: 'break-all',
  },
  tasktit: {
    fontSize: '1.1rem',
    fontWeight: 'bold',
  },
  ntr_taskwrap: {
    width: '310px',
  },
  ntr_list_b: {
    lineHeight: '40px',
    display: '-webkit-box',
    WebkitBoxOrient: 'vertical',
    WebkitLineClamp: 1,
    whiteSpace: 'normal',
    wordBreak: 'break-all',
  },
  ntr_more: {
    minWidth: 'auto',
    cursor: 'pointer',
  },
  ntr_count: {
    backgroundColor: '#F1F1FC',
    textAlign: 'center',
    lineHeight: '71px',
    borderRadius: '54px',
    width: '54px',
    height: '54px',
    float: 'right',
  },
  ntr_count1: {
    fontSize: '1rem',
    lineHeight: '20px',
    color: '#666',
  },
  ntr_count2: {
    fontSize: '1.5rem',
    lineHeight: '30px',
    color: '#000',
    fontWeight: 'bold',
  },
  ntr_info01: {
    color: '#443DF6',
    verticalAlign: 'middle',
    marginTop: '-3px',
    marginRight: '5px',
  },
  ntr_info02: {
    color: '#ED5F65',
    verticalAlign: 'middle',
    marginTop: '-3px',
    marginLeft: '20px',
    marginRight: '5px',
  },
  ntr_barwrap1: {
    backgroundColor: '#F4F4F4',
    margin: 'auto',
    width: '50%',
    position: 'relative',
    height: '180px',
  },
  ntr_bar1: {
    paddingTop: '5px',
    backgroundColor: '#443DF6',
    textAlign: 'center',
    color: '#fff',
    position: 'absolute',
    bottom: '0px',
    width: '100%',
  },
  ntr_barwrap2: {
    backgroundColor: '#EBEBEB',
    margin: 'auto',
    width: '50%',
    position: 'relative',
    height: '180px',
  },
  ntr_bar2: {
    paddingTop: '5px',
    backgroundColor: '#ED5F65',
    textAlign: 'center',
    color: '#fff',
    position: 'absolute',
    bottom: '0px',
    width: '100%',
  },
  ntr_bartit: {
    borderTop: '1px solid #DCDCDC',
    paddingTop: '10px',
    textAlign: 'center',
  },
  ntr_mtable_wrap: {
    width: '100%',
    minHeight: '200px',
    '& table': {
      width: '100%',
      minHeight: '100%',
      border: '0px',
      tableLayout: 'fixed',
    },
    '& th': {
      fontSize: '1rem',
    },
    '& th:first-child': {
      color: '#DC3434',
    },
    '& th:last-child': {
      color: '#3346F4',
    },
    '& td': {
      borderRight: '1px solid #DBDBDB',
      padding: '0px 10px 0px 10px',
      minHeight: '100%',
      verticalAlign: 'top',
    },
    '& tr:first-child td': {
      paddingTop: '12px',
    },
    '& td:last-child': {
      borderRight: '0px',
    },
    '& p': {
      fontSize: '0.9rem',
    },
  },
  ntr_case_c: {
    marginTop: '-2px',
    padding: '0 8px 0 6px',
    marginRight: '3px',
    height: '22px',
    borderRadius: '22px',
    backgroundColor: '#E57373',
    textAlign: 'center',
    color: '#fff',
    '& p': {
      fontSize: '0.9rem',
      lineHeight: '24px',
    },
  },
  ntr_case_r: {
    marginTop: '-2px',
    padding: '0 9px 0 7px',
    marginRight: '3px',
    height: '22px',
    borderRadius: '22px',
    backgroundColor: '#5754AC',
    textAlign: 'center',
    color: '#fff',
    '& p': {
      fontSize: '0.9rem',
      lineHeight: '24px',
    },
  },
  ntr_case_w: {
    marginTop: '-2px',
    padding: '0 9px 0 5px',
    marginRight: '3px',
    height: '22px',
    borderRadius: '22px',
    backgroundColor: '#66BB6A',
    textAlign: 'center',
    color: '#fff',
    '& p': {
      fontSize: '0.9rem',
      lineHeight: '24px',
    },
  },
  ntr_case_loc: {
    marginTop: '-2px',
    padding: '0 12px 0 2px',
    marginRight: '3px',
    height: '22px',
    borderRadius: '22px',
    backgroundColor: '#FF8E31',
    textAlign: 'center',
    color: '#fff',
    '& p': {
      fontSize: '0.7rem',
      lineHeight: '24px',
      fontWeight: 'bold',
    },
  },
  /* 과제 메인 E */
  /* 서브 상단 이미지 S */
  nsum_btn: {
    position: 'absolute',
    right: '0px',
    top: '50px',
  },
  ntr_sline: {
    borderBottom: '1px solid #dcdcdc',
  },
  nmax_wrap: {
    maxWidth: '1568px',
  },
  nsum_wrap: {
    position: 'relative',
  },
  ntr_simg01: {
    width: '211px',
    height: '147px',
    background: 'url("/namoosori/images/icon/ntr_simg01.png")no-repeat center top',
  },
  ntr_simg02: {
    width: '211px',
    height: '157px',
    marginTop: '-10px',
    background: 'url("/namoosori/images/icon/ntr_simg02.png")no-repeat center Top',
  },
  ntr_simg03: {
    width: '211px',
    height: '147px',
    background: 'url("/namoosori/images/icon/ntr_simg03.png")no-repeat center 8px',
  },
  ntr_simg04: {
    width: '211px',
    height: '147px',
    background: 'url("/namoosori/images/icon/ntr_simg04.png")no-repeat center top',
  },
  ntr_simg05: {
    width: '211px',
    height: '157px',
    marginTop: '-10px',
    background: 'url("/namoosori/images/icon/ntr_simg05.png")no-repeat center top',
  },
  ntr_simg06: {
    width: '211px',
    height: '157px',
    marginTop: '-10px',
    background: 'url("/namoosori/images/icon/ntr_simg06.png")no-repeat center top',
  },
  ntr_simg07: {
    width: '211px',
    height: '147px',
    background: 'url("/namoosori/images/icon/ntr_simg07.png")no-repeat center top',
  },
  ntr_simg08: {
    width: '211px',
    height: '147px',
    background: 'url("/namoosori/images/icon/ntr_simg08.png")no-repeat center top',
  },
  ntr_simg09: {
    width: '211px',
    height: '147px',
    background: 'url("/namoosori/images/icon/ntr_simg09.png")no-repeat center top',
  },
  /* 서브 상단 이미지 E */
  /* review S */
  reviewWrap: {
    margin: '0px 0 30px 0',
    float: 'left',
    width: '100%',
    '& h3': {
      fontSize: '1.3rem',
      marginBottom: '10px',
    },
  },
  reviewCard: {
    boxShadow: 'none',
  },
  orange: {
    color: theme.palette.getContrastText(deepOrange[500]),
    backgroundColor: deepOrange[500],
  },
  purple: {
    color: theme.palette.getContrastText(deepPurple[500]),
    backgroundColor: deepPurple[500],
  },
  pink: {
    color: theme.palette.getContrastText(pink[500]),
    backgroundColor: pink[500],
  },
  green: {
    color: '#fff',
    backgroundColor: green[500],
  },
  nBold: {
    fontWeight: 'bold',
  },
  nGray: {
    color: '#999',
  },
  ncommentWrap: {
    paddingTop: '0',
    paddingBottom: '0',
  },
  ntHrspc: {
    margin: '25px 0 25px 0',
  },
  /* review E */
  /* 러닝로그 QnA S */
  nmodalQna: {
    width: 1220,
    backgroundColor: theme.palette.background.paper,
    borderRadius: '4px',
    boxShadow: theme.shadows[5],
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  writtenIcon: {
    color: '#ED5F65',
    margin: '0 5px 0 7px',
  },
  queWrap: {
    borderRight: '1px solid #ddd',
    paddingTop: '1px',
  },
  ncommentQnawrap: {
    paddingTop: '0',
    paddingBottom: '0',
    height: '486px',
  },
  /* 러닝로그 QnA E */
  ntabLine: {
    borderBottom: '1px solid #ddd',
  },
  topbarWrap: {
    marginTop: '30px',
    marginBottom: '20px',
  },
  tableTopline: {
    borderTop: '2px solid #443DF6',
  },
});

export default style;
export type NWithStyles = MuiWithStyles<typeof style>;
export const nWithStyles = muiWithStyles(style);
