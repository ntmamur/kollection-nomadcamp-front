// @ts-nocheck
// admin theme(spacing dense)

import { ThemeOptions } from './Types';


const baseSpacing = 3;

function spacing(spacingSize: number, adjustSize: number = 0): number {
  return baseSpacing * spacingSize + adjustSize;
}

function paddingXSpacing(spacingSize: number, adjustSize: number = 0) {
  return {
    paddingLeft: spacing(spacingSize, adjustSize),
    paddingRight: spacing(spacingSize, adjustSize),
  };
}

function paddingYSpacing(spacingSize: number, adjustSize: number = 0) {
  return {
    paddingTop: spacing(spacingSize, adjustSize),
    paddingBottom: spacing(spacingSize, adjustSize),
  };
}

function marginXSpacing(spacingSize: number, adjustSize: number = 0) {
  return {
    marginLeft: spacing(spacingSize, adjustSize),
    marginRight: spacing(spacingSize, adjustSize),
  };
}

// function marginYSpacing(spacingSize: number, adjustSize: number = 0) {
//   return {
//     marginTop: spacing(spacingSize, adjustSize),
//     marginBottom: spacing(spacingSize, adjustSize),
//   };
// }


const baseOptions: ThemeOptions = {
  direction: 'ltr',
  spacing: baseSpacing,
};

baseOptions.overrides = {

  MuiAvatar: {
    root: {
      width: '24px',
      height: '24px',
    },
  },

  MuiBadge: {
    root: {},
    badge: {
      minWidth: '16px',
      height: '16px',
    },
  },

  MuiButton: {
    root: {
      borderRadius: '3px',
      minWidth: '45px',
      padding: `${spacing(1, 2.5)}px ${spacing(3)}px`,
      lineHeight: '1.65',
    },

    containedSizeSmall: {
      padding: `${spacing(1, 0.5)}px ${spacing(2)}px`,
    },
    contained: {},
    containedSizeLarge: {
      padding: `${spacing(2, 2.5)}px ${spacing(5)}px`,
      fontSize: '13px',
    },

    outlinedSizeSmall: {
      padding: `${spacing(0, 2.5)}px ${spacing(2)}px`,
    },
    outlined: {
      padding: `${spacing(1, 1.5)}px ${spacing(3)}px`,
    },
    outlinedSizeLarge: {
      padding: `${spacing(2, 1.5)}px ${spacing(5)}px`,
      fontSize: '13px',
    },

    textSizeSmall: {
      padding: `${spacing(1, 0.5)}px ${spacing(2)}px`,
    },
    text: {
      padding: `${spacing(1, 2.5)}px ${spacing(3)}px`,
    },
    textSizeLarge: {
      padding: `${spacing(2, 2.5)}px ${spacing(5)}px`,
      fontSize: '13px',
    },

    startIcon: {
      marginLeft: `${spacing(0)}px`,
      marginRight: `${spacing(1)}px`,
    },

    endIcon: {
      marginLeft: `${spacing(1)}px`,
      marginRight: `${spacing(0)}px`,
    },

    iconSizeSmall: {
      '&.MuiButton-startIcon': {
        marginLeft: `${spacing(0)}px`,
      },
      '& > .MuiSvgIcon-root': {
        fontSize: '16px',
      },
    },
    iconSizeMedium: {
      '& > .MuiSvgIcon-root': {
        fontSize: '17px',
      },
    },
    iconSizeLarge: {
      '& > .MuiSvgIcon-root': {
        fontSize: '21px',
      },
    },
  },

  MuiCheckbox: {
    root: {
      padding: `${spacing(1)}px`,
    },
  },

  MuiFormControlLabel: {
    root: {
      marginLeft: `${spacing(0)}px`,
    },
  },

  MuiChip: {
    root: {
      height: '23px',

      '& .MuiAvatar-root': {
        width: spacing(5),
        height: spacing(5),
        marginLeft: spacing(1),
        marginRight: spacing(-1),
        lineHeight: '1.3',
      },
    },
    label: {
      paddingRight: spacing(2, 2),
      paddingLeft: spacing(2, 2),
    },
    sizeSmall: {
      height: '21px',
    },
    deleteIcon: {
      width: spacing(5),
      height: spacing(5),
      marginLeft: spacing(-1),
      marginRight: spacing(2),
    },
  },

  MuiListItem: {
    root: {
      paddingTop: spacing(1),
      paddingBottom: spacing(1),
    },
  },
  MuiListItemText: {
    root: {
      marginTop: spacing(1),
      marginBottom: spacing(1),
    },
  },

  MuiSvgIcon: {
    root: {
      fontSize: '17px',

      '&.MuiSelect-icon': {
        top: 'calc(50% - 9px)',
        right: '3px',
      },
    },
    fontSizeSmall: {
      fontSize: '15px',
    },
    fontSizeLarge: {
      fontSize: '21px',
    },
  },

  MuiIconButton: {
    root: { padding: spacing(1, 4) },
    sizeSmall: {
      padding: '2px',
    },
    SizeMedium: {
      fontSize: '12px',

      '& > .MuiSvgIcon-root': {
        fontSize: '12px',
      },
    },
  },

  MuiTextField: {
    root: {
      '& > label + .MuiInput-formControl.MuiInput-underline': {
        marginTop: spacing(2),
      },
    },
  },

  MuiInputBase: {
    input: {
      paddingTop: spacing(2, 0.77),
      paddingBottom: spacing(2, 0.77),
    },
    inputMarginDense: {
      paddingTop: spacing(0, 2.27),
    },
  },

  MuiOutlinedInput: {
    root: {
      borderRadius: '3px',
    },
    input: {
      padding: `${spacing(2, 0.77)}px ${spacing(2, 2)}px`,
    },
    inputMarginDense: {
      paddingTop: spacing(1, 1.27),
      paddingBottom: spacing(1, 1.27),
    },

    multiline: {
      padding: `${spacing(1, 2.27)}px ${spacing(2)}px`,
    },
  },

  MuiFilledInput: {
    input: {
      paddingRight: spacing(2),
      paddingLeft: spacing(2),
      paddingTop: spacing(3, 1.54),
      paddingBottom: spacing(1, 2),
    },
    inputMarginDense: {
      paddingTop: spacing(2, 2.54),
      paddingBottom: spacing(1, 2),
    },
  },
  '& .MuiSelect-iconOutlined': {
    right: spacing(1),
  },

  MuiInputLabel: {
    root: {
      '&.MuiInputLabel-shrink': {
        transform: `translate(${spacing(0)}px, ${spacing(1, -2)}px) scale(0.75)`,
      },
    },

    formControl: {
      transform: `translate(${spacing(0)}px, ${spacing(4)}px) scale(1)`,
    },

    marginDense: {
      transform: `translate(0, ${spacing(4)}px) scale(1)`,
    },

    outlined: {
      transform: `translate(${spacing(2)}px, ${spacing(3)}px) scale(1)`,
      '&.MuiInputLabel-marginDense': {
        transform: `translate(${spacing(2)}px, ${spacing(2)}px) scale(1)`,
      },
      '&.MuiInputLabel-shrink': {
        transform: `translate(${spacing(5)}px, ${spacing(0, -6)}px) scale(0.75)`,
      },
    },

    filled: {
      paddingLeft: spacing(2),
      paddingRight: spacing(2),
      paddingTop: spacing(3),
      paddingBottom: spacing(2),
      transform: `translate(${spacing(2)}px, ${spacing(1)}px) scale(1)`,
      '&.MuiInputLabel-marginDense': {
        transform: `translate(${spacing(1)}px, ${spacing(0)}px) scale(1)`,
      },
      '&.MuiInputLabel-shrink': {
        transform: `translate(${spacing(1)}px, ${spacing(-1)}px) scale(0.75)`,
        '&.MuiInputLabel-marginDense': {
          transform: `translate(${spacing(1)}px, ${spacing(-1, -1)}px) scale(0.75) !important`,
        },
      },
    },
  },

  MuiSelect: {
    root: {

      '&.MuiSelect-outlined': {
        paddingRight: '42px !important',
      },
    },
  },

  MuiFormHelperText: {
    root: {
      marginTop: `${spacing(1, -1)}px`,
    },
    contained: {

      marginRight: `${spacing(2)}px`,
      marginLeft: `${spacing(2)}px`,
    },
  },

  MuiFormControl: {
    root: {},
    label: {
      transform: 'translate(8px, 8px) scale(1)',
    },
  },

  MuiLink: {
    root: {
      fontSize: '12px',
    },
  },

  MuiRadio: {
    root: {
      padding: `${spacing(1)}px`,
    },
  },

  MuiSwitch: {
    root: {
      width: '42px',
      height: '23px',
      padding: '5px',
    },
    switchBase: {
      padding: `${spacing(1, -1)}px`,
      '&.Mui-checked': {
        transform: `translateX(${spacing(6)}px)`,
      },
    },
    thumb: {
      width: '18px',
      height: '18px',
    },

    sizeSmall: {
      width: '35px',
      height: '21px',
      padding: `${spacing(2)}px`,

      '& .MuiSwitch-thumb': {
        width: '13px',
        height: '13px',
      },
    },
  },

  MuiToolbar: {
    regular: {
      '@media (min-width:600px)': { minHeight: 42 },
    },
    //   dense: {
    //     '@media (min-width:600px)': { minHeight: 46 },
    //   },
    //   gutters: {
    //     '@media (min-width:600px)': { ...paddingXSpacing(2, 2) }, // 18 -> 14
    //   },
  },

  MuiAccordion: {
    root: {
      '&.Mui-expanded': {
        margin: `${spacing(4)}px ${spacing(0)}px`,
      },
    },
  },

  MuiAccordionSummary: {
    root: {
      minHeight: '30px',

      '& .MuiIconButton-edgeEnd': {
        marginRight: `${spacing(0, -6)}px`,
      },

      '&.Mui-expanded': {
        minHeight: '30px',
      },
    },
    content: {
      margin: `${spacing(2)}px`,

      '&.Mui-expanded': {
        margin: `${spacing(2)}px ${spacing(2)}px`,
      },
    },
  },

  MuiAccordionDetails: {
    root: {
      padding: `${spacing(4)}px`,
    },
  },


  MuiCard: {
    root: {
      '& .MuiCardHeader-root': {
        padding: `${spacing(3)}px`,
      },

      '& .MuiCardContent-root': {
        padding: `${spacing(3)}px`,
      },

      '& .MuiCardActions-root': {
        padding: `${spacing(3)}px`,


        '& .MuiIconButton-root': {
          padding: `${spacing(2, -2)}px`,
        },
      },
    },
  },

  MuiTabs: {
    root: {
      minHeight: '30px',

      '& .MuiTab-root': {
        minHeight: '30px',
      },
    },
  },

  MuiDialog: {
    root: {
      '& .MuiDialogTitle-root': {
        padding: `${spacing(2)}px ${spacing(3)}px`,
      },
      '& .MuiDialogContent-root': {
        padding: `${spacing(2)}px ${spacing(3)}px`,
      },
      '& .MuiDialogActions-root': {
        padding: `${spacing(2)}px`,
      },
    },
  },

  MuiList: {
    root: {
      '& .MuiMenuItem-root': {
        paddingTop: `${spacing(1, -1)}px`,
        paddingBottom: `${spacing(1, -1)}px`,
      },
      '& .MuiListItemIcon-root': {
        minWidth: '21px',
      },
      '& .MuiListItemAvatar-root': {
        minWidth: '36px',
      },
      '& .MuiListItemText-root': {
        marginTop: '0',
        marginBottom: '0',
      },
      '& .MuiListItem-gutters': {
        paddingLeft: '9px',
        paddingRight: '9px',
      },
    },
    padding: {
      paddingTop: `${spacing(1)}px`,
      paddingBottom: `${spacing(1)}px`,
    },
  },

  MuiTableCell: {
    root: {
      height: '42px',
      padding: `${spacing(2)}px`,
    },

    sizeSmall: {
      height: '30px',
      padding: `${spacing(1)}px ${spacing(2)}px`,
    },
  },

  MuiTablePagination: {
    toolbar: {
      minHeight: '32px',
    },
  },

  MuiAutocomplete: {
    root: {
      '& .MuiAutocomplete-inputRoot[class*="MuiOutlinedInput-root"]': {
        padding: `${spacing(2, 0.77)}px ${spacing(2)}px`,

        '& .MuiAutocomplete-input': {
          padding: `${spacing(0)}px ${spacing(1)}px`,

          '&:first-child': {
            paddingLeft: `${spacing(0)}px`,
          },
        },
      },
      '& .MuiAutocomplete-inputRoot[class*="MuiOutlinedInput-root"][class*="MuiOutlinedInput-marginDense"]': {
        padding: `${spacing(1, 1.27)}px ${spacing(2)}px`,

        '& .MuiAutocomplete-input': {
          padding: `${spacing(0)}px ${spacing(1)}px`,
        },
      },
    },
    option: {
      paddingLeft: `${spacing(2)}px`,
      paddingRight: `${spacing(2)}px`,
      paddingTop: `${spacing(1, 1.27)}px`,
      paddingBottom: `${spacing(1, 1.27)}px`,
    },
    endAdornment: {
      top: `calc(50% - 12px)`,
    },
  },

  MuiPaginationItem: {
    root: {
      minWidth: '21px',
      height: '21px',
    },
    sizeSmall: {
      minWidth: '18px',
      height: '18px',
    },
    sizeLarge: {
      minWidth: '26px',
      height: '26px',
    },
  },

  MuiToggleButton: {
    root: {
      padding: `${spacing(1, 1)}px`,
      '& .MuiSvgIcon-root': {
        fontSize: '18px',
      },
    },
    sizeSmall: {
      padding: `${spacing(1)}px`,
      '& .MuiSvgIcon-root': {
        fontSize: '15px',
      },
    },
    sizeLarge: {
      padding: `${spacing(2)}px`,
      '& .MuiSvgIcon-root': {
        fontSize: '24px',
      },
    },
  },

  MuiDialogContent: {
    root: {
      '&:first-child': {
        paddingTop: `${spacing(3)}px`,
      },
    },
  },

  MuiTreeItem: {
    label: {
      paddingTop: `${spacing(1)}px`,
      paddingBottom: `${spacing(1)}px`,
    },
  },
};

export default baseOptions;
