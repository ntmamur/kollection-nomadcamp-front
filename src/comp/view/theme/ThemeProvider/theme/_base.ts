import { ThemeOptions } from './Types';


const baseSpacing = 6;

function spacing(spacingSize: number, adjustSize: number = 0): number {
  return baseSpacing * spacingSize + adjustSize;
}

function paddingXSpacing(spacingSize: number, adjustSize: number = 0) {
  return {
    paddingLeft: spacing(spacingSize, adjustSize),
    paddingRight: spacing(spacingSize, adjustSize),
  };
}

function paddingYSpacing(spacingSize: number, adjustSize: number = 0) {
  return {
    paddingTop: spacing(spacingSize, adjustSize),
    paddingBottom: spacing(spacingSize, adjustSize),
  };
}

function marginXSpacing(spacingSize: number, adjustSize: number = 0) {
  return {
    marginLeft: spacing(spacingSize, adjustSize),
    marginRight: spacing(spacingSize, adjustSize),
  };
}

// function marginYSpacing(spacingSize: number, adjustSize: number = 0) {
//   return {
//     marginTop: spacing(spacingSize, adjustSize),
//     marginBottom: spacing(spacingSize, adjustSize),
//   };
// }


const baseOptions: ThemeOptions = {
  direction: 'ltr',
  spacing: baseSpacing,
};

baseOptions.overrides = {
  MuiButton: {
    containedSizeSmall: { padding: '2px 6px' },   // 4 10 -> 2 6
    contained: { padding: '4px 10px' },           // 6 16 -> 4 10
    containedSizeLarge: { padding: '6px 16px' },  // 8 22 -> 6 16

    outlinedSizeSmall: { padding: '1px 5px' },    // 3 9 -> 1 5
    outlined: { padding: '3px 9px' },             // 5 15 -> 3 9
    outlinedSizeLarge: { padding: '5px 15px' },   // 7 21 -> 5 15

    textSizeSmall: { padding: '2px 3px' },  // 4 5 -> 2 3
    text: { padding: '4px 5px' },           // 6 8 -> 4 5
    textSizeLarge: { padding: '6px 8px' },  // 8 11 -> 6 8

    startIcon: { marginRight: spacing(1) }, // 8 -> 6
    endIcon: { marginLeft: spacing(1) },    // 8 -> 6
  },
  MuiIconButton: {
    root: { padding: spacing(1, 4) }, // 12 -> 10
    sizeSmall: { padding: '2px' },  // 3 -> 2
  },
  MuiTextField: {
    root: {
      /* Standard(underline) Input label */
      '& > label + .MuiInput-formControl.MuiInput-underline': {
        marginTop: spacing(2),  // 16 -> 12
      },
    },
  },
  MuiInputBase: {
    input: {
      paddingTop: spacing(1, -2),     // 6 -> 4
      paddingBottom: spacing(1, -1),  // 7 -> 5
    },
    inputMarginDense: {
      paddingTop: '2px',  // 3 -> 2
    },
  },
  MuiOutlinedInput: {
    input: {
      ...paddingYSpacing(3, -5.5),  // 18.5 -> 12.5
      ...paddingXSpacing(2, -2),    // 14 -> 10
    },
    inputMarginDense: {
      ...paddingYSpacing(0.5, 6.5), // 10.5 -> 8.5
    },
  },
  MuiFilledInput: {
    input: {
      padding: undefined,
      ...paddingXSpacing(2, -4),      // 12 -> 8
      paddingTop: spacing(4, -5),     // 27 -> 19
      paddingBottom: spacing(2, -6),  // 10 -> 6
    },
    inputMarginDense: {
      paddingTop: spacing(4, -9),     // 23 -> 15
      paddingBottom: spacing(1, -2),  // 6 -> 4
    },
  },
  MuiInputLabel: {
    formControl: {
      transform: `translate(0, ${spacing(3)}px) scale(1)`,  // 24 -> 18
    },
    marginDense: {
      transform: `translate(0, ${spacing(3, -3)}px) scale(1)`,  // 21 -> 15
    },
    outlined: {
      /* translate(14px, 20px) => translate(10px, 16px) */
      transform: `translate(${spacing(2, -2)}px, ${spacing(2, 4)}px) scale(1)`,
      '&.MuiInputLabel-marginDense': {
        /* translate(14px, 12px) => translate(10px, 12px) */
        transform: `translate(${spacing(2, -2)}px, 12px) scale(1)`,
      },
      '&.MuiInputLabel-shrink': {
        /* translate(14px, -6px) => translate(10px, -6px) */
        transform: `translate(${spacing(2, -2)}px, -6px) scale(0.75)`,
      },
    },
    filled: {
      /* translate(12px, 20px) -> translate(8px, 16px) */
      transform: `translate(${spacing(2, -4)}px, ${spacing(2, 4)}px) scale(1)`,
      '&.MuiInputLabel-marginDense': {
        /* translate(12px, 17px) -> translate(8px, 11px) */
        transform: `translate(${spacing(2, -4)}px, ${spacing(3, -7)}px) scale(1)`,
      },
      '&.MuiInputLabel-shrink': {
        /* translate(12px, 10px) -> translate(8px, 6px) */
        transform: `translate(${spacing(2, -4)}px, ${spacing(2, -6)}px) scale(0.75)`,
      },
      '&.MuiInputLabel-shrink&.MuiInputLabel-marginDense': {
        /* translate(12px, 7px) -> translate(8px, 5px) */
        transform: `translate(${spacing(2, -4)}px, ${spacing(1, -1)}px) scale(0.75)`,
      },
    },
  },
  MuiFormControl: {
    marginNormal: {
      marginTop: spacing(2),    // 16 -> 12
      marginBottom: spacing(1), // 8 -> 6
    },
    marginDense: {
      marginTop: spacing(1),      // 8 -> 6
      marginBottom: spacing(0.5), // 4 -> 3
    },
  },
  MuiFormHelperText: {
    contained: {
      ...marginXSpacing(2, -2),  // 14 -> 10
    },
  },
  MuiListItem: {
    root: { ...paddingYSpacing(1) },    // 8 -> 6
    dense: { ...paddingYSpacing(0.5) }, // 4 -> 3
    gutters: { ...paddingXSpacing(2) }, // 18 -> 14
  },
  MuiListItemIcon: {
    root: {
      minWidth: spacing(7), // 56 -> 42
    },
  },
  MuiTableCell: {
    root: {
      padding: spacing(2),  // 16 -> 12
    },
    sizeSmall: {
      padding: undefined,
      ...paddingYSpacing(1, -2),    // 6 -> 4
      paddingRight: spacing(3), // 24 -> 18
      paddingLeft: spacing(2),  // 16 -> 12
    },
  },
  MuiToolbar: {
    /* 버튼이 차지하는 높이때문에 동적값이 의미없어 minHeight 적당한 값으로 하드코딩 */
    regular: {
      '@media (min-width:600px)': { minHeight: 60 },
    },
    dense: {
      '@media (min-width:600px)': { minHeight: 46 },
    },
    gutters: {
      '@media (min-width:600px)': { ...paddingXSpacing(2, 2) }, // 18 -> 14
    },
  },
  MuiChip: {
    root: {
      backgroundColor: 'rgba(0,0,0,0.075)',
    },
  },
  MuiCard: {
    root: {
      borderRadius: 6,
    },
  },
  MuiLinearProgress: {
    root: {
      borderRadius: 3,
      overflow: 'hidden',
    },
  },
};

export default baseOptions;
