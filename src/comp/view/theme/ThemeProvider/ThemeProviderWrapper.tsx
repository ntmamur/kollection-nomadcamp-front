import React from 'react';
import { ThemeProvider as MuiThemeProvider, ThemeProviderProps } from '@material-ui/styles';

import { Helmet } from 'react-helmet';
import { createThemeCustom, ThemeType } from './theme';
import GlobalStyles from './GlobalStyles';
import { ThemeOptions } from './theme/Types';
import defaultThemes from './theme/themes';


type Props = ThemeProviderProps<ThemeType | string> & {
  themes?: ThemeOptions[];
}

class ThemeProviderWrapper extends React.Component<Props> {
  //
  static defaultProps = {
    themes: defaultThemes,
  };

  render() {
    //
    const { children, themes: themesProp, theme: themeProp } = this.props;
    const themes = themesProp || [];

    const theme = createThemeCustom(themes || [], {
      direction: 'ltr',
      responsiveFontSizes: true,
      theme: themeProp ? themeProp.toString() : themes[0].name,
    });

    return (
      <MuiThemeProvider theme={theme}>
        <Helmet>
          <link
            rel="stylesheet"
            href="https://fonts.googleapis.com/css?family=Roboto+Mono|Roboto+Slab|Roboto:300,400,500,700"
          />
        </Helmet>

        <GlobalStyles />
        {children}
      </MuiThemeProvider>
    );
  }
}

export default ThemeProviderWrapper;
