import * as PageTabTypes from './type';
import PageTabContainer from './PageTabContainer';
import PageTabItem from './sub/PageTabItem';


type PageTabComponent = typeof PageTabContainer & {
  Item: typeof PageTabItem;
};

const PageTab = PageTabContainer as PageTabComponent;

PageTab.Item = PageTabItem;

export default PageTab;
export { PageTabTypes };
