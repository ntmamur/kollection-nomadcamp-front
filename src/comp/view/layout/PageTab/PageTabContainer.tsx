import React from 'react';
import { autobind, ReactComponent } from '@nara.drama/prologue';

import { Box, Tab, Tabs } from '@material-ui/core';
import { SmartTabs, SmartTabsTypes } from '@nara.drama/prologue-util';
import TabParams from './model/TabParams';


interface Props {
  children: React.ReactNode;
  defaultActiveValue?: string;
  activeValue?: string;
  onChange?: (event: React.ChangeEvent, params: TabParams) => void;
}

@autobind
class PageTabContainer extends ReactComponent<Props> {
  //
  static defaultProps = {
    defaultActiveName: undefined,
    activeName: undefined,
    onChange: () => {
    },
  };

  renderTab(params: SmartTabsTypes.RenderTabParams) {
    //
    return (
      <Tabs {...params} />
    );
  }

  renderTabItem(params: SmartTabsTypes.RenderTabItemParams) {
    //
    return (
      <Tab {...params} />
    );
  }

  render() {
    //
    const { defaultActiveValue, activeValue, onChange, children } = this.props;

    return (
      <SmartTabs
        defaultActiveValue={defaultActiveValue}
        activeValue={activeValue}
        onChange={onChange}
        renderTab={this.renderTab}
        renderTabItem={this.renderTabItem}
      >
        <Box>
          {children}
        </Box>
      </SmartTabs>
    );
  }
}

export default PageTabContainer;
