import React from 'react';
import { ReactComponent, autobind } from '@nara.drama/prologue';

import { SmartTabs } from '@nara.drama/prologue-util';


interface Props {
  /** 탭 메뉴 value */
  value: string;
  /** 탭 메뉴 텍스트 */
  label: string;
  /** 탭 컨텐츠 컴포넌트 */
  component?: any;
}

@autobind
class PageTabItemContainer extends ReactComponent<Props> {
  //
  static defaultProps = {
    component: null,
  };

  render() {
    //
    const { value, label, component, children } = this.props;

    return (
      <SmartTabs.Item
        value={value}
        label={label}
        component={component}
      >
        {children}
      </SmartTabs.Item>
    );
  }
}

export default PageTabItemContainer;
