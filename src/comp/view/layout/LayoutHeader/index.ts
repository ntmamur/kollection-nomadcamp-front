import LayoutHeaderContainer from './LayoutHeaderContainer';
import Logo from './sub/Logo';
import Menu from './sub/Menu';
import IconButton from './sub/IconButton';
import RightMenu from './sub/RightMenu';
import ProfileIconButton from './sub/ProfileIconButton';


type LayoutHeaderComponent = typeof LayoutHeaderContainer & {
  Logo: typeof Logo;
  Menu: typeof Menu;
  RightMenu: typeof RightMenu;
  IconButton: typeof IconButton;
  ProfileIconButton: typeof ProfileIconButton;
};

const LayoutHeader = LayoutHeaderContainer as LayoutHeaderComponent;

LayoutHeader.Logo = Logo;
LayoutHeader.Menu = Menu;
LayoutHeader.RightMenu = RightMenu;
LayoutHeader.IconButton = IconButton;
LayoutHeader.ProfileIconButton = ProfileIconButton;

export default LayoutHeader;

