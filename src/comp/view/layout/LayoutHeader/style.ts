import {
  createStyles,
  Theme,
  WithStyles as MuiWithStyles,
  withStyles as muiWithStyles,
} from '@material-ui/core/styles';


const style = (theme: Theme) => createStyles({
  root: {
    zIndex: theme.zIndex.drawer + 1,
    height: 60,
    '& > *': {
      justifyContent: 'space-between',
    },
  },
});

export default style;
export type WithStyles = MuiWithStyles<typeof style>;
export const withStyles = muiWithStyles(style);
