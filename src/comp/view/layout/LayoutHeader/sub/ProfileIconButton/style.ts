import {
  createStyles,
  Theme,
  WithStyles as MuiWithStyles,
  withStyles as muiWithStyles,
} from '@material-ui/core/styles';


const style = (theme: Theme) => createStyles({
  iconButton: {
    display: 'flex',
    alignItems: 'center',
    '& > *': {
      cursor: 'pointer',
    },
  },
});

export default style;
export type WithStyles = MuiWithStyles<typeof style>;
export const withStyles = muiWithStyles(style);
