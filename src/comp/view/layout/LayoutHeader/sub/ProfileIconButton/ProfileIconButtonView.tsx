import React from 'react';
import { autobind, ReactComponent } from '@nara.drama/prologue';

import { Avatar, Box, Typography } from '@material-ui/core';
import { WithStyles, withStyles } from './style';


interface Props extends WithStyles {
  name?: string;
  subName?: string;
  photoPath?: string;
  className?: string;
  onClick?: (e: React.MouseEvent) => void;
}

@autobind
class ProfileIconButtonView extends ReactComponent<Props> {
  //
  static defaultProps = {
    name: '',
    subName: '',
    photoPath: `${process.env.BASE_PATH}/images/all/img-profile-56-px.png`,
    className: '',
    onClick: () => {
    },
  };


  render() {
    //
    const { classes, name, photoPath, onClick } = this.props;

    return (
      <Box className={classes.iconButton}>
        <Box m={1}>
          <Avatar
            src={photoPath}
            onClick={onClick}
          />
        </Box>
        {name && (
          <Box mr={1}>
            <Typography
              variant="h6"
              color="inherit"
            >
              {name}
            </Typography>
          </Box>
        )}
      </Box>
    );
  }
}

export default withStyles(ProfileIconButtonView);
