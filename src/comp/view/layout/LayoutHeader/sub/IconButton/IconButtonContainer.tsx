import React from 'react';
import { ReactComponent } from '@nara.drama/prologue';

import clsx from 'clsx';
import { IconButton as MuiIconButton } from '@material-ui/core';
import { WithStyles, withStyles } from './style';


interface Props extends WithStyles {
  children: React.ReactNode;
  className?: string;
  onClick?: (event: React.MouseEvent<HTMLElement>) => void;
}

class IconButton extends ReactComponent<Props> {
  //
  static defaultProps = {
    className: '',
    onClick: () => {
    },
  };


  render() {
    //
    const { classes, children } = this.props;
    const { className, onClick } = this.propsWithDefault;

    return (
      <MuiIconButton className={clsx(classes.icon, className)} onClick={onClick}>
        {children}
      </MuiIconButton>
    );
  }
}

export default withStyles(IconButton);
