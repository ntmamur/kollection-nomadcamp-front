import {
  createStyles,
  Theme,
  WithStyles as MuiWithStyles,
  withStyles as muiWithStyles,
} from '@material-ui/core/styles';


const style = (theme: Theme) => createStyles({
  root: {
    display: 'flex',
    width: '140x', // width: theme.palette.sidebarWidth,
    height: '100%',
    alignItems: 'center',
    justifyContent: 'left',
  },
});

export default style;
export type WithStyles = MuiWithStyles<typeof style>;
export const withStyles = muiWithStyles(style);
