import React from 'react';
import { ReactComponent, autobind } from '@nara.drama/prologue';
import clsx from 'clsx';

import { Box } from '@material-ui/core';
import { WithStyles, withStyles } from './style';


interface Props extends WithStyles {
  className?: string;
}

@autobind
class MenuContainer extends ReactComponent<Props> {
  //
  static defaultProps = {
    className: '',
  };


  render() {
    //
    const { classes, className, children } = this.props;

    return (
      <Box className={clsx(classes.root, className)}>
        {children}
      </Box>
    );
  }
}

export default withStyles(MenuContainer);
