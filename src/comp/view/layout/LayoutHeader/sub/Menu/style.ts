import {
  createStyles,
  Theme,
  WithStyles as MuiWithStyles,
  withStyles as muiWithStyles,
} from '@material-ui/core/styles';


const style = (theme: Theme) => createStyles({
  root: {
    height: '100%',
    flexGrow: 1,
    alignItems: 'center',
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    overflow: 'hidden',
    flex: '1 auto',
    flexWrap: 'wrap',
    display: 'flex',
    '& > *': {
      marginRight: theme.spacing(2),
      height: '100%',
      display: 'flex',
      alignItems: 'center',
    },
  },
});

export default style;
export type WithStyles = MuiWithStyles<typeof style>;
export const withStyles = muiWithStyles(style);
