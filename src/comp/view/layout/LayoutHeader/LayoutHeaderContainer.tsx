import React from 'react';
import { ReactComponent, autobind } from '@nara.drama/prologue';

import HeaderWrapperView from './view/HeaderWrapperView';


interface Props {
  children: React.ReactNode;
  className?: string;
}

@autobind
class LayoutHeaderContainer extends ReactComponent<Props> {
  //
  static defaultProps = {
    className: '',
  };

  render() {
    //
    const { className, children } = this.props;

    return (
      <HeaderWrapperView
        className={className}
      >
        {children}
      </HeaderWrapperView>
    );
  }
}

export default LayoutHeaderContainer;
