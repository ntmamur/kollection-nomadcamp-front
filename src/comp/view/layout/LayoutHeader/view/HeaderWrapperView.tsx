import React from 'react';
import { ReactComponent } from '@nara.drama/prologue';
import clsx from 'clsx';

import { AppBar, Toolbar } from '@material-ui/core';
import { WithStyles, withStyles } from '../style';


interface Props extends WithStyles {
  children: React.ReactNode;
  className?: string;
}

class HeaderWrapperView extends ReactComponent<Props> {
  //
  static defaultProps = {
    className: '',
  };


  render() {
    //
    const { classes, className, children } = this.props;

    return (
      <AppBar position="fixed" className={clsx(classes.root, className)}>
        <Toolbar disableGutters>
          {children}
        </Toolbar>
      </AppBar>
    );
  }
}

export default withStyles(HeaderWrapperView);
