import { ReactComponent } from '@nara.drama/prologue';
import {
  Button, Box, Container, Grid, Toolbar, Typography, Divider, Link,
} from '@nara.drama/material-ui';
import React from 'react';
import { PageLayout } from '~/comp/view/layout';
import { WithStyles, withStyles } from './style';


interface Props extends WithStyles {
  isLogin: boolean;
  onClickLogin: () => void;
  onClickLogout: () => void;
  onClickClass: () => void;
}


class MainPageContainer extends ReactComponent<Props> {
  //
  render() {
    //
    const { classes, isLogin, onClickLogout, onClickLogin, onClickClass } = this.props;

    return (
      <Box className={classes.white}>
        <Toolbar className={classes.toolbar}>
          <Box pl={3}>
            <img src={`${process.env.BASE_PATH}/images/logo/namoosori_logo.png`} className={classes.logoImage} alt="" />
          </Box>
          <Box width="100%" justifyContent="flex-end" pr={3} display="flex">
            {isLogin ?
              <>
                {/*fixme:*/}
                <Box mr={3}>
                  <Button variant="outlined" onClick={onClickClass}>내 강좌</Button>
                </Box>
                <Button variant="text" onClick={onClickLogout}>LOGOUT</Button>
              </>
              :
              <Button variant="text" onClick={onClickLogin}>LOGIN</Button>
            }
          </Box>
        </Toolbar>

        <Box className={classes.visualImage}>
          <img src={`${process.env.BASE_PATH}/images/main/main-bg.png`} className={classes.imgContent} alt="" />
        </Box>

        <Container>
          <PageLayout>
            <PageLayout.Body>
              <Box mb={8}>
                <Grid container spacing={10}>
                  <Grid item xs={12} sm={6}>
                    <Box>
                      <img
                        src={`${process.env.BASE_PATH}/images/main/namoosori_01.png`}
                        className={classes.imgContent}
                        alt=""
                      />
                    </Box>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Box>
                      <Typography paragraph variant="h1">개발자 코칭프로그램</Typography>
                      <Typography variant="body1">
                        멋진 소프트웨어 개발자의 길을 가고 싶은 분들을 위한 과정을 특별히 기획하였습니다. <br />
                        현실에서 학습자들이 만나는 교육은 내일 당장 취업하여 회사가 원하는 일을 하는 역량에 맞춰져 있기 때문입니다. <br />
                        안타깝게도 대학 교육조차도 기반을 다루기 보다는 취업에 필요한 현실 교육으로 초점을 옮기고 있습니다. 비즈니스 공간에서 발생한 문제를 해결함으로써
                        모두에게 만족감을 선사하는 그런 엔지니어의 삶을 누릴 수 있기를 소망합니다. 그 소망의 시작이 나무소리 코칭 프로그램이 되길 바랍니다.
                      </Typography>
                    </Box>
                    <Box width="100%" textAlign="left" pt={3}>
                      <Button variant="text">
                        <Link href="https://www.nextree.io/namoosori/">VIEW DETAIL</Link>
                      </Button>
                    </Box>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <Box>
                      <Typography paragraph align="right" variant="h1">개발팀 역량 강화 프로그램</Typography>
                      <Typography variant="body1" align="right">
                        넥스트리는 개발자에 의한 개발자 교육을 표방하며 KOSTA의 가산 교육장에서 5년간 개발자 교육을 진행하였습니다. <br />
                        5년간 1만 7천명의 재직자를 대상으로 대략 서른 과목을 준비하여 강의를 진행했습니다. <br />
                        그 이후 집합 교육 부분은 진행하지 않고, 프로젝트를 중심으로 MSA, DDD, 등과 같은 전문 교육을 프로젝트 중심으로 진행하였습니다.<br />
                        프로젝트라는 특별한 환경에서 활동하였으므로, 교육에 그치지 않고, 코칭, 컨설팅 영역을 아우르는 서비스를 제공하였으며, 관련 실적은 다음과 같습니다.
                      </Typography>
                    </Box>
                    <Box width="100%" textAlign="right" pt={3}>
                      <Button variant="text">
                        <Link href="https://www.nextree.io/namoosori-business/">VIEW DETAIL</Link>
                      </Button>
                    </Box>
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <img
                      src={`${process.env.BASE_PATH}/images/main/namoosori_02.png`}
                      className={classes.imgContent}
                      alt=""
                    />
                  </Grid>
                </Grid>
              </Box>
            </PageLayout.Body>
          </PageLayout>
        </Container>
        <Box my={3}>
          <Divider />
        </Box>
        <Box pb={3}>
          <Typography align="center" color="textSecondary">
            넥스트리(주) 대표이사 송태국 | 주소 : 08503 서울 금천구 가산디지털1로 171 가산SK V1센터 1806호 | TEL : 02 6332 5250 | 사업자등록번호 : 119-86-91148
          </Typography>
          <Typography align="center" color="textSecondary">
            Copyright© 2021 namoosori. All rights reserved.
          </Typography>
        </Box>
      </Box>
    );
  }
}


export default withStyles(MainPageContainer);

