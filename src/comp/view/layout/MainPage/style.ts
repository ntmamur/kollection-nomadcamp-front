
import { createStyles, Theme, WithStyles as MuiWithStyles, withStyles as muiWithStyles } from '@material-ui/core/styles';


const style = (theme: Theme) => createStyles({
  white: {
    backgroundColor: '#F0F8FF',
    width: '100%',
  },
  toolbar: {
    background: '#fff',
  },
  logoImage: {
    height: '32px',
  },
  visualImage: {
    width: '100%',
    maxHeight: '640px',
    overflow: 'hidden',
  },
  imgContent: {
    width: '100%',
  },
  buttonColor: {
    borderColor: '#f0f',
  },
});

export default style;
export type WithStyles = MuiWithStyles<typeof style>;
export const withStyles = muiWithStyles(style);
