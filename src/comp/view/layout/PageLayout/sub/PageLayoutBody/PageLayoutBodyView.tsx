import React from 'react';
import { ReactComponent } from '@nara.drama/prologue';
import { Box, Container } from '@material-ui/core';
import { nWithStyles, NWithStyles } from '../../../../theme';


interface Props extends NWithStyles {
  children: React.ReactNode;
}

class PageLayoutBodyView extends ReactComponent<Props> {
  //
  render() {
    //
    const { children, classes } = this.props;

    return (
      <Container className={classes.nmax_wrap}>
        <Box mb={10}>
          {children}
        </Box>
      </Container>
    );
  }
}


export default nWithStyles(PageLayoutBodyView);
