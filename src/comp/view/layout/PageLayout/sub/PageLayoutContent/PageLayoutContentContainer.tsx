import React from 'react';
import { ReactComponent } from '@nara.drama/prologue';
import { Box } from '@material-ui/core';


interface Props {
  children: React.ReactNode;
}

class PageLayoutContentContainer extends ReactComponent<Props> {
  //
  render() {
    //
    const { children } = this.props;

    return (
      <Box>
        {children}
      </Box>
    );
  }
}

export default PageLayoutContentContainer;
