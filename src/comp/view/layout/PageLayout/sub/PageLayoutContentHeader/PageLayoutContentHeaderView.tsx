import React from 'react';
import { ReactComponent } from '@nara.drama/prologue';
import { Box } from '@material-ui/core';


interface Props {
  children: React.ReactNode;
}

class PageLayoutContentHeaderView extends ReactComponent<Props> {
  //
  render() {
    //
    const { children } = this.props;

    return (
      <Box mb={5}>
        {children}
      </Box>
    );
  }
}

export default PageLayoutContentHeaderView;
