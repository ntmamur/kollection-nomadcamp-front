import React from 'react';
import { autobind, ReactComponent } from '@nara.drama/prologue';

import { Toolbar } from '@material-ui/core';
import { AppContext, NavBreadcrumbModel } from '@nara.drama/prologue-util';


interface Props {
  children: React.ReactNode;
  breadcrumb?: NavBreadcrumbModel[];
  standAlone?: boolean;
}

@autobind
class PageLayoutContainer extends ReactComponent<Props> {
  //
  static defaultProps = {
    breadCrumb: [],
    standAlone: false,
  };

  static contextType = AppContext.Context;

  context!: React.ContextType<typeof AppContext.Context>;


  componentDidMount() {
    //
    this.setBreadcrumb(this.props.breadcrumb!);
  }

  componentDidUpdate(prevProps: Props) {
    //
    if (prevProps.breadcrumb !== this.props.breadcrumb) {
      this.setBreadcrumb(this.props.breadcrumb!);
    }
  }

  setBreadcrumb(breadcrumbs: NavBreadcrumbModel[]) {
    //
    const { breadcrumb: breadcrumbContext } = this.context;

    breadcrumbContext.setValue(breadcrumbs);
  }

  render() {
    //
    const { standAlone, children } = this.props;

    return (
      <>
        {!standAlone && (
          <Toolbar />
        )}

        {children}
      </>
    );
  }
}

export default PageLayoutContainer;
