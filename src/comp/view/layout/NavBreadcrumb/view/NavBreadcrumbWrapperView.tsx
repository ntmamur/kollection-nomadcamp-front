import React from 'react';
import { ReactComponent } from '@nara.drama/prologue';


interface Props {
  children: React.ReactNode;
}

class NavBreadcrumbWrapperView extends ReactComponent<Props> {
  //
  render() {
    //
    const { children } = this.props;

    return (
      <div className="breadcrumbs">
        <div className="cont-inner">
          {children}
        </div>
      </div>
    );
  }
}

export default NavBreadcrumbWrapperView;
