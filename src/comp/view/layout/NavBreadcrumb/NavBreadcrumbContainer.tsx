import React from 'react';
import { autobind, ReactComponent } from '@nara.drama/prologue';

import { Breadcrumbs, Link } from '@material-ui/core';
import { NavBreadcrumbModel } from '@nara.drama/prologue-util';
import NavBreadcrumbWrapperView from './view/NavBreadcrumbWrapperView';


interface Props {
  items: NavBreadcrumbModel[];
  children?: React.ReactNode;
  onClickItem?: (e: React.MouseEvent, item: NavBreadcrumbModel) => void;
  onClickHome?: (e: React.MouseEvent) => void;
}

@autobind
class NavBreadcrumbContainer extends ReactComponent<Props> {
  //
  static defaultProps = {
    onClickItem: () => {
    },
    onClickHome: () => {
    },
  };

  static Model: NavBreadcrumbModel;


  onClickHome(e: React.MouseEvent) {
    //
    this.propsWithDefault.onClickHome(e);
  }

  onClickItem(event: React.MouseEvent<HTMLAnchorElement, MouseEvent>, item: NavBreadcrumbModel) {
    //
    this.propsWithDefault.onClickItem(event, item);
  }

  renderItem(item: NavBreadcrumbModel, index: number) {
    //
    const { items } = this.props;
    const isLast = items && index === items.length - 1;
    let sectionEl;

    if (isLast) {
      sectionEl = (
        <Link key={index} color="textPrimary">
          {item.text}
        </Link>
      );
    }
    else {
      sectionEl = (
        <Link
          key={index}
          onClick={(event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => this.onClickItem(event, item)}
          color="inherit"
        >
          {item.text}
        </Link>
      );
    }

    return sectionEl;
  }

  render() {
    //
    const { items, children } = this.props;

    return (
      <NavBreadcrumbWrapperView>

        <Breadcrumbs className="standard" separator="›">
          <Link
            key="Home"
            onClick={this.onClickHome}
            color="inherit"
          >
            Home
          </Link>

          {Array.isArray(items) && items.map((item, index) => (
            this.renderItem(item, index)
          ))}
        </Breadcrumbs>

        {children}
      </NavBreadcrumbWrapperView>
    );
  }
}

export default NavBreadcrumbContainer;
