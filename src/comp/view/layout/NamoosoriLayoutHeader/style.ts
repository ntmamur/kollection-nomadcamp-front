
import { createStyles, Theme, WithStyles as MuiWithStyles, withStyles as muiWithStyles } from '@material-ui/core/styles';


const style = (theme: Theme) => createStyles({
  card: {
    display: 'flex',
    height: theme.spacing(40),
    cursor: 'pointer',
    background: 'transparent',
    border: '0',
  },
  actionArea: {
    width: 'auto',
  },
  media: {
    marginRight: theme.spacing(2),
    borderRadius: theme.shape.borderRadius,
    display: 'flex',
    alignItems: 'center',
    overflow: 'hidden',
    '& > *': {
      width: '80%',
      height: '80%',
    },
  },
  mediaIcon: {
    width: '160px',
    '& > *': {
      width: '100%',
      height: 'auto',
    },
  },
  textWrapper: {
    flex: 1,
    wordBreak: 'break-all',
  },
  iconWrapper: {
    display: 'flex',
    alignItems: 'center',
    padding: 0,
    '& > *': {
      marginRight: theme.spacing(3),
    },
  },
});

export default style;
export type WithStyles = MuiWithStyles<typeof style>;
export const withStyles = muiWithStyles(style);
