import React from 'react';
import { observer } from 'mobx-react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { Box, Container, Grid, Paper, Toolbar, Typography } from '@nara.drama/material-ui';
import { WithStyles, withStyles } from './style';


interface Props extends WithStyles {
  title: string;
  description: string;
  img: string;
  action?: React.ReactNode;
  onClick?: (event: React.MouseEvent) => void;
}

@autobind
@observer
class LayoutHeaderView extends ReactComponent<Props> {
  //
  static defaultProps = {
    onClick: () => {
    },
    action: () => null,
  };

  render() {
    //
    const { classes, title, description, img, action } = this.propsWithDefault;

    return (
      <>
        <Toolbar />
        <Paper>
          <Container maxWidth="lg">
            <Box py={8}>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <Box display="flex" alignContent="center">
                    <Box mr={5} className={classes.mediaIcon}>
                      <img src={img} alt={title} />
                    </Box>
                    <Box mb={2} className={classes.textWrapper}>
                      <Typography paragraph variant="h3">{title}</Typography>
                      <Typography style={{ whiteSpace: 'pre-line' }}>
                        {description.slice(0, 300)}
                        {description.length > 300 ? '...' : ''}
                      </Typography>
                    </Box>
                    <Box display="flex" flexDirection="column-reverse">
                      {action}
                    </Box>
                  </Box>
                </Grid>
              </Grid>
            </Box>
          </Container>
        </Paper>
      </>
    );
  }
}

export default withStyles(LayoutHeaderView);
