
import React, { ReactElement, Component } from 'react';
import autobind from 'autobind-decorator';

import { Popover as MuiPopover, PopoverProps } from '@material-ui/core';
import Trigger, { TriggerTypes } from '../Trigger';


type Props = {
  trigger?: ReactElement;
  open?: boolean;
} & Omit<PopoverProps, 'open'>;

interface State {
  anchorEl: PopoverProps['anchorEl'];
}

/**
 * Material-UI extended
 */
@autobind
class PopoverView extends Component<Props> {
  //
  static defaultProps = {
    trigger: undefined,
    open: false,
    elevation: 8,
    marginThreshold: 16,
    onClose: () => {},
  };

  state: State = {
    anchorEl: undefined,
  };


  getAnchorEl() {
    //
    const { anchorEl: anchorElProps } = this.props;
    const { anchorEl: anchorElState } = this.state;

    if (typeof anchorElProps !== 'undefined') {
      return anchorElProps;
    }
    else {
      return anchorElState;
    }
  }

  onOpen(event: React.SyntheticEvent) {
    //
    this.setState({
      anchorEl: event.currentTarget,
    });
  }

  onClose(event: {}, reason: 'backdropClick' | 'escapeKeyDown') {
    //
    this.props.onClose!(event, reason);

    this.setState({
      anchorEl: null,
    });
  }

  render() {
    //
    const { trigger, open, children, ...otherProps } = this.props;

    return trigger ? (
      <Trigger
        toggle
        element={trigger}
        onOpen={this.onOpen}
        onClose={this.onClose}
      >
        {(context: TriggerTypes.TriggerContextParams) => (
          <MuiPopover
            {...otherProps}
            open={open || context.open}
            anchorEl={this.getAnchorEl()}
            onClose={context.onClose}
          >
            {children}
          </MuiPopover>
        )}
      </Trigger>
    ) : (
      <MuiPopover
        {...otherProps}
        open={open || false}
        anchorEl={this.getAnchorEl()}
      >
        {children}
      </MuiPopover>
    );
  }
}

export default PopoverView;
