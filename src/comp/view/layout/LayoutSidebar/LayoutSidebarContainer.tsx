import React from 'react';
import { autobind, ReactComponent, InvalidPropsException } from '@nara.drama/prologue';
import { Toolbar, List, ListSubheader, ListItem, ListItemText, Drawer } from '@material-ui/core';
import { NameTextModel } from '@nara.drama/prologue-util';

import MenuType from './model/MenuType';
import { WithStyles, withStyles } from './style';


interface Props extends WithStyles {
  activeName: string;
  menus: NameTextModel[];
  onClickMenu?: (menu: NameTextModel) => void;
}

@autobind
class LayoutSidebarContainer extends ReactComponent<Props> {
  //
  static defaultProps = {
    onClickMenu: () => {
    },
  };

  onClickMenu(menu: NameTextModel) {
    //
    const { onClickMenu } = this.props;

    if (typeof onClickMenu !== 'function') {
      throw new InvalidPropsException('LayoutSidebar', 'onClickMenu');
    }
    onClickMenu(menu);
  }

  renderTitleMenu(menu: NameTextModel, index: number) {
    //
    const { classes } = this.props;

    return (
      <ListSubheader
        key={`menu-${index}`}
        disableGutters
        className={classes.title}
      >
        {menu.text}
      </ListSubheader>
    );
  }

  renderBasicMenu(menu: NameTextModel, index: number) {
    //
    const { activeName } = this.props;

    return (
      <ListItem
        button
        key={`menu-${index}`}
        selected={activeName === menu.name}
        onClick={() => this.onClickMenu(menu)}
      >
        <ListItemText
          primary={menu.text}
        />
      </ListItem>
    );
  }


  render() {
    //
    const { classes, menus } = this.props;

    return (
      <Drawer
        open
        variant="permanent"
        className={classes.drawer}
        PaperProps={{
          className: classes.paper,
        }}
      >
        <Toolbar />

        <List>
          {menus.map((menu, index) => {
            if (menu.type === MenuType.Title) {
              return this.renderTitleMenu(menu, index);
            }
            else {
              return this.renderBasicMenu(menu, index);
            }
          })}
        </List>
      </Drawer>
    );
  }
}

export default withStyles(LayoutSidebarContainer);
