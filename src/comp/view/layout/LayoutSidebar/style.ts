import {
  createStyles,
  Theme,
  WithStyles as MuiWithStyles,
  withStyles as muiWithStyles,
} from '@material-ui/core/styles';


const style = (theme: Theme) => createStyles({
  // root: {
  //   // display: 'flex',
  //   // flexShrink: 0,
  // },
  title: {
    paddingRight: theme.spacing(2),
    paddingLeft: theme.spacing(2),
  },
  drawer: {
    width: 260,
  },
  paper: {
    position: 'fixed',
    width: 260,
    height: '100vh',
  },
  select: {
    color: 'red',
  },
  selected: {
    color: 'red',
  },
  item: {
    '&$selected': {
      color: 'red',
    },
  },
  itemSelected: {},
});

export default style;
export type WithStyles = MuiWithStyles<typeof style>;
export const withStyles = muiWithStyles(style);
