import React from 'react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import clsx from 'clsx';
import { WithStyles, withStyles } from './style';


interface Props extends WithStyles {
  img?: string;
  className?: string;
}

@autobind
class HeaderImageView extends ReactComponent<Props> {
  //
  static defaultProps = {
    img: '',
    className: '',
  };


  render() {
    //
    const { classes, className, img } = this.props;

    return (
      <div className={clsx(classes.ntr_simg, className)} />
    );
  }
}

export default withStyles(HeaderImageView);
