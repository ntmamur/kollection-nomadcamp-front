import {
  createStyles,
  Theme,
  WithStyles as MuiWithStyles,
  withStyles as muiWithStyles,
} from '@material-ui/core/styles';


const style = (theme: Theme) => createStyles({
  icon: {
    verticalAlign: 'middle',
    marginRight: theme.spacing(1),
  },
  error: {
    color: theme.palette.error.main,
  },
  info: {
    color: theme.palette.info.main,
  },
  question: {
    color: theme.palette.primary.main,
  },
  check: {
    color: theme.palette.primary.main,
  },
});

export default style;
export type WithStyles = MuiWithStyles<typeof style>;
export const withStyles = muiWithStyles(style);
