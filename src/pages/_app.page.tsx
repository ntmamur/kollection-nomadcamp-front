import React, { Component } from 'react';
import { autobind } from '@nara.drama/prologue';
import { Provider } from 'mobx-react';
import { AppProps } from 'next/app';
import { Helmet } from 'react-helmet';
import { dialogTypes, dialogUtil } from '@nara.drama/prologue-util';
import { Bay } from '@nara.platform/dock';
import { store } from '~/comp/state';
import mainRouter from './mainRouter';
import configureApp from './configureApp';
import '../../public/css/rsmap.css';
import '../../public/css/editor.css';
import { ReactApp } from '~/comp/view';
import { KollectieType, useLoginInfo} from '~/nara';
import { ManagementLayout } from '~/pages/management/layout';
import { EmployeeLayout } from '~/pages/employee/layout';


configureApp();

@autobind
class ChannelApp extends Component<AppProps> {
  //
  routeToMain() {
    //
    mainRouter.index(this.props.router);
  }

  routeToLogin() {
    //
    mainRouter.login();
  }

  onExpiredSession() {
    console.log('onExpiredSession');
    dialogUtil.alert('인증 유효 시간이 만료되어 로그인 페이지로 이동합니다..', {
      noticeType: dialogTypes.NoticeType.Info,
    }).then(() => this.routeToLogin());
  }

  getKollectieType(): KollectieType {
    //
    const kollectiePath = this.props.router.pathname.split('/')[1];

    switch (kollectiePath) {
      case 'management':
        return KollectieType.Management;
      case 'employee':
        return KollectieType.Employee;
      default:
        return KollectieType.Employee;
    }
  }

  getOptionalDevAccount() {
    //
    const isProduction = process.env.NODE_ENV === 'production';

    if (isProduction) {
      return undefined;
    }
    else {
      // TODO fix mechanism of authorization in dev mode
      if (typeof window !== 'undefined') {
        useLoginInfo();
      }
      return undefined;

      // return new IdName('dev_001@nextree.io', '1');
    }
  }

  renderByLayout() {
    const { Component, pageProps } = this.props;
    const kollectieType = pageProps.kollectieType || this.getKollectieType();

    if (pageProps.withoutAppLayout) {
      return (
        <Component
          {...pageProps}
        />
      );
    }
    else {
      switch (kollectieType) {
        case KollectieType.Management:
          return (
            <ManagementLayout>
              <Component
                {...pageProps}
              />
            </ManagementLayout>
          );
        case KollectieType.Employee:
          return (
            <EmployeeLayout>
              <Component
                {...pageProps}
              />
            </EmployeeLayout>
          );
        default:
          return (
            <Component
              {...pageProps}
            />
          );
      }
    }
  }

  render() {
    //
    const { pageProps } = this.props;

    return (
      <Provider
        {...store}
      >
        <ReactApp>
          {/*<Bay
            routeToAppMainAction={this.routeToMain}
            // below optional
            isPublic={pageProps.withoutAuth}
            devAccount={this.getOptionalDevAccount()}
            onExpiredSession={this.onExpiredSession}
          >*/}
            <Helmet>
              <title>template</title>
            </Helmet>
            {this.renderByLayout()}
          {/*</Bay>*/}
        </ReactApp>
      </Provider>
    );
  }
}

export default ChannelApp;
