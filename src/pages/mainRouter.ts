import { NextRouter } from 'next/dist/client/router';
import { Route, routeUtil } from '@nara.drama/prologue';
import Router from 'next/router';


const mainRouter = {
  //
  index: (nextRouter: NextRouter) =>
    //
    mainRouter.main(nextRouter),

  main: (nextRouter: NextRouter) => {
    const route = routeUtil.create('/');

    nextRouter.push(route.href, route.as);
  },

  login: () => {
    const domain = window.location.host;

    window.location.replace(`http://${domain}${process.env.LOGIN_PATH}`);
  },
};

export default mainRouter;

export function routeAction(route: Route) {
  Router.push(route.href, route.as);
}
