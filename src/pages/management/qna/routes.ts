import { routeUtil } from '@nara.drama/prologue';
import { baseUrl } from '../routes';


const routes = {
  //
  index: () =>
    //
    routes.qna(),

  qna: () =>
    //
    routeUtil.create(`${baseUrl}/qna`),

  questionDetail: (questionId: string) =>
    //
    routeUtil.create(`${baseUrl}/qna/${questionId}`),
  questionForm: () =>
    //
    routeUtil.create(`${baseUrl}/qna/form`)
  ,
};

export default routes;
