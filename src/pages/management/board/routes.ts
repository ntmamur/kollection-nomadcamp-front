import { routeUtil } from '@nara.drama/prologue';
import { baseUrl } from '../routes';


const routes = {
  //
  index: () =>
    //
    routes.board(),

  board: () =>
    //
    routeUtil.create(`${baseUrl}/board`),

  postingDetail: (boardName: string, postingId: string) =>
    //
    routeUtil.create(`${baseUrl}/board/${boardName}/${postingId}`)
  ,
};

export default routes;
