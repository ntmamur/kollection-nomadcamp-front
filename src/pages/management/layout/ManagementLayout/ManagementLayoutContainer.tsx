import React, { ContextType } from 'react';
import { observer } from 'mobx-react';
import { NextRouter, withRouter } from 'next/router';
import { autobind, ReactComponent, Route } from '@nara.drama/prologue';
import { AppContext } from '@nara.drama/prologue-util';
import { dock, MenuNode, WithDock } from '@nara.platform/dock';
import { KollectieType } from '~/nara';
import KollectieLayoutView from './view/KollectieLayoutView';
import { menuNodes } from './menus';
import {NomadcampDock} from '~/comp/view/dockingStation/AppDock';


const menu = menuNodes;

interface Props {
  router: NextRouter;
}

interface State {
}

interface InjectedProps extends WithDock{
}

@dock([], menu)
@autobind
@observer
class ManagementLayoutContainer extends ReactComponent<Props, State, InjectedProps> {
  //
  static contextType = AppContext.Context;

  context!: ContextType<typeof AppContext.Context>;

  onRoute(route: Route) {
    //
    this.props.router.push(route.href, route.as);
  }

  routeToHome() {
    //
    this.onRoute({ href: '/', as: '/' });
  }

  getCurrentMenuKey(): string {
    //
    const pathname = this.props.router.pathname;
    const paths = pathname.split('/');
    return paths[2];
  }

  getSubMenus(activeMenuKey: string) {
    const { menu } = this.injected;

    const activeMenu = menu.find(menuNode => menuNode.key === activeMenuKey);
    return activeMenu && activeMenu.children || [];
  }

  onClickLogo() {
    //
    this.routeToHome();
  }

  onClickMenu(menu: MenuNode) {
    //
    if (!menu.route) {
      return;
    }

    this.onRoute(menu.route);
  }

  render() {
    const { menu } = this.injected;
    const activeMenuKey = this.getCurrentMenuKey();

    const subMenus = this.getSubMenus(activeMenuKey);

    return (
      <KollectieLayoutView
        appDock={
          <NomadcampDock
            kollectieType={KollectieType.Management}
            menuNodes={menu}
            activeMenuKey={activeMenuKey}
            onClickLogo={this.onClickLogo}
            onClickMenu={this.onClickMenu}
          />
        }
        subMenus={subMenus}
      >
        {this.props.children}
      </KollectieLayoutView>

    );
  }
}

export default withRouter(ManagementLayoutContainer);
