import {MenuNode} from '@nara.platform/dock';
import qnaRoutes from '../../qna/routes';
import boardRoutes from '../../board/routes';


import { StageRoleType } from '~/nara';


export const menuNodes = [
  MenuNode.new({
    key: 'nomad',
    label: 'Nomad',
    //route: blockRoutes.index(),
    roles: [StageRoleType.Admin, StageRoleType.Manager, StageRoleType.Member],
  }),
  MenuNode.new({
    key: 'project',
    label: 'Project',
    //route: learninglogRoutes.index(),
    roles: [StageRoleType.Admin, StageRoleType.Manager, StageRoleType.Member],
  }),
  MenuNode.new({
    key: 'qna',
    label: 'Q & A',
    route: qnaRoutes.index(),
    roles: [StageRoleType.Admin, StageRoleType.Manager, StageRoleType.Member],
  }),
  MenuNode.new({
    key: 'board',
    label: 'Board',
    route: boardRoutes.index(),
    roles: [StageRoleType.Admin, StageRoleType.Manager, StageRoleType.Member],
  }),
  MenuNode.new({
    key: 'issue',
    label: 'Issue',
    //route: boardRoutes.index(),
    roles: [StageRoleType.Admin, StageRoleType.Manager, StageRoleType.Member],
  }),
];
