import React from 'react';
import { NextRouter, withRouter } from 'next/router';
import { withDock, WithDock } from '@nara.platform/dock';
import { observer } from 'mobx-react';
import { JsonViewer, KollectieType } from '~/nara';


export async function getStaticProps() {
  //
  return {
    props: {
      kollectieType: KollectieType.FunctionPattern,
    },
  };
}

interface Props extends WithDock{
  router: NextRouter;
}

const IndexPage: React.FC<Props> = observer(
  ({ router, ...rest }) => {

    const { dock, roles, allowed } = rest;
    const { currentCineroom, currentAudience, currentCast, currentActor } = dock;
    const { getStageRoles, getTaskRoles, hasStageRoles, hasTaskRoles } = roles;

    return (
      <div style={{ paddingLeft: '100px', paddingTop: '100px' }}>
        <h1>
          [functionpattern/bar Page] Customize design by below information...
        </h1>
      </div>
    );
  }
);

export default withRouter(withDock(IndexPage));
