import React from 'react';
import { NextRouter, withRouter } from 'next/router';
import { withDock, WithDock } from '@nara.platform/dock';
import { observer } from 'mobx-react';
import { JsonViewer, KollectieType, StageRoleType } from '~/nara';


export async function getStaticProps() {
  //
  return {
    props: {
      kollectieType: KollectieType.FunctionPattern,
    },
  };
}

const pageRoles = [StageRoleType.Manager, StageRoleType.Admin];

interface Props extends WithDock{
  router: NextRouter;
}

const IndexPage: React.FC<Props> = observer(
  ({ router, ...rest }) => {

    const { dock, roles, allowed } = rest;
    const { currentCineroom, currentAudience, currentCast, currentActor } = dock;
    const { getStageRoles, getTaskRoles, hasStageRoles, hasTaskRoles } = roles;

    return (
      <div style={{ paddingLeft: '100px', paddingTop: '100px' }}>
        <h1>
          [functionpattern/bar/content Page] Customize design by below information...
        </h1>
        <JsonViewer title={'Current Cineroom'} data={currentCineroom} />
        <JsonViewer title={'Current Audience'} data={currentAudience} />
        <JsonViewer title={'Current Cast'} data={currentCast} />
        <JsonViewer title={'Current Actor'} data={currentActor} />
        <JsonViewer title={'StageRoles of Current Actor'} data={getTaskRoles} />
        <JsonViewer title={'Is allowed to this page'} data={allowed} />
      </div>
    );
  }
);

export default withRouter(withDock(IndexPage, pageRoles));
