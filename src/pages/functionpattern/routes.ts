import { routeUtil } from '@nara.drama/prologue';


const baseUrl = '/functionpattern';
const routes = {
  //
  index: () =>
    //
    routes.foo(),

  foo: () =>
    //
    routeUtil.create(`${baseUrl}/foo`),

  board: () =>
    //
    routeUtil.create(`${baseUrl}/bar`)
  ,
};

export default routes;
export { baseUrl };
