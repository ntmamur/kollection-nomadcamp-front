import { routeUtil } from '@nara.drama/prologue';
import { baseUrl } from '../routes';


const routes = {
  //
  index: () =>
    //
    routes.content(),

  content: () =>
    //
    routeUtil.create(`${baseUrl}/foo/content`),

  management: () =>
    //
    routeUtil.create(`${baseUrl}/foo/management`),
};

export default routes;
