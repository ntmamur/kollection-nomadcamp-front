import React from 'react';

import { NextRouter, withRouter } from 'next/router';
import { observer } from 'mobx-react';
import { withDock, WithDock } from '@nara.platform/dock';
import { KollectieType } from '~/nara';
import { AppDock } from '~/comp';
import KollectieLayoutView from './view/KollectieLayoutView';
import { menuNodes } from './menus';


const kollectieMenu = menuNodes;

interface Props extends WithDock{
  router: NextRouter;
}

const KollectieLayoutContainer: React.FC<Props> = observer(
  ({ router, ...rest }) => {

    const { menu, children } = rest;

    function routeToHome() {
      //
      router.push('/');
    }

    function getCurrentMenuKey(): string {
      //
      const pathname = router.pathname;
      const paths = pathname.split('/');
      return paths[2];
    }

    function getSubMenus(activeMenuKey: string) {
      const activeMenu = menu.find(menuNode => menuNode.key === activeMenuKey);
      return activeMenu && activeMenu.children || [];
    }

    return (
      <KollectieLayoutView
        appDock={
          <AppDock
            kollectieType={KollectieType.FunctionPattern}
            menuNodes={menu}
            activeMenuKey={getCurrentMenuKey()}
          />
        }
        subMenus={getSubMenus(getCurrentMenuKey())}
      >
        {children}
      </KollectieLayoutView>
    );
  }
);

export default withRouter(withDock(KollectieLayoutContainer, [], kollectieMenu));
