import { MenuNode } from '@nara.platform/dock';
import { StageRoleType } from '~/nara';
import fooRoutes from '~/pages/functionpattern/foo/routes';
import barRoutes from '~/pages/functionpattern/bar/routes';


export const menuNodes = [
  MenuNode.new({
    key: 'foo',
    label: 'FOO',
    route: fooRoutes.index(),
    children: [
      MenuNode.new({   // "Route not defined" maybe mean not "Action Menu", but "Title Menu"
        key: 'foo',
        label: 'Foo',
      }),
      MenuNode.new({   // "Route defined" maybe mean "Action Menu"
        key: 'content',
        label: 'Content',
        route: fooRoutes.content(),
      }),
      MenuNode.new({   // "Route defined" maybe mean "Action Menu"
        key: 'management',
        label: 'Management',
        route: fooRoutes.management(),
        roles: [StageRoleType.Admin, StageRoleType.Manager],
      }),
    ],
  }),
  MenuNode.new({
    key: 'bar',
    label: 'BAR',
    route: barRoutes.index(),
    roles: [StageRoleType.Admin, StageRoleType.Manager],
    children: [
      MenuNode.new({
        key: 'bar',
        label: 'Bar',
        roles: [StageRoleType.Admin, StageRoleType.Manager],
      }),
      MenuNode.new({  // In this case, "Member" cannot see this menu even though permitted at this menu, because "Member" was rejected at upper menu
        key: 'content',
        label: 'Content',
        route: barRoutes.content(),
      }),
    ],
  }),
];
