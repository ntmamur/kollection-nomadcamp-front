import React from 'react';
import { observer } from 'mobx-react';
import { MenuNode } from '@nara.platform/dock';
import { WithStyles, withStyles } from './style';


interface Props extends WithStyles{
  appDock: React.ReactNode;
  subMenus: MenuNode[];
}

const KollectieLayoutView: React.FC<Props> = observer(
  ({ appDock, ...rest }) => {
    const { children } = rest;
    return (
      <>
        <h2>
          Customize design...
        </h2>
        {appDock}
        {children}
      </>
    );
  }
);

export default withStyles(KollectieLayoutView);
