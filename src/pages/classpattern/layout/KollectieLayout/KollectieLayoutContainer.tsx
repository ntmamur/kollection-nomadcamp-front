import React, { ContextType } from 'react';
import { autobind, ReactComponent } from '@nara.drama/prologue';

import { NextRouter, withRouter } from 'next/router';

import { AppContext } from '@nara.drama/prologue-util';
import { observer } from 'mobx-react';
import { dock, WithDock } from '@nara.platform/dock';
import { KollectieType } from '~/nara';
import { AppDock } from '~/comp';
import KollectieLayoutView from './view/KollectieLayoutView';
import { menuNodes } from './menus';


const menu = menuNodes;

interface Props{
  router: NextRouter;
}

interface InjectedProps extends WithDock{
}

@dock([], menu)
@autobind
@observer
class KollectieLayoutContainer extends ReactComponent<Props, {}, InjectedProps> {
  //
  static contextType = AppContext.Context;

  context!: ContextType<typeof AppContext.Context>;

  routeToHome() {
    //
    this.props.router.push('/');
  }

  getCurrentMenuKey(): string {
    //
    const pathname = this.props.router.pathname;
    const paths = pathname.split('/');
    return paths[2];
  }

  getSubMenus(activeMenuKey: string) {
    const { menu } = this.injected;

    const activeMenu = menu.find(menuNode => menuNode.key === activeMenuKey);
    return activeMenu && activeMenu.children || [];
  }

  render() {
    const { menu } = this.injected;
    const activeMenuKey = this.getCurrentMenuKey();
    const subMenus = this.getSubMenus(activeMenuKey);

    return (
      <KollectieLayoutView
        appDock={
          <AppDock
            kollectieType={KollectieType.ClassPattern}
            menuNodes={menu}
            activeMenuKey={activeMenuKey}
          />
        }
        subMenus={subMenus}
      >
        {this.props.children}
      </KollectieLayoutView>
    );
  }
}

export default withRouter(KollectieLayoutContainer);
