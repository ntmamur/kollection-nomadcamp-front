import React from 'react';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { MenuNode } from '@nara.platform/dock';
import { WithStyles, withStyles } from './style';


interface Props {
  appDock: React.ReactNode;
  subMenus: MenuNode[];
}

interface InjectedProps extends WithStyles {
}

@autobind
class KollectieLayoutView extends ReactComponent<Props, {}, InjectedProps> {
  //
  render() {
    //
    const { appDock, subMenus } = this.props;
    return (
      <>
        <h2>
          Customize design...
        </h2>
        {appDock}
        {this.props.children}
      </>
    );
  }
}

export default withStyles(KollectieLayoutView);
