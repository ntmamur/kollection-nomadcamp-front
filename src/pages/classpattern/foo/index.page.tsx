import React from 'react';
import { NextRouter, withRouter } from 'next/router';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { dock, WithDock } from '@nara.platform/dock';
import { JsonViewer, KollectieType } from '~/nara';


export async function getStaticProps() {
  //
  return {
    props: {
      kollectieType: KollectieType.ClassPattern,
    },
  };
}

interface Props {
  router: NextRouter;
}

interface InjectedProps extends WithDock{
}

@dock()
@autobind
class IndexPage extends ReactComponent<Props, {}, InjectedProps> {
  //
  render() {
    //
    const { dock, roles, allowed } = this.injected;
    const { currentCineroom, currentAudience, currentCast, currentActor } = dock;
    const { getStageRoles, getTaskRoles, hasStageRoles, hasTaskRoles } = roles;

    return (
      <div style={{ paddingLeft: '100px', paddingTop: '100px' }}>
        <h1>
          [classpattern/foo Page] Customize design by below information...
        </h1>
        <JsonViewer title={'Current Cineroom'} data={currentCineroom} />
        <JsonViewer title={'Current Audience'} data={currentAudience} />
        <JsonViewer title={'Current Cast'} data={currentCast} />
        <JsonViewer title={'Current Actor'} data={currentActor} />
        <JsonViewer title={'StageRoles of Current Actor'} data={getTaskRoles} />
        <JsonViewer title={'Is allowed to this page'} data={allowed} />
      </div>
    );
  }
}

export default withRouter(IndexPage);
