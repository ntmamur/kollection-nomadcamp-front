import { routeUtil } from '@nara.drama/prologue';
import { baseUrl } from '../routes';


const routes = {
  //
  index: () =>
    //
    routeUtil.create(`${baseUrl}/bar`),

  content: () =>
    //
    routeUtil.create(`${baseUrl}/bar/content`),
};

export default routes;
