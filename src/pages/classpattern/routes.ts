import { routeUtil } from '@nara.drama/prologue';


const baseUrl = '/classpattern';
const routes = {
  //
  index: () =>
    //
    routeUtil.create(`${baseUrl}`),

  foo: () =>
    //
    routeUtil.create(`${baseUrl}/foo`),

  board: () =>
    //
    routeUtil.create(`${baseUrl}/bar`),
};

export default routes;
export { baseUrl };
