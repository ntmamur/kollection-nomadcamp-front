import React from 'react';
import { NextRouter, withRouter } from 'next/router';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { dialogTypes, dialogUtil } from '@nara.drama/prologue-util';
import { dock, WithDock } from '@nara.platform/dock';
import { Box, Grid, Typography } from '@material-ui/core';
import { NWithStyles, nWithStyles } from '~/comp/view/theme';


export async function getStaticProps() {
  //
  return {
    props: {
      withoutAppLayout: true,
      withoutAuth: true,
    },
  };
}

interface Props extends NWithStyles {
  router: NextRouter;
}

interface State {
  openResetPasswordModal: boolean;
}

interface InjectedProps extends WithDock{
}

@dock()
@autobind
class IndexPage extends ReactComponent<Props, State, InjectedProps> {
  //
  state: State = {
    openResetPasswordModal: false,
  };

  async onSuccess() {
    //
    // const { findCineDock } = this.injected.stageContextStateKeeper;
    //
    // await findCineDock();
    //
    // const { kollecties } = this.injected.stageContextStateKeeper;
    //
    // if (kollecties && kollecties[0]) {
    //   const kollectie = kollecties[0];
    //   this.routeToProgram(kollectie.id);
    //
    // }
    // else {
    //   dialogUtil.alert({ title: 'Alert', message: `나무소리 사용 권한이 없습니다.` });
    // }
  }

  routeToProgram(membership: string): void {
    //
    this.props.router.push(`/${membership}`);
  }

  openResetPasswordModal() {
    //
    this.setState({ openResetPasswordModal: true });
  }

  closeResetPasswordModal() {
    //
    this.setState({ openResetPasswordModal: false });
  }

  onSuccessResetPassword() {
    //
    dialogUtil.alert({
      title: 'Success',
      type: dialogTypes.NoticeType.Info,
      message: 'password changed.',
    });
    this.setState({ openResetPasswordModal: false });
  }

  render() {
    //
    const { classes } = this.props;
    const { dock, roles } = this.injected;

    return (
      <Grid container className={classes.nl_wrap}>
        <Grid container item alignItems="center" className={classes.nl_ob}>
          <Grid item className={classes.nl_rb}>
            <Box className={classes.nl_img2} />
          </Grid>
          <Grid container item alignItems="center" direction="column" justify="space-between" className={classes.nl_lb}>
            <Box />
            <Box className={classes.nl_con}>
              <Grid container>
                <Box component="h1" className={classes.nl_logo} />
              </Grid>
              <Box mb={8}>
                <Typography variant="h2" className={classes.nl_h2}>MSA Developer Training</Typography>
              </Box>
              <Box className={classes.nl_sp1}>
                <Box className={classes.nl_line} />
              </Box>
              <Typography variant="body1" className={classes.nl_footer}>
                Nextree
                <span className={classes.nl_footer_line} />주소 : 08503 서울 금천구 가산디지털1로 171<br />
                <span className={classes.nl_footer_line} />TEL : 02 6332 5250 <br />
                Copyright© 2021 nomadcamp.io. All rights reserved.
              </Typography>
            </Box>
            <Box />
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

export default withRouter(nWithStyles(IndexPage));
