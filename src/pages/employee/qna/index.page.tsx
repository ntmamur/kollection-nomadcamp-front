import React from 'react';
import { observer } from 'mobx-react';
import { NextRouter, withRouter } from 'next/router';
import { autobind, ReactComponent } from '@nara.drama/prologue';
import { CoachQuestionList, Question } from '@nara.drama/qna';
import { Box, Button, Divider, Grid, Typography } from '@material-ui/core';
import { dock, WithDock } from '@nara.platform/dock';
import { Card } from '@nara.drama/material-ui';
import routes from './routes';
import { KollectieType, StageRoleType } from '~/nara';
import { PageLayout } from "~/comp/view/layout";
import { CoachQuestionDetail } from "~/comp/view/drama-comp";
import { nWithStyles } from "~/comp/view/theme";
import { QnaUtil } from "~/comp/view/shared/util";
import { NWithStyles } from "~/comp/view/theme";
import { Modal } from "~/comp/view/comp";
import { Editor } from "~/comp/view/comp";

export async function getStaticProps() {
  //
  return {
    props: {
      kollectieType: KollectieType.Employee,
    },
  };
}

const pageRoles = [StageRoleType.Admin, StageRoleType.Manager, StageRoleType.Member];

interface Props extends NWithStyles {
  router: NextRouter;
}

interface State {
  selectedQuestion: Question | null;
  showQuestionModal: boolean;
}

interface InjectedProps extends WithDock {
}


@dock(pageRoles)
@autobind
@observer
class QnAPage extends ReactComponent<Props, State, InjectedProps> {
  //
  state = { showQuestionModal: false, selectedQuestion: null };

  getClientAccountId() {
    //
    // FIXME currentKollectionEditionId
    const { currentActorId } = this.injected.dock;

    return QnaUtil.genClientAccountId(currentActorId, 'currentKollectionEditionId');
  }

  routeToDetail(event: React.MouseEvent, question: Question) {
    //
    const route = routes.questionDetail(question.id);

    this.props.router.push(route.href, route.as);
  }

  renderEmptyList(): React.ReactNode {
    //
    return (
      <Box mx={3} my={3} display="flex" justifyContent="center" flexDirection="column" alignItems="center">
        <Box p={4} mb={3} width="110px" height="110px" textAlign="center" style={{ borderRadius: '100%', backgroundColor: '#f9f9f9' }}>
          <img src={`${process.env.BASE_PATH}/images/all/icon_nodata.svg`} />
        </Box>
        <Typography color="textSecondary" variant="body2">
          등록된 질문이 없습니다.
        </Typography>
      </Box>
    );
  }

  onClickQuestion(event: React.MouseEvent, question: Question) {
    //
    this.setState({ selectedQuestion: question, showQuestionModal: true });
  }

  onSuccessRegisterAnswer() {

  }

  onFailRegisterAnswer() {

  }

  onClickClose() {
    //
    this.setState({ selectedQuestion: null, showQuestionModal: false });
  }

  renderQuestionDetailModal() {
    //
    const { currentAudienceId } = this.injected.dock;
    const { showQuestionModal, selectedQuestion } = this.state;
    const { classes } = this.props;

    if (!selectedQuestion) {
      return null;
    }

    if (selectedQuestion) {
      return (
        <Modal
          maxWidth="lg"
          open={showQuestionModal}
          className={classes.modal}
        >
          <div className={classes.nmodalQna}>
            <Typography component="h3" className={classes.nmodal_tit}>답변하기</Typography>
            <CoachQuestionDetail
              question={selectedQuestion}
              audienceId={currentAudienceId}
              onSuccess={this.onSuccessRegisterAnswer}
              onFail={this.onFailRegisterAnswer}
            />
            <Divider />
            <Grid container justify="center">
              <Box mt={2} mb={2}>
                <Grid item>
                  <Button variant="contained" color="primary" onClick={this.onClickClose}>확인</Button>
                </Grid>
              </Box>
            </Grid>
          </div>
        </Modal>
      );
    }
    else {
      return null;
    }
  }

  render() {
    //
    const { classes } = this.props;
    const { currentTroupeId } = this.injected.dock;

    return (
      <PageLayout>
        <PageLayout.Header
          className={classes.ntr_simg03}
          title="Q&A"
          description={''}
        />
        <PageLayout.Body>
          <CoachQuestionList
            clientAccountId={this.getClientAccountId()}
            groupId={currentTroupeId}
          >
            <CoachQuestionList.Header />
            <Card>
              <Card.Content>
                <CoachQuestionList.Content
                  renderQuestion={(question: Question) => (
                    <Editor
                      readOnly
                      contentId={question.id}
                      content={question.answerMessage}
                      height={475}
                    />
                  )}
                  emptyList={this.renderEmptyList()}
                  onClick={this.onClickQuestion}
                />
                <Box pt={2} display="flex" justifyContent="center">
                  <CoachQuestionList.Pagination />
                </Box>
              </Card.Content>
            </Card>
          </CoachQuestionList>
          {this.renderQuestionDetailModal()}
        </PageLayout.Body>
      </PageLayout>
    );
  }
}

export default withRouter(nWithStyles(QnAPage));
