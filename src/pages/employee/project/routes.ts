import { routeUtil } from '@nara.drama/prologue';
import { baseUrl } from '../routes';


const routes = {
  //
  index: () =>
    //
    routes.projects(),

  projects: () =>
    //
    routeUtil.create(`${baseUrl}/projects`),
};



export default routes;
