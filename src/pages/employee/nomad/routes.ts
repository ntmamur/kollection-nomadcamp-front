import { routeUtil } from '@nara.drama/prologue';
import { baseUrl } from '../routes';


const routes = {
  //
  index: () =>
    //
    routes.nomad(),

  nomad: () =>
    //
    routeUtil.create(`${baseUrl}/nomad`),
};



export default routes;
