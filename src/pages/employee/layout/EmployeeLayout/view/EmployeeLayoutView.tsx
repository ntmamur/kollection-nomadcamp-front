import React from 'react';
import { autobind, ReactComponent, Route } from '@nara.drama/prologue';
import { MenuNode } from '@nara.platform/dock';
import { routeAction } from '~/pages/mainRouter';
import { WithStyles, withStyles } from './style';


interface Props {
  appDock: React.ReactNode;
  subMenus: MenuNode[];
}

interface InjectedProps extends WithStyles {
}

@autobind
class KollectieLayoutView extends ReactComponent<Props, {}, InjectedProps> {
  //
  getSubMenuActiveName(sideMenus: MenuNode[]): string {
    //
    const pathname = window.location.pathname;
    const activeMenuItem = sideMenus.find(menu => pathname.includes(menu.key));

    if (activeMenuItem) {
      return activeMenuItem.key;
    }
    return '';
  }

  renderTitle(subMenu: MenuNode) {
    return (
      <li>{subMenu.label}</li>
    );
  }

  renderMenu(subMenu: MenuNode, activeMenu: string) {
    if (subMenu.label === activeMenu) {
      return (
        <li onClick={() => routeAction(subMenu.route as Route)}>
          <b>
            {subMenu.label}
          </b>
        </li>
      );
    }
    else {
      return (
        <li onClick={() => routeAction(subMenu.route as Route)}>
          {subMenu.label}
        </li>
      );
    }
  }

  render() {
    //
    const { appDock, subMenus } = this.props;
    const activeName = this.getSubMenuActiveName(subMenus);

    return (
      <>
        {appDock}
        {
          subMenus
          && (
            <ul>
              {
              subMenus.map(subMenu => {
                if (subMenu.isTitle) {
                  return this.renderTitle(subMenu);
                }
                else {
                  return this.renderMenu(subMenu, activeName);
                }
              })
              }
            </ul>
          )
        }
        {this.props.children}
      </>
    );
  }
}

export default withStyles(KollectieLayoutView);
