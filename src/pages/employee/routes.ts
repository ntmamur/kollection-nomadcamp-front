import { routeUtil } from '@nara.drama/prologue';


const baseUrl = '/employee';
const routes = {
  //
  index: () =>
    //
    routes.qna(),

  qna: () =>
    //
    routeUtil.create(`${baseUrl}/qna`),

  board: () =>
    //
    routeUtil.create(`${baseUrl}/board`)
  ,
};

export default routes;
export { baseUrl };
