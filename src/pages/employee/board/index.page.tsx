import React from 'react';
import dynamic from 'next/dynamic';
import { observer } from 'mobx-react';
import { NextRouter, withRouter } from 'next/router';
import { autobind, IdName, ReactComponent, Route } from '@nara.drama/prologue';
import { dock, WithDock } from '@nara.platform/dock';
import { KollectieType, StageRoleType } from '~/nara';
import { BoardListTab } from '~/comp/view/drama-comp';
import routes from './routes';
import {nWithStyles, NWithStyles} from "~/comp/view/theme";
import {PageLayout} from "~/comp/view/layout";



export async function getStaticProps() {
  //
  return {
    props: {
      kollectieType: KollectieType.Management,
    },
  };
}

const pageRoles = [StageRoleType.Manager, StageRoleType.Admin, StageRoleType.Member];


const PostList = dynamic<any>(
  () => import('@nara.drama/board').then(module => module.PostList),
  { ssr: false }
);

const PostListHeader = dynamic<any>(
  () => import('@nara.drama/board').then(module => module.PostList)
    .then(postList => postList.Header),
  { ssr: false }
);

const PostListContent = dynamic<any>(
  () => import('@nara.drama/board').then(module => module.PostList)
    .then(postList => postList.Content),
  { ssr: false }
);

const PostListPagination = dynamic<any>(
  () => import('@nara.drama/board').then(module => module.PostList)
    .then(postList => postList.Pagination),
  { ssr: false }
);

interface Props extends NWithStyles {
  router: NextRouter;
}

interface State {
  boardId: string;
  boardTitle: string;
}

interface InjectedProps extends WithDock {
}


@dock(pageRoles)
@autobind
@observer
class BoardPage extends ReactComponent<Props, State, InjectedProps> {
  //
  state: State = {
    boardId: '',
    boardTitle: '',
  };

  onClickPost(postId: string) {
    //
    const { boardTitle } = this.state;
    const route = routes.postingDetail(boardTitle, postId);

    this.onRoute(route);
  }

  setBoard(boardId: string, boardTitle: string) {
    //
    this.setState({ boardId, boardTitle });
  }

  onRoute(route: Route) {
    //
    this.props.router.push(route.href, route.as);
  }

  render() {
    //
    const { classes } = this.props;
    const {
      currentCineroomId,
      currentActorId,
      currentActorName,
      // TODO
      // currentKollectionEditionId,
      currentAudienceId,
      currentTroupeId,
    } = this.injected.dock;
    const { boardId } = this.state;

    return (
      <PageLayout>
        <PageLayout.Header
          title="게시판"
          description={'나무소리 교육에 대한 새로운 소식을 확인해 보세요.'}
          className={classes.ntr_simg05}
        />
        <BoardListTab
          cineroomKey={currentCineroomId}
          troupeId={currentTroupeId}
          // FIXME
          kollectionEditionId={'currentKollectionEditionId'}
          actor={new IdName(currentActorId, currentActorName)}
          audienceKey={currentAudienceId}
          onInitDefaultBoard={this.setBoard}
          onClick={this.setBoard}
        />
        <PageLayout.Body>
          <PageLayout.Content>
            {
              boardId && (
                <PostList boardId={boardId}>
                  <PostListHeader />
                  <PostListContent
                    onRowClick={this.onClickPost}
                  />
                  <PostListPagination />
                </PostList>
              )
            }
          </PageLayout.Content>
        </PageLayout.Body>
      </PageLayout>
    );
  }
}

export default withRouter(nWithStyles(BoardPage));
