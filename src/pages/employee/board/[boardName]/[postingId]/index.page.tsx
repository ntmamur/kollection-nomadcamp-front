import React from 'react';
import dynamic from 'next/dynamic';
import { observer } from 'mobx-react';
import { NextRouter, withRouter } from 'next/router';
import { autobind, ReactComponent, IdName } from '@nara.drama/prologue';
import { Box, Button, Container, Divider, Paper, Toolbar } from '@nara.drama/material-ui';
import { dock, WithDock } from '@nara.platform/dock';
import { KollectieType, StageRoleType } from '~/nara';
import { PostCommentList, SubActions, NavBreadcrumb } from '~/comp/view';
import routes from '../../routes';



export async function getServersideProps() {
  //
  return {
    props: {
      kollectieType: KollectieType.Management,
    },
  };
}

const pageRoles = [StageRoleType.Instructor, StageRoleType.Student, StageRoleType.Coach, StageRoleType.Trainee];

const PostInfo = dynamic<any>(
  () => import('@nara.drama/board').then(module => module.PostInfo),
  { ssr: false }
);

const PostInfoTitle = dynamic<any>(
  () => import('@nara.drama/board').then(module => module.PostInfo)
    .then(postInfo => postInfo.Title),
  { ssr: false }
);

const PostInfoContent = dynamic<any>(
  () => import('@nara.drama/board').then(module => module.PostInfo)
    .then(postInfo => postInfo.Content),
  { ssr: false }
);


interface Props {
  router: NextRouter;
}

interface InjectedProps extends WithDock {
}


@dock(pageRoles)
@autobind
@observer
class QuestionDetailPage extends ReactComponent<Props, {}, InjectedProps> {
  //
  getQuery() {
    //
    const { query } = this.props.router;

    return {
      boardName: query.boardName as string,
      postingId: query.postingId as string,
    };
  }

  routeToBoard() {
    //
    const route = routes.board();

    this.props.router.push(route.href, route.as);
  }

  render() {
    //
    const { currentCitizenId, currentActorName } = this.injected.dock;
    const { boardName, postingId } = this.getQuery();

    return (
      <PostInfo
        postId={postingId}
        idName={new IdName(currentCitizenId, currentActorName, '')}
      >
        <Toolbar />
        <Paper>
          <Container maxWidth="lg">
            <Box py={8}>
              <NavBreadcrumb
                items={[
                  { text: '게시판' },
                  { text: boardName },
                ]}
                onClickItem={this.routeToBoard}
              />
              <Box mt={3}>
                <PostInfoTitle />
              </Box>
            </Box>
          </Container>
        </Paper>
        <Container>
          <Box my={8}>
            <PostInfoContent />
            <Box py={3}>
              <Divider />
            </Box>
            <SubActions>
              <SubActions.Left>
                <Button color="primary" onClick={this.routeToBoard}>목록</Button>
              </SubActions.Left>
            </SubActions>
            <PostCommentList
              postingId={postingId}
              citizenKey={currentCitizenId}
              actorName={currentActorName}
            />
          </Box>
        </Container>
      </PostInfo>
    );
  }
}

export default withRouter(QuestionDetailPage);
