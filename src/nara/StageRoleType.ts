import spec from './kollection.json';


export const StageRoleType = {
  Member: spec.kollection.stageRoles.Member.code,
  Manager: spec.kollection.stageRoles.Manager.code,
  Admin: spec.kollection.stageRoles.Admin.code,
};
