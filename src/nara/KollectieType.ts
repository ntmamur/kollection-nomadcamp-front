export enum KollectieType {
  ClassPattern = 'ClassPattern',
  FunctionPattern = 'FunctionPattern',
  Management = 'Management',
  Employee = 'Employee'
}
