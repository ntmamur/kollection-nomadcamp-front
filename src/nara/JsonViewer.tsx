import React from 'react';
import { autobind, ReactComponent } from '@nara.drama/prologue';


interface Props {
  title: string;
  data: any;
}

@autobind
class JsonViewer extends ReactComponent<Props> {
  render() {
    const { title, data } = this.props;
    return (
      <div style={{ marginTop: '50px' }}>
        <div>
          <div style={{ marginTop: '20px' }}>
            <h3>
              {title}
            </h3>
          </div>
          <div style={{ marginTop: '20px' }}>
            <textarea style={{ border: '0px', height: '150px', width: '100%' }}>
              {JSON.stringify(data, null, 3)}
            </textarea>
          </div>
        </div>
        <div style={{ marginTop: '50px' }}>
          <hr />
        </div>
      </div>
    );
  }
}

export default JsonViewer;
