import { IdName, StringList, WebStorage } from '@nara.drama/prologue';
import { CitizenUser } from '@nara.platform/checkpoint';
import { Cineroom } from '@nara.platform/metro';
import { CurrentActor, StageDock } from '@nara.platform/dock';
import { Cast, RoleSet } from '@nara.drama/stage';
import { StageRoleType } from '~/nara/StageRoleType';


const citizenUser = new CitizenUser('dev_001@nextree.io', 'tester');
const citizenId = '1@1:1';
const cineroomId = '1:1:1';
const audience = new IdName('1@1:1:1', '인 준희');
const troupeId = cineroomId + '-100';
const castId = troupeId + '1';


const actor = new IdName('1@1:1:1-11', '인 준희');


export const useLoginInfo = () => {
  const loggedIn = WebStorage.newSession<boolean>('isLogin');

  const citizenUserStorage = WebStorage.newSession<CitizenUser>('citizenUser');
  const cineroomStorage = WebStorage.newSession<Cineroom>('cineroom');
  const audienceStorage = WebStorage.newSession<IdName>('audience', IdName.fromDomain);
  const cineroomIdsStorage = WebStorage.newSession<string[]>('cineroomIds', Array);   // {id: officeServantId / name: officeServantType} set to header by setAxiosConfigure
  const actorStorage = WebStorage.newSession<IdName>('actor', IdName.fromDomain);   // {id: officeServantId / name: officeServantType} set to header by setAxiosConfigure

  loggedIn.save(true);
  const cu = citizenUser;

  cu.citizenId = citizenId;
  citizenUserStorage.save(cu);

  const cine = new Cineroom('방송 1차', '1:1', 1);

  cine.id = cineroomId;
  cineroomStorage.save(cine);
  audienceStorage.save(new IdName('audienceId', audience.id));
  cineroomIdsStorage.save([cineroomId]);
  actorStorage.save(new IdName('actorId', actor.id));
};


export const getCustomDoc = () => {
  //
  const orgRoleKeys = new StringList();

  orgRoleKeys.elements = ['Member', 'Leader'];

  const roleSet = new RoleSet([StageRoleType.Admin, StageRoleType.Manager, StageRoleType.Member]);

  const customActor = CurrentActor.fromDomain({
    id: actor.id,
    name: actor.name,
    email: citizenUser.loginId,
    audienceId: audience.id,
    castId,
    troupeId,
    orgRoleKeys,
    stageRoleSet: roleSet,
    taskRoleSet: new RoleSet([]),
    dramaRoleSet: new RoleSet([]),
  });


  const customCast = new Cast(
    // id: castId,
    'MSA 교육과정 1차',
    cineroomId,
    troupeId,
    `${process.env.BASE_PATH}/classroom`,
    // kollectie: new IdName('1:1:1-30', '강의실'),
  );

  customCast.id = castId;
  customCast.kollectie = new IdName('1:1:1-30', '강의실');

  return new StageDock('', customCast, customActor);
};
