
const path = require('path');
const withPlugins = require('next-compose-plugins');
const withTM = require('next-transpile-modules')(['@nara.kollection/kollect-nomadcamp-front']);
const removeImports = require('next-remove-imports')(['@nara.kollection/kollect-nomadcamp-front']);


const BASE_PATH = '/nomadcamp';


module.exports = withPlugins([withTM, removeImports], {
  env: {
    BASE_PATH,
  },
  basePath: BASE_PATH,
  pageExtensions: ['page.tsx'],
  distDir: 'dist',
  webpack(config) {
    //
    config.resolve.alias['~'] = path.resolve(__dirname, 'src');
    config.module.rules.push({
      test: /\.svg$/,
      use: ['@svgr/webpack'],
    });

    config.node = {
      fs: 'empty',
    };

    return config;
  },
});
