apt-get install -y gettext

while getopts ":s:d:" opt; do
  case $opt in
    s) service="$OPTARG"
    ;;
    d) deploy="$OPTARG"
    ;;
    \?) echo "Invalid option -$OPTARG" >&2
    ;;
  esac
done

envsubst < deploy/"$deploy".yml > deploy.yml
envsubst < deploy/"$service".yml > service.yml
